<?php
namespace Rbins\PersoBundle\Utils;
use Symfony\Component\Form\FormBuilder;
use Rbins\PersoBundle\Form\ReportsType;

class ReportFetcher {

  static public function getUsersReports(){
    return array(
      'medex' => array(
        'name_fr' => 'Medex',
        'name_nl' => 'Medex',
        'format' => array('pdf'),
      ),
      'mobib' => array(
        'name_fr' => 'Mobib',
        'name_nl' => 'Mobib',
        'format' => array('pdf'/*,'docx'*/),
      ),
      'train_cart' => array(
        'name_fr' => 'Carte de train',
        'name_nl' => 'Treinkaart',
        'format' => array('pdf'/*,'docx'*/),
      ),
      'accidents' => array(
        'name_fr' => 'Accident',
        'name_nl' => 'Arbeidsongeval',
        'format' => array('pdf','docx'),
      ),
      'scdf' => array(
        'name_fr' => 'SCDF',
        'name_nl' => 'CDVU',
        'format' => array('pdf'/*,'docx'*/),
      ),
      'info' => array(
        'name_fr' => 'Formulaire d\'info',
        'name_nl' => 'inlichtingsformulier',
        'translatable' => true,
        'format' => array('pdf'/*,'docx'*/),
      ),
     /* 'duty_history' => array(
        'name_fr' => 'Duty history',
        'name_nl' => 'Duty history',
        'format' => array('xls'),
      ),
      'status_history' => array(
        'name_fr' => 'Status history',
        'name_nl' => 'Status history',
        'format' => array('xls'),
      ),*/
    );
  }

  static public function getGlobalReports($builder = null){
    $reports = array(
      'transport_bike' => array(
        'name_fr' => 'Vélo',
        'name_nl' => 'Fiets',
        'format' => array('xls','pdf'),
      ),
      'contracts' => array(
        'name_fr' => 'Contrats',
        'name_nl' => 'contracten',
        'format' => array('xls','pdf'),
      ),
      'categories' => array(
        'name_fr' => 'Rapport par catégories',
        'name_nl' => 'Verslag van categorie',
        'format' => array('xls','pdf'),
      ),
      'sw10' => array(
        'name_fr' => 'Rapport SW10',
        'name_nl' => 'Sw10 Verslag',
        'format' => array('xls','pdf'),
      ),
      'inventaris' => array(
        'name_fr' => 'Inventaris',
        'name_nl' => 'Inventaris',
        'translatable' => true,
        'format' => array('xls','pdf'),
      ),
      'birthdate' => array(
        'name_fr' => 'Liste par age',
        'name_nl' => 'Lijst per leeftijd',
        'format' => array('xls','pdf'),
      ),
      'problems' => array(
        'name_fr' => 'Problèmes',
        'name_nl' => 'Problemen',
        'format' => array('pdf','xls'),
      ),
      'cresc_services' => array(
        'name_fr' => 'Crescendo : Services',
        'name_nl' => 'Crescendo : Services',
        'format' => array('csv'),
      ),
      'cresc_users' => array(
        'name_fr' => 'Crescendo : Users',
        'name_nl' => 'Crescendo : Users',
        'format' => array('csv'),
      ),
      'func_org' => array(
        'name_fr' => 'Organigramme chefs fonctionnel',
        'name_nl' => 'Functionele Chef Organigram',
        'format' => array('pdf','xls'),
      ),
      'export' => array(
        'name_fr' => 'Db export',
        'name_nl' => 'Db export',
        'format' => array('csv'),
      ),
      'pdata' => array(
        'name_fr' => 'PDATA',
        'name_nl' => 'PDATA',
        'format' => array('csv'),
        'with_form' => true,
      ),
      'work_percentage' => array(
        'name_fr' => 'Régime de travail',
        'name_nl' => 'Arbeidsregime ',
        'format' => array('xls','pdf'),
        'with_form' => true,
      ),
      'replacement_contract' => array(
        'name_fr' => 'Contrats de remplacement',
        'name_nl' => 'Vervanging contracten',
        'format' => array('xls','pdf'),
        'with_form' => true,
      ),
      'cumul' => array(
        'name_fr' => 'Cumul',
        'name_nl' => 'Cumul',
        'translatable' => true,
        'format' => array('xls'),
        'with_form' => true,
      ),
      'com_gest' => array(
        'name_fr' => 'Mouvements de personnels',
        'name_nl' => 'Personeelsbewegingen',
        'format' => array('xls','pdf'),
        'with_form' => true,
      ),
      'primes' => array(
        'name_fr' => 'Primes',
        'name_nl' => 'Premie',
        'format' => array('pdf', 'xls'),
        'with_form' => true,
      ),
      'transports' => array(
        'name_fr' => 'Transport',
        'name_nl' => 'Transport',
        'format' => array('pdf','xls'),
        'with_form' => true,
      ),
      'percentage' => array(
        'name_fr' => 'Changement de %',
        'name_nl' => 'Wijziging %',
        'format' => array('xls'),
        'with_form' => true,
      ),
      'percentage_cause' => array(
        'name_fr' => 'Cause de changement de %',
        'name_nl' => 'Wijziging verantwoording %',
        'format' => array('xls'),
        'with_form' => true,
      ),
      'train_namur_ottignies' => array(
        'name_fr' => 'Personne prenant le train en gare de Namur / Ottignies',
        'name_nl' => 'Persoon die de trein in Namur / Ottignies neemen',
        'format' => array('csv'),
      ),
    );

    if($builder){
      foreach($reports as $name => $report) {
        if(isset($reports[$name]['with_form']) && $reports[$name]['with_form']) {
          $reports[$name]['form'] = $builder->createForm(new ReportsType($name,  $reports[$name]));
          $reports[$name]['form_view'] = $reports[$name]['form']->createView();
        }
      }
    }

    return $reports;
  }

  static public function get($report_server, $report_name, $variables = array(), $format = null, $logger = null) {
    set_time_limit(0);
    $report_path = "perso/".$report_name;

    //Add _fr or _nl to the report path if lang is given
    if(isset($variables['lang'])) {
      $report_path .= "_".$variables['lang'];
      unset($variables['lang']);
    }

    $report_list = self::getGlobalReports();
    if(! $format ){
      if(isset($report_list[$report_name]['format'])) {
        $format = $report_list[$report_name]['format'][0];
      }
      else $format = 'pdf';
    }
    
    if(isset($report_list[$report_name]['translatable']) && $report_list[$report_name]['translatable']) {
      if($variables['locale'] == 'en') $variables['locale'] = 'fr';
      $report_path .= "_".$variables['locale'];
      unset($variables['locale']);
    }
    
    $url = "${report_server}/rest_v2/reports/${report_path}.${format}";
    if(! empty($variables) ) {
      $url .= '?'.http_build_query($variables);
    }
    if($logger)
      $logger->info('Fetch report : ' . $url);
    $content = @file_get_contents($url);
    if($content !== false){
      header("Content-type: application/${format}");
      header("Content-Disposition: attachment; filename=\"$report_name.$format\"");
      echo $content;
      exit();
    }
    throw new \Exception('Error Unable to connect to reporting server or non existing report : '. $url);
  }
}