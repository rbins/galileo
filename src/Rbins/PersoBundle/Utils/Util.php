<?php 
namespace Rbins\PersoBundle\Utils;

class Util {
  static public function unaccent($string){
    $search = explode(',','á,à,â,ä,ã,å,ç,é,è,ê,ë,í,ì,î,ï,ñ,ó,ò,ô,ö,õ,ú,ù,û,ü,ý,ÿ');
    $replace = explode(',','a,a,a,a,a,a,c,e,e,e,e,i,i,i,i,n,o,o,o,o,o,u,u,u,u,y,y');
    return str_replace($search, $replace, $string);
  }
}