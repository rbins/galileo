<?php

namespace Rbins\PersoBundle\Entity;


class CataloguePremium extends Catalogue
{

    /**
     * @var string $code
     */
    private $code;

    /**
     * @var string $name_fr
     */
    private $name_fr;

    /**
     * @var string $name_nl
     */
    private $name_nl;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    private $Premium;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->Premium = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Set code
     *
     * @param string $code
     * @return CataloguePremium
     */
    public function setCode($code)
    {
        $this->code = $code;
    
        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set name_fr
     *
     * @param string $nameFr
     * @return CataloguePremium
     */
    public function setNameFr($nameFr)
    {
        $this->name_fr = $nameFr;
    
        return $this;
    }

    /**
     * Get name_fr
     *
     * @return string 
     */
    public function getNameFr()
    {
      return $this->name_fr;
    }

    /**
     * Set name_nl
     *
     * @param string $nameNl
     * @return CataloguePremium
     */
    public function setNameNl($nameNl)
    {
        $this->name_nl = $nameNl;
    
        return $this;
    }

    /**
     * Get name_nl
     *
     * @return string 
     */
    public function getNameNl()
    {
        return $this->name_nl;
    }

    /**
     * Add Premium
     *
     * @param \Rbins\PersoBundle\Entity\Premium $premium
     * @return CataloguePremium
     */
    public function addPremium(\Rbins\PersoBundle\Entity\Premium $premium)
    {
        $this->Premium[] = $premium;
    
        return $this;
    }

    /**
     * Remove Premium
     *
     * @param \Rbins\PersoBundle\Entity\Premium $premium
     */
    public function removePremium(\Rbins\PersoBundle\Entity\Premium $premium)
    {
        $this->Premium->removeElement($premium);
    }

    /**
     * Get Premium
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPremium()
    {
        return $this->Premium;
    }
}
