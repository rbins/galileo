<?php

namespace Rbins\PersoBundle\Entity;

use Rbins\PersoBundle\Utils\Util as Util;

/**
 * Rbins\PersoBundle\Entity\Catalogue
 */
class Catalogue
{
    /**
     * @var integer $id
     */
    private $id;

    /**
     * @var string $type
     */
    private $type;

    /**
     * @var string $code
     */
    private $code;

    /**
     * @var booolean $is_active
     */
    private $is_active;

    /**
     * @var string $ehr_code
     */
    private $ehr_code;

    /**
     * @var string $name_fr
     */
    private $name_fr;

    /**
     * @var string $name_nl
     */
    private $name_nl;

    public function __toString()
    {
      return $this->getNameEn();
    }

    public function getLabel($locale) {
      $fct = 'getName'.$locale;
      return $this->$fct();
    }
    public function getNameEn()
    {
      return $this->getNameFr() .'-'.$this->getNameNl() ;
    }

    public function getCatalogueNameEn() {
      if(!$this->getIsActive()) return '$$'.$this->getNameEn();
      return $this->getNameEn();
    }
    public function getCatalogueNameFr() {
      if(!$this->getIsActive()) return '$$'.$this->getNameFr();
      return $this->getNameFr();
    }
    public function getCatalogueNameNl() {
      if(!$this->getIsActive()) return '$$'.$this->getNameNl();
      return $this->getNameNl();
    }

    public function getFilter()
    {
      return  Util::unaccent(strtolower($this->getNameFr() .' '.$this->getNameNl()));
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set is_active
     *
     * @param boolean $isActive
     * @return Catalogue
     */
    public function setIsActive($isActive)
    {
        $this->is_active = $isActive;

        return $this;
    }

    /**
     * Get is_active
     *
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->is_active;
    }
    /**
     * @var string
     */
}
