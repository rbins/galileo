<?php

namespace Rbins\PersoBundle\Entity;

use Rbins\PersoBundle\Utils\Util as Util;
use Doctrine\ORM\Mapping as ORM;

/**
 * Rbins\PersoBundle\Entity\MedCenter
 */
class MedCenter
{
    /**
     * @var integer $id
     */
    private $id;

    /**
     * @var string $name
     */
    private $name;

    /**
     * @var string $phone
     */
    private $phone;

    /**
     * @var string $fax
     */
    private $fax;

    /**
     * @var string $address
     */
    private $address;

    /**
     * @var \Rbins\PersoBundle\Entity\City
     */
    private $City;

    public function __toString() {
      return $this->getName();
    }
    
    public function getLabel() {
      return $this->getName();
    }

    public function getFilter()
    {
      return  Util::unaccent(strtolower($this->getName().$this->getCity()->getName().$this->getAddress()));
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return MedCenter
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return MedCenter
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    
        return $this;
    }

    /**
     * Get phone
     *
     * @return string 
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set fax
     *
     * @param string $fax
     * @return MedCenter
     */
    public function setFax($fax)
    {
        $this->fax = $fax;
    
        return $this;
    }

    /**
     * Get fax
     *
     * @return string 
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return MedCenter
     */
    public function setAddress($address)
    {
        $this->address = $address;
    
        return $this;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set City
     *
     * @param \Rbins\PersoBundle\Entity\City $city
     * @return MedCenter
     */
    public function setCity(\Rbins\PersoBundle\Entity\City $city = null)
    {
        $this->City = $city;
    
        return $this;
    }

    /**
     * Get City
     *
     * @return \Rbins\PersoBundle\Entity\City
     */
    public function getCity()
    {
        return $this->City;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $Person;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->Person = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add Person
     *
     * @param \Rbins\PersoBundle\Entity\Person $person
     * @return MedCenter
     */
    public function addPerson(\Rbins\PersoBundle\Entity\Person $person)
    {
        $this->Person[] = $person;
    
        return $this;
    }

    /**
     * Remove Person
     *
     * @param \Rbins\PersoBundle\Entity\Person $person
     */
    public function removePerson(\Rbins\PersoBundle\Entity\Person $person)
    {
        $this->Person->removeElement($person);
    }

    /**
     * Get Person
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPerson()
    {
        return $this->Person;
    }
    /**
     * @var string
     */
    private $comment;


    /**
     * Set comment
     *
     * @param string $comment
     * @return MedCenter
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    
        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }
}
