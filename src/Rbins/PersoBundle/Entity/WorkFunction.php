<?php

namespace Rbins\PersoBundle\Entity;


class WorkFunction extends Catalogue
{

    /**
     * @var string $code
     */
    private $code;

    /**
     * @var string $name_fr
     */
    private $name_fr;

    /**
     * @var string $name_nl
     */
    private $name_nl;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    private $Person;

    public function __toString()
    {
      return $this->getNameEn();
    }

    public function getLabel($locale) {
      $fct = 'getName'.$locale;
      return $this->$fct();
    }
    public function getNameEn()
    {
      return $this->getNameFr() .'-'.$this->getNameNl() ;
    }

    public function getCatalogueNameEn() {
      return $this->getNameEn();
    }
    public function getCatalogueNameFr() {
      return $this->getNameFr();
    }
    public function getCatalogueNameNl() {
      return $this->getNameNl();
    }
      

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->Person = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Set code
     *
     * @param string $code
     * @return WorkFunction
     */
    public function setCode($code)
    {
        $this->code = $code;
    
        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set name_fr
     *
     * @param string $nameFr
     * @return WorkFunction
     */
    public function setNameFr($nameFr)
    {
        $this->name_fr = $nameFr;
    
        return $this;
    }

    /**
     * Get name_fr
     *
     * @return string 
     */
    public function getNameFr()
    {
        return $this->name_fr;
    }

    /**
     * Set name_nl
     *
     * @param string $nameNl
     * @return WorkFunction
     */
    public function setNameNl($nameNl)
    {
        $this->name_nl = $nameNl;
    
        return $this;
    }

    /**
     * Get name_nl
     *
     * @return string 
     */
    public function getNameNl()
    {
        return $this->name_nl;
    }

    private $WorkFunction;


    /**
     * Add WorkFunction
     *
     * @param \Rbins\PersoBundle\Entity\Person $workfunction
     * @return WorkFunction
     */
    public function addWorkFunction(\Rbins\PersoBundle\Entity\Person $workfunction)
    {
        $this->WorkFunction[] = $workfunction;
    
        return $this;
    }


    /**
     * Get WorkFunction
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getWorkFunction()
    {
        return $this->WorkFunction;
    }

    /**
     * Remove WorkFunction
     *
     * @param \Rbins\PersoBundle\Entity\Person $workFunction
     */
    public function removeWorkFunction(\Rbins\PersoBundle\Entity\Person $workFunction)
    {
        $this->WorkFunction->removeElement($workFunction);
    }
}
