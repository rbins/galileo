<?php

namespace Rbins\PersoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PhoneBook
 */
class PhoneBook
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $type;

    /**
     * @var string
     */
    private $entry;

    /**
     * @var \Rbins\PersoBundle\Entity\Person
     */
    private $person;

    /**
     * Set id
     *
     * @param integer $id
     * @return PhoneBook
     */
    public function setId($id)
    {
        $this->id = $id;
    
        return $this;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return PhoneBook
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set entry
     *
     * @param string $entry
     * @return PhoneBook
     */
    public function setEntry($entry)
    {
        $this->entry = $entry;
    
        return $this;
    }

    /**
     * Get entry
     *
     * @return string 
     */
    public function getEntry()
    {
        return $this->entry;
    }

    /**
     * Set person
     *
     * @param \Rbins\PersoBundle\Entity\Person $person
     * @return PhoneBook
     */
    public function setPerson(\Rbins\PersoBundle\Entity\Person $person = null)
    {
        $this->person = $person;
    
        return $this;
    }

    /**
     * Get person
     *
     * @return \Rbins\PersoBundle\Entity\Person 
     */
    public function getPerson()
    {
        return $this->person;
    }

}
