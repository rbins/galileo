<?php

namespace Rbins\PersoBundle\Entity;


class SubLevel extends Catalogue
{
  /**
   * @var string $code
   */
  private $code;

  /**
   * @var string $name_fr
   */
  private $name_fr;

  /**
   * @var string $name_nl
   */
  private $name_nl;

  /**
   * @var \Doctrine\Common\Collections\ArrayCollection
   */
  private $WorkingHist;

  /**
   * Constructor
   */
  public function __construct()
  {
      $this->WorkingHist = new \Doctrine\Common\Collections\ArrayCollection();
  }



    /**
     * Set code
     *
     * @param string $code
     * @return SubLevel
     */
    public function setCode($code)
    {
        $this->code = $code;
    
        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set name_fr
     *
     * @param string $nameFr
     * @return SubLevel
     */
    public function setNameFr($nameFr)
    {
        $this->name_fr = $nameFr;
    
        return $this;
    }

    /**
     * Get name_fr
     *
     * @return string 
     */
    public function getNameFr()
    {
        return $this->name_fr;
    }

    /**
     * Set name_nl
     *
     * @param string $nameNl
     * @return SubLevel
     */
    public function setNameNl($nameNl)
    {
        $this->name_nl = $nameNl;
    
        return $this;
    }

    /**
     * Get name_nl
     *
     * @return string 
     */
    public function getNameNl()
    {
        return $this->name_nl;
    }

    /**
     * Add WorkingHist
     *
     * @param \Rbins\PersoBundle\Entity\WorkingHist $workingHist
     * @return SubLevel
     */
    public function addWorkingHist(\Rbins\PersoBundle\Entity\WorkingHist $workingHist)
    {
        $this->WorkingHist[] = $workingHist;
    
        return $this;
    }

    /**
     * Remove WorkingHist
     *
     * @param \Rbins\PersoBundle\Entity\WorkingHist $workingHist
     */
    public function removeWorkingHist(\Rbins\PersoBundle\Entity\WorkingHist $workingHist)
    {
        $this->WorkingHist->removeElement($workingHist);
    }

    /**
     * Get WorkingHist
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getWorkingHist()
    {
        return $this->WorkingHist;
    }
}
