<?php

namespace Rbins\PersoBundle\Entity;


class FunctFamily extends Catalogue
{

    /**
     * @var string $code
     */
    private $code;

    /**
     * @var string $ehr_code
     */
    private $ehr_code;

    /**
     * @var string $name_fr
     */
    private $name_fr;

    /**
     * @var string $name_nl
     */
    private $name_nl;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    private $FunctionFamily;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->FunctionFamily = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Set code
     *
     * @param string $code
     * @return FunctFamily
     */
    public function setCode($code)
    {
        $this->code = $code;
    
        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set ehr_code
     *
     * @param string $ehrCode
     * @return FunctFamily
     */
    public function setEhrCode($ehrCode)
    {
        $this->ehr_code = $ehrCode;
    
        return $this;
    }

    /**
     * Get ehr_code
     *
     * @return string 
     */
    public function getEhrCode()
    {
        return $this->ehr_code;
    }

    /**
     * Set name_fr
     *
     * @param string $nameFr
     * @return FunctFamily
     */
    public function setNameFr($nameFr)
    {
        $this->name_fr = $nameFr;
    
        return $this;
    }

    /**
     * Get name_fr
     *
     * @return string 
     */
    public function getNameFr()
    {
        return $this->name_fr;
    }

    /**
     * Set name_nl
     *
     * @param string $nameNl
     * @return FunctFamily
     */
    public function setNameNl($nameNl)
    {
        $this->name_nl = $nameNl;
    
        return $this;
    }

    /**
     * Get name_nl
     *
     * @return string 
     */
    public function getNameNl()
    {
        return $this->name_nl;
    }

    /**
     * Add FunctionFamily
     *
     * @param \Rbins\PersoBundle\Entity\WorkingHist $functionFamily
     * @return FunctFamily
     */
    public function addFunctionFamily(\Rbins\PersoBundle\Entity\WorkingHist $functionFamily)
    {
        $this->FunctionFamily[] = $functionFamily;
    
        return $this;
    }

    /**
     * Remove FunctionFamily
     *
     * @param \Rbins\PersoBundle\Entity\WorkingHist $functionFamily
     */
    public function removeFunctionFamily(\Rbins\PersoBundle\Entity\WorkingHist $functionFamily)
    {
        $this->FunctionFamily->removeElement($functionFamily);
    }

    /**
     * Get FunctionFamily
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFunctionFamily()
    {
        return $this->FunctionFamily;
    }
    /**
     * @var string
     */
    private $level;


    /**
     * Set level
     *
     * @param string $level
     * @return FunctFamily
     */
    public function setLevel($level)
    {
        $this->level = $level;
    
        return $this;
    }

    /**
     * Get level
     *
     * @return string 
     */
    public function getLevel()
    {
        return $this->level;
    }
}
