<?php

namespace Rbins\PersoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Rbins\PersoBundle\Entity\Person
 * @ORM\HasLifecycleCallbacks
 */
class Person
{
    /**
     * @var integer $id
     */
    private $id;

    /**
     * @var string $first_name
     */
    private $first_name;

    /**
     * @var string $last_name
     */
    private $last_name;

    /**
     * @var string $alias_first_name
     */
    private $alias_first_name;

    /**
     * @var string $alias_last_name
     */
    private $alias_last_name;

    /**
     * @var string $mother_tongue
     */
    private $mother_tongue;

    /**
     * @var boolean $dependance
     */
    private $dependance;

    /**
     * @var \DateTime $birth_date
     */
    private $birth_date;

    /**
     * @var string $birth_place
     */
    private $birth_place;


    /**
     * @var string $gender
     */
    private $gender;

    /**
     * @var string $identitycard_nr
     */
    private $identitycard_nr;

    /**
     * @var string $national_nr
     */
    private $national_nr;

    /**
     * @var string $matricule
     */
    private $matricule;


    /**
     * @var string $dimona_nr
     */
    private $dimona_nr;

    /**
     * @var boolean $payroll_tax
     */
    private $payroll_tax;

    /**
     * @var string $account_iban_nr
     */
    private $account_iban_nr;

    /**
     * @var string $account_bic_nr
     */
    private $account_bic_nr;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    private $WorkingHist;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    private $PersonAddress;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    private $PersonContact;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    private $PersonFamily;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    private $premium;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    private $transport;

    /**
     * @var Rbins\PersoBundle\Entity\Country
     */
    private $nationality;

    /**
     * @var Rbins\PersoBundle\Entity\MaritalStatus
     */
    private $maritalStatus;
    
    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     */    
    private $Entry;
    /**
     * @var string
     */
    private $sis_nr;
  

    public function __toString() {
      return $this->getFirstName() . ' '.$this->getLastName();
    }
    public function getLabel($locale) {
      return $this->__toString();
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->WorkingHist = new \Doctrine\Common\Collections\ArrayCollection();
        $this->WorkingDuty = new \Doctrine\Common\Collections\ArrayCollection();
        $this->ReplacementPerson = new \Doctrine\Common\Collections\ArrayCollection();
        $this->PersonAddress = new \Doctrine\Common\Collections\ArrayCollection();
        $this->PersonContact = new \Doctrine\Common\Collections\ArrayCollection();
        $this->PersonFamily = new \Doctrine\Common\Collections\ArrayCollection();
        $this->premium = new \Doctrine\Common\Collections\ArrayCollection();
        $this->transport = new \Doctrine\Common\Collections\ArrayCollection();
        $this->Entry = new \Doctrine\Common\Collections\ArrayCollection();
        $this->MedCenter = new \Doctrine\Common\Collections\ArrayCollection();
        $this->CombinedJob = new \Doctrine\Common\Collections\ArrayCollection();
        $this->PhoneBook = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set first_name
     *
     * @param string $firstName
     * @return Person
     */
    public function setFirstName($firstName)
    {
        $this->first_name = $firstName;
    
        return $this;
    }

    /**
     * Get first_name
     *
     * @return string 
     */
    public function getFirstName()
    {
        return $this->first_name;
    }

    /**
     * Set last_name
     *
     * @param string $lastName
     * @return Person
     */
    public function setLastName($lastName)
    {
        $this->last_name = $lastName;
    
        return $this;
    }

    /**
     * Get last_name
     *
     * @return string 
     */
    public function getLastName()
    {
        return $this->last_name;
    }

    /**
     * Set alias_first_name
     *
     * @param string $aliasFirstName
     * @return Person
     */
    public function setAliasFirstName($aliasFirstName)
    {
        $this->alias_first_name = $aliasFirstName;
    
        return $this;
    }

    /**
     * Get alias_first_name
     *
     * @return string 
     */
    public function getAliasFirstName()
    {
        return $this->alias_first_name;
    }

    /**
     * Set alias_last_name
     *
     * @param string $aliasLastName
     * @return Person
     */
    public function setAliasLastName($aliasLastName)
    {
        $this->alias_last_name = $aliasLastName;
    
        return $this;
    }

    /**
     * Get alias_last_name
     *
     * @return string 
     */
    public function getAliasLastName()
    {
        return $this->alias_last_name;
    }

    /**
     * Set mother_tongue
     *
     * @param string $motherTongue
     * @return Person
     */
    public function setMotherTongue($motherTongue)
    {
        $this->mother_tongue = $motherTongue;
    
        return $this;
    }

    /**
     * Get mother_tongue
     *
     * @return string 
     */
    public function getMotherTongue()
    {
        return $this->mother_tongue;
    }

    /**
     * Set dependance
     *
     * @param boolean $dependance
     * @return Person
     */
    public function setDependance($dependance)
    {
        $this->dependance = $dependance;
    
        return $this;
    }

    /**
     * Get dependance
     *
     * @return boolean 
     */
    public function getDependance()
    {
        return $this->dependance;
    }

    /**
     * Set birth_date
     *
     * @param \DateTime $birthDate
     * @return Person
     */
    public function setBirthDate($birthDate)
    {
        $this->birth_date = $birthDate;
    
        return $this;
    }

    /**
     * Get birth_date
     *
     * @return \DateTime 
     */
    public function getBirthDate()
    {
        return $this->birth_date;
    }
    
    public function isBirth_date()
    {        
      $now = new \DateTime;
      return ($this->birth_date < $now) ;
    }

    /**
     * Set birth_place
     *
     * @param string $birthPlace
     * @return Person
     */
    public function setBirthPlace($birthPlace)
    {
        $this->birth_place = $birthPlace;
    
        return $this;
    }

    /**
     * Get birth_place
     *
     * @return string 
     */
    public function getBirthPlace()
    {
        return $this->birth_place;
    }


    /**
     * Set gender
     *
     * @param string $gender
     * @return Person
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
    
        return $this;
    }

    /**
     * Get gender
     *
     * @return string 
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set identitycard_nr
     *
     * @param string $identitycardNr
     * @return Person
     */
    public function setIdentitycardNr($identitycardNr)
    {
        $this->identitycard_nr = $identitycardNr;
    
        return $this;
    }

    /**
     * Get identitycard_nr
     *
     * @return string 
     */
    public function getIdentitycardNr()
    {
        return $this->identitycard_nr;
    }

    /**
     * Set national_nr
     *
     * @param string $nationalNr
     * @return Person
     */
    public function setNationalNr($nationalNr)
    {
        $this->national_nr = $nationalNr;
    
        return $this;
    }

    public function isNational_nr(){
      return (preg_match('/^[0-9]{6}\-[0-9]{3}\-[0-9]{2}$/',$this->national_nr));
    }

    /**
     * Get national_nr
     *
     * @return string 
     */
    public function getNationalNr()
    {
        return $this->national_nr;
    }

    /**
     * Set matricule
     *
     * @param string $matricule
     * @return Person
     */
    public function setMatricule($matricule)
    {
        $this->matricule = $matricule;
    
        return $this;
    }

    /**
     * Get matricule
     *
     * @return string 
     */
    public function getMatricule()
    {
        return $this->matricule;
    }

    /**
     * Set dimona_nr
     *
     * @param string $dimonaNr
     * @return Person
     */
    public function setDimonaNr($dimonaNr)
    {
        $this->dimona_nr = $dimonaNr;
    
        return $this;
    }

    /**
     * Get dimona_nr
     *
     * @return string 
     */
    public function getDimonaNr()
    {
        return $this->dimona_nr;
    }

    /**
     * Set payroll_tax
     *
     * @param boolean $payrollTax
     * @return Person
     */
    public function setPayrollTax($payrollTax)
    {
        $this->payroll_tax = $payrollTax;
    
        return $this;
    }

    /**
     * Get payroll_tax
     *
     * @return boolean 
     */
    public function getPayrollTax()
    {
        return $this->payroll_tax;
    }

    /**
     * Set account_iban_nr
     *
     * @param string $accountIbanNr
     * @return Person
     */
    public function setAccountIbanNr($accountIbanNr)
    {
        $this->account_iban_nr = $accountIbanNr;
    
        return $this;
    }

    /**
     * Get account_iban_nr
     *
     * @return string 
     */
    public function getAccountIbanNr()
    {
        return $this->account_iban_nr;
    }

    /**
     * Set account_bic_nr
     *
     * @param string $accountBicNr
     * @return Person
     */
    public function setAccountBicNr($accountBicNr)
    {
        $this->account_bic_nr = $accountBicNr;
    
        return $this;
    }

    /**
     * Get account_bic_nr
     *
     * @return string 
     */
    public function getAccountBicNr()
    {
        return $this->account_bic_nr;
    }

    /**
     * Add WorkingHist
     *
     * @param Rbins\PersoBundle\Entity\WorkingHist $workingHist
     * @return Person
     */
    public function addWorkingHist(\Rbins\PersoBundle\Entity\WorkingHist $workingHist)
    {
        $workingHist->setPerson($this);
        $this->WorkingHist[] = $workingHist;
        return $this;
    }

    /**
     * Remove WorkingHist
     *
     * @param Rbins\PersoBundle\Entity\WorkingHist $workingHist
     */
    public function removeWorkingHist(\Rbins\PersoBundle\Entity\WorkingHist $workingHist)
    {
        $this->WorkingHist->removeElement($workingHist);
    }

    /**
     * Get WorkingHist
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getWorkingHist()
    {
        return $this->WorkingHist;
    }

    /**
     * Add ReplacementPerson
     *
     * @param Rbins\PersoBundle\Entity\WorkingHist $replacementPerson
     * @return Person
     */
    public function addReplacementPerson(\Rbins\PersoBundle\Entity\WorkingHist $replacementPerson)
    {
        $this->ReplacementPerson[] = $replacementPerson;
    
        return $this;
    }

    /**
     * Remove ReplacementPerson
     *
     * @param Rbins\PersoBundle\Entity\WorkingHist $replacementPerson
     */
    public function removeReplacementPerson(\Rbins\PersoBundle\Entity\WorkingHist $replacementPerson)
    {
        $this->ReplacementPerson->removeElement($replacementPerson);
    }

    /**
     * Get ReplacementPerson
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getReplacementPerson()
    {
        return $this->ReplacementPerson;
    }

    /**
     * Add PersonAddress
     *
     * @param Rbins\PersoBundle\Entity\PersonAddress $personAddress
     * @return Person
     */
    public function addPersonAddres(\Rbins\PersoBundle\Entity\PersonAddress $personAddress)
    {
        $personAddress->setPerson($this);    
        $this->PersonAddress[] = $personAddress;
    
        return $this;
    }

    /**
     * Remove PersonAddress
     *
     * @param Rbins\PersoBundle\Entity\PersonAddress $personAddress
     */
    public function removePersonAddres(\Rbins\PersoBundle\Entity\PersonAddress $personAddress)
    {
        $this->PersonAddress->removeElement($personAddress);
    }

    /**
     * Get PersonAddress
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getPersonAddress()
    {
        return $this->PersonAddress;
    }

    /**
     * Add PersonContact
     *
     * @param Rbins\PersoBundle\Entity\PersonContact $personContact
     * @return Person
     */
    public function addPersonContact(\Rbins\PersoBundle\Entity\PersonContact $personContact)
    {
        $personContact->setPerson($this);
        $this->PersonContact[] = $personContact;
    
        return $this;
    }

    /**
     * Remove PersonContact
     *
     * @param Rbins\PersoBundle\Entity\PersonContact $personContact
     */
    public function removePersonContact(\Rbins\PersoBundle\Entity\PersonContact $personContact)
    {
        $this->PersonContact->removeElement($personContact);
    }

    /**
     * Get PersonContact
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getPersonContact()
    {
        return $this->PersonContact;
    }

    /**
     * Add family
     *
     * @param Rbins\PersoBundle\Entity\Family $family
     * @return Person
     */
    public function addPersonFamily(\Rbins\PersoBundle\Entity\Family $family)
    {
        $family->setPerson($this);
        $this->PersonFamily[] = $family;   
        return $this;
    }

    /**
     * Remove family
     *
     * @param Rbins\PersoBundle\Entity\Family $family
     */
    public function removePersonFamily(\Rbins\PersoBundle\Entity\Family $family)
    {
        $this->PersonFamily->removeElement($family);
    }

    /**
     * Get family
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getPersonFamily()
    {
        return $this->PersonFamily;
    }

    /**
     * Add premium
     *
     * @param Rbins\PersoBundle\Entity\Premium $premium
     * @return Person
     */
    public function addPremium(\Rbins\PersoBundle\Entity\Premium $premium)
    {
        $premium->setPerson($this) ;
        $this->premium[] = $premium;
    
        return $this;
    }

    /**
     * Remove premium
     *
     * @param Rbins\PersoBundle\Entity\Premium $premium
     */
    public function removePremium(\Rbins\PersoBundle\Entity\Premium $premium)
    {
        $this->premium->removeElement($premium);
    }

    /**
     * Get premium
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getPremium()
    {
        return $this->premium;
    }

    /**
     * Add transport
     *
     * @param Rbins\PersoBundle\Entity\Transport $transport
     * @return Person
     */
    public function addTransport(\Rbins\PersoBundle\Entity\Transport $transport)
    {
        $transport->setPerson($this) ;
        $this->transport[] = $transport;
    
        return $this;
    }

    /**
     * Remove transport
     *
     * @param Rbins\PersoBundle\Entity\Transport $transport
     */
    public function removeTransport(\Rbins\PersoBundle\Entity\Transport $transport)
    {
        $this->transport->removeElement($transport);
    }

    /**
     * Get transport
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getTransport()
    {
        return $this->transport;
    }

    /**
     * Set nationality
     *
     * @param Rbins\PersoBundle\Entity\Country $nationality
     * @return Person
     */
    public function setNationality(\Rbins\PersoBundle\Entity\Country $nationality = null)
    {
        $this->nationality = $nationality;
    
        return $this;
    }

    /**
     * Get nationality
     *
     * @return Rbins\PersoBundle\Entity\Country 
     */
    public function getNationality()
    {
        return $this->nationality;
    }

    /**
     * Set maritalStatus
     *
     * @param Rbins\PersoBundle\Entity\MaritalStatus $maritalStatus
     * @return Person
     */
    public function setMaritalStatus(\Rbins\PersoBundle\Entity\MaritalStatus $maritalStatus = null)
    {
        $this->maritalStatus = $maritalStatus;
    
        return $this;
    }

    /**
     * Get maritalStatus
     *
     * @return Rbins\PersoBundle\Entity\MaritalStatus 
     */
    public function getMaritalStatus()
    {
        return $this->maritalStatus;
    }
    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    private $PersonEntry;


    /**
     * Add PersonEntry
     *
     * @param Rbins\PersoBundle\Entity\PersonEntry $personEntry
     * @return Person
     */
    public function addPersonEntry(\Rbins\PersoBundle\Entity\PersonEntry $personEntry)
    {
        $personEntry->setPerson($this) ;
        $this->PersonEntry[] = $personEntry;
    
        return $this;
    }

    /**
     * Remove PersonEntry
     *
     * @param Rbins\PersoBundle\Entity\PersonEntry $personEntry
     */
    public function removePersonEntry(\Rbins\PersoBundle\Entity\PersonEntry $personEntry)
    {
        $this->PersonEntry->removeElement($personEntry);
    }

    /**
     * Get PersonEntry
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getPersonEntry()
    {
        return $this->PersonEntry;
    }
    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    private $Collaborator;

    /**
     * @var Rbins\PersoBundle\Entity\Person
     */
    private $FunctionalChief;


    /**
     * Add Collaborator
     *
     * @param Rbins\PersoBundle\Entity\Person $collaborator
     * @return Person
     */
    public function addCollaborator(\Rbins\PersoBundle\Entity\Person $collaborator)
    {
        $this->Collaborator[] = $collaborator;
    
        return $this;
    }

    /**
     * Remove Collaborator
     *
     * @param Rbins\PersoBundle\Entity\Person $collaborator
     */
    public function removeCollaborator(\Rbins\PersoBundle\Entity\Person $collaborator)
    {
        $this->Collaborator->removeElement($collaborator);
    }

    /**
     * Get Collaborator
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getCollaborator()
    {
        return $this->Collaborator;
    }

    /**
     * Set FunctionalChief
     *
     * @param Rbins\PersoBundle\Entity\Person $functionalChief
     * @return Person
     */
    public function setFunctionalChief(\Rbins\PersoBundle\Entity\Person $functionalChief = null)
    {
        $this->FunctionalChief = $functionalChief;
    
        return $this;
    }

    /**
     * Get FunctionalChief
     *
     * @return Rbins\PersoBundle\Entity\Person 
     */
    public function getFunctionalChief()
    {
        return $this->FunctionalChief;
    }
    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    private $Document;


    /**
     * Add Document
     *
     * @param Rbins\PersoBundle\Entity\Document $document
     * @return Person
     */
    public function addDocument(\Rbins\PersoBundle\Entity\Document $document)
    {
        $document->setPerson($this);
        $this->Document[] = $document;
    
        return $this;
    }

    /**
     * Remove Document
     *
     * @param Rbins\PersoBundle\Entity\Document $document
     */
    public function removeDocument(\Rbins\PersoBundle\Entity\Document $document)
    {
        $this->Document->removeElement($document);
    }

    /**
     * Get Document
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getDocument()
    {
        return $this->Document;
    }
    /**
     * @var string $other_account_holder
     */
    private $other_account_holder;


    /**
     * Set other_account_holder
     *
     * @param string $otherAccountHolder
     * @return Person
     */
    public function setOtherAccountHolder($otherAccountHolder)
    {
        $this->other_account_holder = $otherAccountHolder;
    
        return $this;
    }

    /**
     * Get other_account_holder
     *
     * @return string 
     */
    public function getOtherAccountHolder()
    {
        return $this->other_account_holder;
    }
    /**
     * @var boolean
     */
    private $household_allowance;

    /**
     * @var boolean
     */
    private $residence_allowance;


    /**
     * Set household_allowance
     *
     * @param boolean $householdAllowance
     * @return Person
     */
    public function setHouseholdAllowance($householdAllowance)
    {
        $this->household_allowance = $householdAllowance;
    
        return $this;
    }

    /**
     * Get household_allowance
     *
     * @return boolean 
     */
    public function getHouseholdAllowance()
    {
        return $this->household_allowance;
    }

    /**
     * Set residence_allowance
     *
     * @param boolean $residenceAllowance
     * @return Person
     */
    public function setResidenceAllowance($residenceAllowance)
    {
        $this->residence_allowance = $residenceAllowance;
    
        return $this;
    }

    /**
     * Get residence_allowance
     *
     * @return boolean 
     */
    public function getResidenceAllowance()
    {
        return $this->residence_allowance;
    }

    /**
     * Set sis_nr
     *
     * @param string $sisNr
     * @return Person
     */
    public function setSisNr($sisNr)
    {
        $this->sis_nr = $sisNr;
    
        return $this;
    }

    /**
     * Get sis_nr
     *
     * @return string 
     */
    public function getSisNr()
    {
        return $this->sis_nr;
    }
    /**
     * @var \Rbins\PersoBundle\Entity\MedCenter
     */
    private $MedCenter;


    /**
     * Set MedCenter
     *
     * @param \Rbins\PersoBundle\Entity\MedCenter $medCenter
     * @return Person
     */
    public function setMedCenter(\Rbins\PersoBundle\Entity\MedCenter $medCenter = null)
    {
        $this->MedCenter = $medCenter;
    
        return $this;
    }

    /**
     * Get MedCenter
     *
     * @return \Rbins\PersoBundle\Entity\MedCenter 
     */
    public function getMedCenter()
    {
        return $this->MedCenter;
    }
    /**
     * @var string
     */
    private $med_number;


    /**
     * Set med_number
     *
     * @param string $medNumber
     * @return Person
     */
    public function setMedNumber($medNumber)
    {
        $this->med_number = $medNumber;
    
        return $this;
    }

    /**
     * Get med_number
     *
     * @return string 
     */
    public function getMedNumber()
    {
        return $this->med_number;
    }
    /**
     * @var boolean
     */
    private $handicap;


    /**
     * @var string
     */
    private $passport_nr;

    /**
     * @var string
     */
    private $comment;


    /**
     * Set handicap
     *
     * @param boolean $handicap
     * @return Person
     */
    public function setHandicap($handicap)
    {
        $this->handicap = $handicap;
    
        return $this;
    }

    /**
     * Get handicap
     *
     * @return boolean 
     */
    public function getHandicap()
    {
        return $this->handicap;
    }

    /**
     * Set passport_nr
     *
     * @param string $passportNr
     * @return Person
     */
    public function setPassportNr($passportNr)
    {
      if($passportNr == null) $passportNr = "";
      $this->passport_nr = $passportNr;
    
      return $this;
    }

    /**
     * Get passport_nr
     *
     * @return string 
     */
    public function getPassportNr()
    {
        return $this->passport_nr;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * @return Person
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    
        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }
    /**
     * @var boolean
     */
    private $senior_working;


    /**
     * Set senior_working
     *
     * @param boolean $seniorWorking
     * @return Person
     */
    public function setSeniorWorking($seniorWorking)
    {
        $this->senior_working = $seniorWorking;
    
        return $this;
    }

    /**
     * Get senior_working
     *
     * @return boolean 
     */
    public function getSeniorWorking()
    {
        return $this->senior_working;
    }
    /**
     * @var string
     */
    private $homework;


    /**
     * Set homework
     *
     * @param string $homework
     * @return Person
     */
    public function setHomework($homework)
    {
        $this->homework = $homework;
    
        return $this;
    }

    /**
     * Get homework
     *
     * @return string 
     */
    public function getHomework()
    {
        return $this->homework;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $CombinedJob;


    /**
     * Add CombinedJob
     *
     * @param \Rbins\PersoBundle\Entity\CombinedJob $combinedJob
     * @return Person
     */
    public function addCombinedJob(\Rbins\PersoBundle\Entity\CombinedJob $combinedJob)
    {
        $combinedJob->setPerson($this);        
        $this->CombinedJob[] = $combinedJob;
    
        return $this;
    }

    /**
     * Remove CombinedJob
     *
     * @param \Rbins\PersoBundle\Entity\CombinedJob $combinedJob
     */
    public function removeCombinedJob(\Rbins\PersoBundle\Entity\CombinedJob $combinedJob)
    {
        $this->CombinedJob->removeElement($combinedJob);
    }

    /**
     * Get CombinedJob
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCombinedJob()
    {
        return $this->CombinedJob;
    }
    /**
     * @var string
     */
    private $building;

    /**
     * @var string
     */
    private $building_floor;

    /**
     * @var string
     */
    private $building_room;


    /**
     * Set building
     *
     * @param string $building
     * @return Person
     */
    public function setBuilding($building)
    {
        $this->building = $building;
    
        return $this;
    }

    /**
     * Get building
     *
     * @return string 
     */
    public function getBuilding()
    {
        return $this->building;
    }

    /**
     * Set building_floor
     *
     * @param string $buildingFloor
     * @return Person
     */
    public function setBuildingFloor($buildingFloor)
    {
        $this->building_floor = $buildingFloor;
    
        return $this;
    }

    /**
     * Get building_floor
     *
     * @return string 
     */
    public function getBuildingFloor()
    {
        return $this->building_floor;
    }
    /**
     * @var \DateTime
     */

    /**
     * Set building_room
     *
     * @param string $buildingRoom
     * @return Person
     */
    public function setBuildingRoom($buildingRoom)
    {
        $this->building_room = $buildingRoom;
    
        return $this;
    }

    /**
     * Get building_room
     *
     * @return string 
     */
    public function getBuildingRoom()
    {
        return $this->building_room;
    }
    /**
     * @var \DateTime
     */

    private $senior_working_date;

    /**
     * @var \Rbins\PersoBundle\Entity\Degree
     */
    private $Degree;


    /**
     * Set senior_working_date
     *
     * @param \DateTime $seniorWorkingDate
     * @return Person
     */
    public function setSeniorWorkingDate($seniorWorkingDate)
    {
        $this->senior_working_date = $seniorWorkingDate;
    
        return $this;
    }

    /**
     * Get senior_working_date
     *
     * @return \DateTime 
     */
    public function getSeniorWorkingDate()
    {
        return $this->senior_working_date;
    }

    /**
     * Set Degree
     *
     * @param \Rbins\PersoBundle\Entity\Degree $degree
     * @return Person
     */
    public function setDegree(\Rbins\PersoBundle\Entity\Degree $degree = null)
    {
        $this->Degree = $degree;
        return $this;
    }

    /**
     * Get Degree
     *
     * @return \Rbins\PersoBundle\Entity\Degree 
     */
    public function getDegree()
    {
        return $this->Degree;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $WorkingDuty;


    /**
     * Add WorkingDuty
     *
     * @param \Rbins\PersoBundle\Entity\WorkingDuty $workingDuty
     * @return Person
     */
    public function addWorkingDuty(\Rbins\PersoBundle\Entity\WorkingDuty $workingDuty)
    {
        $workingDuty->setPerson($this);
        $this->WorkingDuty[] = $workingDuty;
        return $this;
    }

    /**
     * Remove WorkingDuty
     *
     * @param \Rbins\PersoBundle\Entity\WorkingDuty $workingDuty
     */
    public function removeWorkingDuty(\Rbins\PersoBundle\Entity\WorkingDuty $workingDuty)
    {
        $this->WorkingDuty->removeElement($workingDuty);
    }

    /**
     * Get WorkingDuty
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getWorkingDuty()
    {
        return $this->WorkingDuty;
    }
    /**
     * @var \Rbins\PersoBundle\Entity\Frame
     */
    private $Frame;


    /**
     * Set Frame
     *
     * @param \Rbins\PersoBundle\Entity\Frame $frame
     * @return Person
     */
    public function setFrame(\Rbins\PersoBundle\Entity\Frame $frame = null)
    {
        $this->Frame = $frame;
    
        return $this;
    }

    /**
     * Get Frame
     *
     * @return \Rbins\PersoBundle\Entity\Frame 
     */
    public function getFrame()
    {
        return $this->Frame;
    }
    /**
     * @var \Rbins\PersoBundle\Entity\Frame
     */
    private $WorkFunction;


    /**
     * Set WorkFunction
     *
     * @param \Rbins\PersoBundle\Entity\WorkFunction $workfunction
     * @return Person
     */
    public function setWorkFunction(\Rbins\PersoBundle\Entity\WorkFunction $workfunction = null)
    {
        $this->WorkFunction = $workfunction;

        return $this;
    }

    /**
     * Get WorkFunction
     *
     * @return \Rbins\PersoBundle\Entity\WorkFunction
     */
    public function getWorkFunction()
    {
        return $this->WorkFunction;
    }

    /**
     * @var integer
     */
    private $handicap_perc;


    /**
     * Set handicap_perc
     *
     * @param integer $handicapPerc
     * @return Person
     */
    public function setHandicapPerc($handicapPerc)
    {
        $this->handicap_perc = $handicapPerc;
    
        return $this;
    }

    /**
     * Get handicap_perc
     *
     * @return integer 
     */
    public function getHandicapPerc()
    {
        return $this->handicap_perc;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $PhoneBook;


    /**
     * Add PhoneBook
     *
     * @param \Rbins\PersoBundle\Entity\PhoneBook $phoneBook
     * @return Person
     */
    public function addPhoneBook(\Rbins\PersoBundle\Entity\PhoneBook $phoneBook)
    {
        $phoneBook->setPerson($this);
        $this->PhoneBook[] = $phoneBook;
    
        return $this;
    }

    /**
     * Remove PhoneBook
     *
     * @param \Rbins\PersoBundle\Entity\PhoneBook $phoneBook
     */
    public function removePhoneBook(\Rbins\PersoBundle\Entity\PhoneBook $phoneBook)
    {
        $this->PhoneBook->removeElement($phoneBook);
    }

    /**
     * Get PhoneBook
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPhoneBook()
    {
        return $this->PhoneBook;
    }
    /**
     * @var string
     */
    private $description;


    /**
     * Set description
     *
     * @param string $description
     * @return Person
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }
    /**
     * @var string
     */
    
    /**
     * @Assert\File(maxSize="6000000")
     */
    protected $file = null;

    /**
     * @var string
     */
    private $photo;


    /**
     * Set photo
     *
     * @param string $photo
     * @return Person
     */
    public function setPhoto($photo)
    {
        $this->photo = $photo;
    
        return $this;
    }

    /**
     * Get photo
     *
     * @return string 
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    public function upload()
    {
        if($this->file=== null)
        {
            return;
        }
        $file_to_delete = $this->getUploadRootDir().'/'.$this->id.'.*' ;
        array_map( "unlink", glob( $file_to_delete ) );
        $this->setPhoto($this->id.'.'.$this->file->guessExtension());
        $this->file->move($this->getUploadRootDir(), $this->id.'.'.$this->file->guessExtension()) ;
        unset($this->file);
    }

    public function removeUpload()
    {
      if ($this->file = $this->getUploadRootDir().'/'.$this->getPhoto()) {
        if( file_exists($this->file))
        {
          unlink($this->file);
          $this->photo = null ;
        }
      }
    }

    public function getFile()
    {
      return $this->file;
    }

    public function setFile($file)
    {
      $this->file = $file;
      return  $this;
    }

    public function getPhotoPath()
    {
        return null === $this->photo ? null : $this->getUploadDir().'/'.$this->photo;
    }

    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__.'/../../../../web/'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw when displaying uploaded doc/image in the view.
        return 'photos';
    }  
    /**
     * @var string
     */
    private $uid;


    /**
     * Set uid
     *
     * @param string $uid
     * @return Person
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    
        return $this;
    }

    /**
     * Get uid
     *
     * @return string 
     */
    public function getUid()
    {
        return $this->uid;
    }

    public function getAge($birth)
    {        
        $arr1 = explode('/', $birth);
        $arr2 = explode('/', date('d/m/Y'));
        
        if(($arr1[1] < $arr2[1]) || (($arr1[1] == $arr2[1]) && ($arr1[0] <= $arr2[0])))
        return $arr2[2] - $arr1[2];

        return $arr2[2] - $arr1[2] - 1;
    }
     /**
     * @var string
     */
    private $situation;   
    /**
     * Set situation
     *
     * @param string $situation
     * @return Person
     */
    public function setSituation($situation)
    {
        $this->situation = $situation;
    
        return $this;
    }

    /**
     * Get situation
     *
     * @return string 
     */
    public function getSituation()
    {
        return $this->situation;
    }

     /**
     * @var string
     */
    private $hobbies;   
    /**
     * Set hobbies
     *
     * @param string $hobbies
     * @return Person
     */
    public function setHobbies($hobbies)
    {
        $hobbies = preg_replace('/\s;/', ',',$hobbies) ;
        $this->hobbies = $hobbies;
    
        return $this;
    }

    /**
     * Get hobbies
     *
     * @return string 
     */
    public function getHobbies()
    {
        return $this->hobbies;
    }    


     /**
     * @var boolean
     */
    private $pb_visible=true;


    /**
     * @var string
     */
    private $email;   
    /**
     * Set email
     *
     * @param string $email
     * @return Person
     */
    public function setEmail($email)
    {
        $this->email = strtolower($email);
        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set pb_visible
     *
     * @param boolean $pbVisible
     * @return Person
     */
    public function setPbVisible($pbVisible)
    {
        $this->pb_visible = $pbVisible;
    
        return $this;
    }

    /**
     * Get pb_visible
     *
     * @return boolean 
     */
    public function getPbVisible()
    {
        return $this->pb_visible;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $CollaboratorR;

    /**
     * @var \Rbins\PersoBundle\Entity\Person
     */
    private $ResponsibleChief;


    /**
     * Add personAddress
     *
     * @param \Rbins\PersoBundle\Entity\PersonAddress $personAddress
     *
     * @return Person
     */
    public function addPersonAddress(\Rbins\PersoBundle\Entity\PersonAddress $personAddress)
    {
        $this->PersonAddress[] = $personAddress;

        return $this;
    }

    /**
     * Remove personAddress
     *
     * @param \Rbins\PersoBundle\Entity\PersonAddress $personAddress
     */
    public function removePersonAddress(\Rbins\PersoBundle\Entity\PersonAddress $personAddress)
    {
        $this->PersonAddress->removeElement($personAddress);
    }

    /**
     * Add collaboratorR
     *
     * @param \Rbins\PersoBundle\Entity\Person $collaboratorR
     *
     * @return Person
     */
    public function addCollaboratorR(\Rbins\PersoBundle\Entity\Person $collaboratorR)
    {
        $this->CollaboratorR[] = $collaboratorR;

        return $this;
    }

    /**
     * Remove collaboratorR
     *
     * @param \Rbins\PersoBundle\Entity\Person $collaboratorR
     */
    public function removeCollaboratorR(\Rbins\PersoBundle\Entity\Person $collaboratorR)
    {
        $this->CollaboratorR->removeElement($collaboratorR);
    }

    /**
     * Get collaboratorR
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCollaboratorR()
    {
        return $this->CollaboratorR;
    }

    /**
     * Set responsibleChief
     *
     * @param \Rbins\PersoBundle\Entity\Person $responsibleChief
     *
     * @return Person
     */
    public function setResponsibleChief(\Rbins\PersoBundle\Entity\Person $responsibleChief = null)
    {
        $this->ResponsibleChief = $responsibleChief;

        return $this;
    }

    /**
     * Get responsibleChief
     *
     * @return \Rbins\PersoBundle\Entity\Person
     */
    public function getResponsibleChief()
    {
        return $this->ResponsibleChief;
    }
}
