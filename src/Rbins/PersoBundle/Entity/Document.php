<?php

namespace Rbins\PersoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Rbins\PersoBundle\Entity\Document
 */
class Document
{
    /**
     * @var integer $id
     */
    private $id;

    /**
     * @var sting $title
     */
    private $title;

    /**
     * @var sting $path
     */
    private $path;

    /**
     * @var \Rbins\PersoBundle\Entity\Person
     */
    private $Person;

    /**
     * @Assert\File(maxSize="6000000")
     */
    protected $file = null;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param sting $title
     * @return Document
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    public function getFile()
    {
      return $this->file;
    }

    public function setFile($file)
    {
      $this->file = $file;
      return  $this;
    }
    /**
     * Get title
     *
     * @return sting 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set path
     *
     * @param sting $path
     * @return Document
     */
    public function setPath($path)
    {
        $this->path = $path;
    
        return $this;
    }

    /**
     * Get path
     *
     * @return sting 
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set Person
     *
     * @param \Rbins\PersoBundle\Entity\Person $person
     * @return Document
     */
    public function setPerson(\Rbins\PersoBundle\Entity\Person $person = null)
    {
        $this->Person = $person;
    
        return $this;
    }

    /**
     * Get Person
     *
     * @return \Rbins\PersoBundle\Entity\Person
     */
    public function getPerson()
    {
        return $this->Person;
    }


    public function getAbsolutePath()
    {
        return null === $this->path ? null : $this->getUploadRootDir().'/'.$this->path;
    }

    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__.'/../../../../'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw when displaying uploaded doc/image in the view.
        return 'uploads/documents';
    }

    public function preUpload()
    {
      if (null !== $this->file) {
        // do whatever you want to generate a unique name
        $this->path = sha1(uniqid(mt_rand(), true)).'.'.$this->file->guessExtension();
        $this->setName($this->file->getClientOriginalName());
      }
    }

    public function upload()
    {
      if (null === $this->file) {
        return;
      }

      // if there is an error when moving the file, an exception will
      // be automatically thrown by move(). This will properly prevent
      // the entity from being persisted to the database on error
      $this->file->move($this->getUploadRootDir(), $this->path);

      unset($this->file);
    }

    public function removeUpload()
    {
      if ($this->file = $this->getAbsolutePath()) {
        if( file_exists($this->file))
          unlink($this->file);
      }
    }
    /**
     * @var string $name
     */
    private $name;


    /**
     * Set name
     *
     * @param string $name
     * @return Document
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }
    /**
     * @var \Rbins\PersoBundle\Entity\DocType
     */
    private $DocType;


    /**
     * Set DocType
     *
     * @param \Rbins\PersoBundle\Entity\DocType $docType
     * @return Document
     */
    public function setDocType(\Rbins\PersoBundle\Entity\DocType $docType = null)
    {
        $this->DocType = $docType;
    
        return $this;
    }

    /**
     * Get DocType
     *
     * @return \Rbins\PersoBundle\Entity\DocType 
     */
    public function getDocType()
    {
        return $this->DocType;
    }
    
    
    
    
    /**
     * @var string type_ref
     */
    private $type_ref;


    /**
     * Set $type_ref
     *
     * @param string $type_ref
     * @return Document
     */
    public function setTypeRef($type_ref)
    {
        $this->type_ref = $type_ref;
    
        return $this;
    }

    /**
     * Get type_ref
     *
     * @return string 
     */
    public function getTypeRef()
    {
        return $this->type_ref;
    }    
}
