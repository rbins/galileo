<?php

namespace Rbins\PersoBundle\Entity;


class ExitReason extends Catalogue
{

    /**
     * @var string $code
     */
    private $code;

    /**
     * @var string $name_fr
     */
    private $name_fr;

    /**
     * @var string $name_nl
     */
    private $name_nl;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    private $Person;

    public function __toString()
    {
      return $this->getNameEn();
    }

    public function getLabel($locale) {
      $fct = 'getName'.$locale;
      return $this->$fct();
    }
    public function getNameEn()
    {
      return $this->getNameFr() .'-'.$this->getNameNl() ;
    }

    public function getCatalogueNameEn() {
      return $this->getNameEn();
    }
    public function getCatalogueNameFr() {
      return $this->getNameFr();
    }
    public function getCatalogueNameNl() {
      return $this->getNameNl();
    }
      

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->Person = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Set code
     *
     * @param string $code
     * @return ExitReason
     */
    public function setCode($code)
    {
        $this->code = $code;
    
        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set name_fr
     *
     * @param string $nameFr
     * @return ExitReason
     */
    public function setNameFr($nameFr)
    {
        $this->name_fr = $nameFr;
    
        return $this;
    }

    /**
     * Get name_fr
     *
     * @return string 
     */
    public function getNameFr()
    {
        return $this->name_fr;
    }

    /**
     * Set name_nl
     *
     * @param string $nameNl
     * @return ExitReason
     */
    public function setNameNl($nameNl)
    {
        $this->name_nl = $nameNl;
    
        return $this;
    }

    /**
     * Get name_nl
     *
     * @return string 
     */
    public function getNameNl()
    {
        return $this->name_nl;
    }

    private $Entry;


    /**
     * Add Entry
     *
     * @param \Rbins\PersoBundle\Entity\PersonEntry $entry
     * @return ExitReason
     */
    public function addEntry(\Rbins\PersoBundle\Entity\PersonEntry $entry)
    {
        $this->Entry[] = $entry;
    
        return $this;
    }

    /**
     * Remove Entry
     *
     * @param \Rbins\PersoBundle\Entity\PersonEntry $entry
     */
    public function removeEntry(\Rbins\PersoBundle\Entity\PersonEntry $entry)
    {
        $this->Entry->removeElement($entry);
    }

    /**
     * Get Entry
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEntry()
    {
        return $this->Entry;
    }
    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    private $PersonEntry;


    /**
     * Add PersonEntry
     *
     * @param \Rbins\PersoBundle\Entity\PersonEntry $personEntry
     * @return ExitReason
     */
    public function addPersonEntry(\Rbins\PersoBundle\Entity\PersonEntry $personEntry)
    {
        $this->PersonEntry[] = $personEntry;
    
        return $this;
    }

    /**
     * Remove PersonEntry
     *
     * @param \Rbins\PersoBundle\Entity\PersonEntry $personEntry
     */
    public function removePersonEntry(\Rbins\PersoBundle\Entity\PersonEntry $personEntry)
    {
        $this->PersonEntry->removeElement($personEntry);
    }

    /**
     * Get PersonEntry
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPersonEntry()
    {
        return $this->PersonEntry;
    }
}
