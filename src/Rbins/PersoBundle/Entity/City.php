<?php

namespace Rbins\PersoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Rbins\PersoBundle\Utils\Util as Util;

/**
 * Rbins\PersoBundle\Entity\City
 */
class City
{
    /**
     * @var integer $id
     */
    private $id;

    /**
     * @var string $code
     */
    private $code;

    /**
     * @var string $name
     */
    private $name;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    private $PersonAddress;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    private $MedCenter;

    /**
     * @var \Rbins\PersoBundle\Entity\Country
     */
    private $Country;

    public function getFilter()
    {
      return  Util::unaccent(strtolower($this->getName() .''.$this->getCode()));
    }


    public function getLabel()
    {
      return $this->getName().' ('.$this->getCode() .')';
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->PersonAddress = new \Doctrine\Common\Collections\ArrayCollection();
        $this->MedCenter = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    public function __toString() {
      return $this->getName().' ('.$this->getCode() .')';
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return City
     */
    public function setCode($code)
    {
        $this->code = $code;
    
        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return City
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add personAddress
     *
     * @param \Rbins\PersoBundle\Entity\PersonAddress $personAddress
     *
     * @return City
     */
    public function addPersonAddress(\Rbins\PersoBundle\Entity\PersonAddress $personAddress)
    {
        $this->PersonAddress[] = $personAddress;

        return $this;
    }

    /**
     * Remove personAddress
     *
     * @param \Rbins\PersoBundle\Entity\PersonAddress $personAddress
     */
    public function removePersonAddress(\Rbins\PersoBundle\Entity\PersonAddress $personAddress)
    {
        $this->PersonAddress->removeElement($personAddress);
    }

    /**
     * Get PersonAddress
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPersonAddress()
    {
        return $this->PersonAddress;
    }

    /**
     * Add MedCenter
     *
     * @param \Rbins\PersoBundle\Entity\MedCenter $medCenter
     * @return City
     */
    public function addMedCenter(\Rbins\PersoBundle\Entity\MedCenter $medCenter)
    {
        $this->MedCenter[] = $medCenter;
    
        return $this;
    }

    /**
     * Remove MedCenter
     *
     * @param \Rbins\PersoBundle\Entity\MedCenter $medCenter
     */
    public function removeMedCenter(\Rbins\PersoBundle\Entity\MedCenter $medCenter)
    {
        $this->MedCenter->removeElement($medCenter);
    }

    /**
     * Get MedCenter
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMedCenter()
    {
        return $this->MedCenter;
    }

    /**
     * Set Country
     *
     * @param \Rbins\PersoBundle\Entity\Country $country
     * @return City
     */
    public function setCountry(\Rbins\PersoBundle\Entity\Country $country = null)
    {
        $this->Country = $country;
    
        return $this;
    }

    /**
     * Get Country
     *
     * @return \Rbins\PersoBundle\Entity\Country
     */
    public function getCountry()
    {
        return $this->Country;
    }
    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    private $TransportFrom;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    private $TransportTo;


    /**
     * Add TransportFrom
     *
     * @param \Rbins\PersoBundle\Entity\Transport $transportFrom
     * @return City
     */
    public function addTransportFrom(\Rbins\PersoBundle\Entity\Transport $transportFrom)
    {
        $this->TransportFrom[] = $transportFrom;
    
        return $this;
    }

    /**
     * Remove TransportFrom
     *
     * @param \Rbins\PersoBundle\Entity\Transport $transportFrom
     */
    public function removeTransportFrom(\Rbins\PersoBundle\Entity\Transport $transportFrom)
    {
        $this->TransportFrom->removeElement($transportFrom);
    }

    /**
     * Get TransportFrom
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTransportFrom()
    {
        return $this->TransportFrom;
    }

    /**
     * Add TransportTo
     *
     * @param \Rbins\PersoBundle\Entity\Transport $transportTo
     * @return City
     */
    public function addTransportTo(\Rbins\PersoBundle\Entity\Transport $transportTo)
    {
        $this->TransportTo[] = $transportTo;
    
        return $this;
    }

    /**
     * Remove TransportTo
     *
     * @param \Rbins\PersoBundle\Entity\Transport $transportTo
     */
    public function removeTransportTo(\Rbins\PersoBundle\Entity\Transport $transportTo)
    {
        $this->TransportTo->removeElement($transportTo);
    }

    /**
     * Get TransportTo
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTransportTo()
    {
        return $this->TransportTo;
    }
}
