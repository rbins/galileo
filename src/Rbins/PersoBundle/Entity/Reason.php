<?php

namespace Rbins\PersoBundle\Entity;


class Reason extends Catalogue
{

    /**
     * @var string $code
     */
    private $code;

    /**
     * @var string $name_fr
     */
    private $name_fr;

    /**
     * @var string $name_nl
     */
    private $name_nl;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    private $Cause;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->Cause = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Set code
     *
     * @param string $code
     * @return Reason
     */
    public function setCode($code)
    {
        $this->code = $code;
    
        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set name_fr
     *
     * @param string $nameFr
     * @return Reason
     */
    public function setNameFr($nameFr)
    {
        $this->name_fr = $nameFr;
    
        return $this;
    }

    /**
     * Get name_fr
     *
     * @return string 
     */
    public function getNameFr()
    {
        return $this->name_fr;
    }

    /**
     * Set name_nl
     *
     * @param string $nameNl
     * @return Reason
     */
    public function setNameNl($nameNl)
    {
        $this->name_nl = $nameNl;
    
        return $this;
    }

    /**
     * Get name_nl
     *
     * @return string 
     */
    public function getNameNl()
    {
        return $this->name_nl;
    }

    /**
     * Add Cause
     *
     * @param \Rbins\PersoBundle\Entity\WorkingHist $cause
     * @return Reason
     */
    public function addCause(\Rbins\PersoBundle\Entity\WorkingHist $cause)
    {
        $this->Cause[] = $cause;
    
        return $this;
    }

    /**
     * Remove Cause
     *
     * @param \Rbins\PersoBundle\Entity\WorkingHist $cause
     */
    public function removeCause(\Rbins\PersoBundle\Entity\WorkingHist $cause)
    {
        $this->Cause->removeElement($cause);
    }

    /**
     * Get Cause
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCause()
    {
        return $this->Cause;
    }
    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    private $Reason;


    /**
     * Add Reason
     *
     * @param \Rbins\PersoBundle\Entity\WorkingHist $reason
     * @return Reason
     */
    public function addReason(\Rbins\PersoBundle\Entity\WorkingHist $reason)
    {
        $this->Reason[] = $reason;
    
        return $this;
    }

    /**
     * Remove Reason
     *
     * @param \Rbins\PersoBundle\Entity\WorkingHist $reason
     */
    public function removeReason(\Rbins\PersoBundle\Entity\WorkingHist $reason)
    {
        $this->Reason->removeElement($reason);
    }

    /**
     * Get Reason
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReason()
    {
        return $this->Reason;
    }
}
