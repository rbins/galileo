<?php

namespace Rbins\PersoBundle\Entity;


class Cause extends Catalogue
{

    /**
     * @var string $code
     */
    private $code;

    /**
     * @var string $name_fr
     */
    private $name_fr;

    /**
     * @var string $name_nl
     */
    private $name_nl;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    private $WorkingDuty;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->WorkingDuty = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Set code
     *
     * @param string $code
     * @return WorkingDuty
     */
    public function setCode($code)
    {
        $this->code = $code;
    
        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set name_fr
     *
     * @param string $nameFr
     * @return WorkingDuty
     */
    public function setNameFr($nameFr)
    {
        $this->name_fr = $nameFr;
    
        return $this;
    }

    /**
     * Get name_fr
     *
     * @return string 
     */
    public function getNameFr()
    {
        return $this->name_fr;
    }

    /**
     * Set name_nl
     *
     * @param string $nameNl
     * @return WorkingDuty
     */
    public function setNameNl($nameNl)
    {
        $this->name_nl = $nameNl;
    
        return $this;
    }

    /**
     * Get name_nl
     *
     * @return string 
     */
    public function getNameNl()
    {
        return $this->name_nl;
    }

    /**
     * Add WorkingDuty
     *
     * @param \Rbins\PersoBundle\Entity\WorkingDuty $WorkingDuty
     * @return WorkingDuty
     */
    public function addWorkingDuty(\Rbins\PersoBundle\Entity\WorkingDuty $WorkingDuty)
    {
        $this->WorkingDuty[] = $WorkingDuty;
    
        return $this;
    }

    /**
     * Remove WorkingDuty
     *
     * @param \Rbins\PersoBundle\Entity\WorkingDuty $WorkingDuty
     */
    public function removeWorkingDuty(\Rbins\PersoBundle\Entity\WorkingDuty $WorkingDuty)
    {
        $this->WorkingDuty->removeElement($WorkingDuty);
    }

    /**
     * Get WorkingDuty
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getWorkingDuty()
    {
        return $this->WorkingDuty;
    }
}
