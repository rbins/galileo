<?php

namespace Rbins\PersoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Rbins\PersoBundle\Entity\Family
 */
class Family
{
    /**
     * @var integer $id
     */
    private $id;

    /**
     * @var string $full_name
     */
    private $full_name;

    /**
     * @var string $gender
     */
    private $gender;

    /**
     * @var \DateTime $birth_date
     */
    private $birth_date;

    /**
     * @var \DateTime $allowance_end
     */
    private $allowance_end;

    /**
     * @var boolean $parential_leave
     */
    private $parential_leave;

    /**
     * @var string $comment
     */
    private $comment;

    /**
     * @var string $handicap
     */
    private $handicap;

    /**
     * @var string $employed
     */
    private $employed;
    
    /**
     * @var \Rbins\PersoBundle\Entity\Person
     */
    private $person;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set full_name
     *
     * @param string $fullName
     * @return Child
     */
    public function setFullName($fullName)
    {
        $this->full_name = $fullName;
    
        return $this;
    }

    /**
     * Get full_name
     *
     * @return string 
     */
    public function getFullName()
    {
        return $this->full_name;
    }

    /**
     * Set gender
     *
     * @param string $gender
     * @return Child
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
    
        return $this;
    }

    /**
     * Get gender
     *
     * @return string 
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set birth_date
     *
     * @param \DateTime $birthDate
     * @return Child
     */
    public function setBirthDate($birthDate)
    {
        $this->birth_date = $birthDate;
    
        return $this;
    }
    
    /**
     * Get birth_date
     *
     * @return \DateTime 
     */
    public function getBirthDate()
    {
        return $this->birth_date;
    }

    /**
     * Set allowance_end
     *
     * @param \DateTime $allowanceEnd
     * @return Child
     */
    public function setAllowanceEnd($allowanceEnd)
    {
        $this->allowance_end = $allowanceEnd;
    
        return $this;
    }

    /**
     * Get allowance_end
     *
     * @return \DateTime 
     */
    public function getAllowanceEnd()
    {
        return $this->allowance_end;
    }

    /**
     * Set parential_leave
     *
     * @param boolean $parentialLeave
     * @return Child
     */
    public function setParentialLeave($parentialLeave)
    {
        $this->parential_leave = $parentialLeave;
    
        return $this;
    }
    
    /**
     * Set handicap
     *
     * @param boolean $handicap
     * @return Family
     */
    public function setHandicap($handicap)
    {
        $this->handicap = $handicap;
    
        return $this;
    }
    
    /**
     * Set employed
     *
     * @param boolean $employed
     * @return Child
     */
    public function setEmployed($employed)
    {
        if($employed == 2) $this->employed = null ;
        else $this->employed = $employed;
    
        return $this;
    }    

    /**
     * Get handicap
     *
     * @return boolean 
     */
    public function getHandicap()
    {
        return $this->handicap;
    }

    /**
     * Get employed
     *
     * @return boolean 
     */
    public function getEmployed()
    {
        return $this->employed;
    }

    /**
     * Get parential_leave
     *
     * @return boolean 
     */
    public function getParentialLeave()
    {
        return $this->parential_leave;
    }    

    /**
     * Set comment
     *
     * @param string $comment
     * @return Child
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    
        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set person
     *
     * @param \Rbins\PersoBundle\Entity\Person $person
     * @return Child
     */
    public function setPerson(\Rbins\PersoBundle\Entity\Person $person = null)
    {
        $this->person = $person;
    
        return $this;
    }

    /**
     * Get person
     *
     * @return \Rbins\PersoBundle\Entity\Person
     */
    public function getPerson()
    {
        return $this->person;
    }
    /**
     * @var boolean $allowance
     */
    private $allowance;


    /**
     * Set allowance
     *
     * @param boolean $allowance
     * @return Child
     */
    public function setAllowance($allowance)
    {
        $this->allowance = $allowance;
    
        return $this;
    }

    /**
     * Get allowance
     *
     * @return boolean 
     */
    public function getAllowance()
    {
        return $this->allowance;
    }
    /**
     * @var string $type
     */
    private $type;


    /**
     * Set type
     *
     * @param string $type
     * @return Family
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }
}
