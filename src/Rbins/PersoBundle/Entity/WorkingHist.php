<?php

namespace Rbins\PersoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Rbins\PersoBundle\Entity\Person;
use Symfony\Component\Validator\ExecutionContext;
/**
 * Rbins\PersoBundle\Entity\WorkingHist
 */
class WorkingHist
{
    /**
     * @var integer $id
     */
    private $id;

    /**
     * @var string $level
     */
    private $level;

    /**
     * @var string $linguistic_role
     */
    private $linguistic_role;

    /**
     * @var \DateTime $seniority
     */
    private $seniority;

    /**
     * @var \DateTime $salary_seniority
     */
    private $salary_seniority;

    /**
     * @var \DateTime $start_date
     */
    private $start_date;

    /**
     * @var \DateTime $end_date
     */
    private $end_date;

    /**
     * @var string $comment
     */
    private $comment;

    /**
     * @var \Rbins\PersoBundle\Entity\Person
     */
    private $Person;

    /**
     * @var string $ReplacementPerson
     */
    private $replacement_person;

    /**
     * @var \Rbins\PersoBundle\Entity\Status
     */
    private $Status;

    /**
     * @var \Rbins\PersoBundle\Entity\Grade
     */
    private $Grade;

    /**
     * @var \Rbins\PersoBundle\Entity\Reason
     */
    private $Reason;

    /**
     * @var \Rbins\PersoBundle\Entity\SalaryGrade
     */
    private $SalaryGrade;

    /**
     * @var \Rbins\PersoBundle\Entity\FunctFamily
     */
    private $FunctionFamily;

    /**
     * @var \Rbins\PersoBundle\Entity\ContractDuration
     */
    private $ContractDuration;

    /**
     * @var \Rbins\PersoBundle\Entity\ContractComment
     */
    private $ContractComment;

    /**
     * @var \Rbins\PersoBundle\Entity\SubLevel
     */
    private $SubLevel;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set comment
     *
     * @param string $comment
     * @return WorkingHist
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    
        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set level
     *
     * @param string $level
     * @return WorkingHist
     */
    public function setLevel($level)
    {
        $this->level = $level;
    
        return $this;
    }

    /**
     * Get level
     *
     * @return string 
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * Set linguistic_role
     *
     * @param string $linguisticRole
     * @return WorkingHist
     */
    public function setLinguisticRole($linguisticRole)
    {
        $this->linguistic_role = $linguisticRole;
    
        return $this;
    }

    /**
     * Get linguistic_role
     *
     * @return string 
     */
    public function getLinguisticRole()
    {
        return $this->linguistic_role;
    }

    /**
     * Set seniority
     *
     * @param \DateTime $seniority
     * @return WorkingHist
     */
    public function setSeniority($seniority)
    {
        $this->seniority = $seniority;
    
        return $this;
    }

    /**
     * Get seniority
     *
     * @return \DateTime 
     */
    public function getSeniority()
    {
        return $this->seniority;
    }

    /**
     * Set salary_seniority
     *
     * @param \DateTime $salarySeniority
     * @return WorkingHist
     */
    public function setSalarySeniority($salarySeniority)
    {
        $this->salary_seniority = $salarySeniority;
    
        return $this;
    }

    /**
     * Get salary_seniority
     *
     * @return \DateTime 
     */
    public function getSalarySeniority()
    {
        return $this->salary_seniority;
    }

    /**
     * Set start_date
     *
     * @param \DateTime $startDate
     * @return WorkingHist
     */
    public function setStartDate($startDate)
    {
        $this->start_date = $startDate;
    
        return $this;
    }
    
    /**
     * Get start_date
     *
     * @return \DateTime 
     */
    public function getStartDate()
    {
        return $this->start_date;
    }

    /**
     * Set end_date
     *
     * @param \DateTime $endDate
     * @return WorkingHist
     */
    public function setEndDate($endDate)
    {
        $this->end_date = $endDate;

        return $this;
    }

    /** end_date validator **/
    public function isEnd_date()
    {
      if (!$this->end_date) return true ;
      return ($this->start_date <= $this->end_date) ;
    }

    /**
     * Get end_date
     *
     * @return \DateTime 
     */
    public function getEndDate()
    {
        return $this->end_date;
    }

    public function setPerson(\Rbins\PersoBundle\Entity\Person $person = null)
    {
        $this->Person = $person;
    
        return $this;
    }

    /**
     * Get Person
     *
     * @return \Rbins\PersoBundle\Entity\Person
     */
    public function getPerson()
    {
        return $this->Person;
    }

    /**
     * Set Status
     *
     * @param \Rbins\PersoBundle\Entity\Status $status
     * @return WorkingHist
     */
    public function setStatus(\Rbins\PersoBundle\Entity\Status $status = null)
    {
        $this->Status = $status;
    
        return $this;
    }

    /**
     * Get Status
     *
     * @return \Rbins\PersoBundle\Entity\Status
     */
    public function getStatus()
    {
        return $this->Status;
    }

    /**
     * Set Grade
     *
     * @param \Rbins\PersoBundle\Entity\Grade $grade
     * @return WorkingHist
     */
    public function setGrade(\Rbins\PersoBundle\Entity\Grade $grade = null)
    {
        $this->Grade = $grade;
    
        return $this;
    }

    /**
     * Get Grade
     *
     * @return \Rbins\PersoBundle\Entity\Grade
     */
    public function getGrade()
    {
        return $this->Grade;
    }

    /**
     * Set Reason
     *
     * @param \Rbins\PersoBundle\Entity\Reason $reason
     * @return WorkingHist
     */
    public function setReason(\Rbins\PersoBundle\Entity\Reason $reason = null)
    {
        $this->Reason = $reason;
    
        return $this;
    }

    /**
     * Get Reason
     *
     * @return \Rbins\PersoBundle\Entity\Reason
     */
    public function getReason()
    {
        return $this->Reason;
    }

    /**
     * Set SalaryGrade
     *
     * @param \Rbins\PersoBundle\Entity\SalaryGrade $salaryGrade
     * @return WorkingHist
     */
    public function setSalaryGrade(\Rbins\PersoBundle\Entity\SalaryGrade $salaryGrade = null)
    {
        $this->SalaryGrade = $salaryGrade;
    
        return $this;
    }

    /**
     * Get SalaryGrade
     *
     * @return \Rbins\PersoBundle\Entity\SalaryGrade
     */
    public function getSalaryGrade()
    {
        return $this->SalaryGrade;
    }

    /**
     * Set FunctionFamily
     *
     * @param \Rbins\PersoBundle\Entity\FunctFamily $functionFamily
     * @return WorkingHist
     */
    public function setFunctionFamily(\Rbins\PersoBundle\Entity\FunctFamily $functionFamily = null)
    {
        $this->FunctionFamily = $functionFamily;
    
        return $this;
    }

    /**
     * Get FunctionFamily
     *
     * @return \Rbins\PersoBundle\Entity\FunctFamily
     */
    public function getFunctionFamily()
    {
        return $this->FunctionFamily;
    }

    /**
     * Set ContractDuration
     *
     * @param \Rbins\PersoBundle\Entity\ContractDuration $contractDuration
     * @return WorkingHist
     */
    public function setContractDuration(\Rbins\PersoBundle\Entity\ContractDuration $contractDuration = null)
    {
        $this->ContractDuration = $contractDuration;
    
        return $this;
    }

    /**
     * Get ContractDuration
     *
     * @return \Rbins\PersoBundle\Entity\ContractDuration
     */
    public function getContractDuration()
    {
        return $this->ContractDuration;
    }

    /**
     * Set ContractComment
     *
     * @param \Rbins\PersoBundle\Entity\ContractComment $contractComment
     * @return WorkingHist
     */
    public function setContractComment(\Rbins\PersoBundle\Entity\ContractComment $contractComment = null)
    {
        $this->ContractComment = $contractComment;
    
        return $this;
    }

    /**
     * Get ContractComment
     *
     * @return \Rbins\PersoBundle\Entity\ContractComment
     */
    public function getContractComment()
    {
        return $this->ContractComment;
    }

    /**
     * Set SubLevel
     *
     * @param \Rbins\PersoBundle\Entity\SubLevel $subLevel
     * @return WorkingHist
     */
    public function setSubLevel(\Rbins\PersoBundle\Entity\SubLevel $subLevel = null)
    {
        $this->SubLevel = $subLevel;
    
        return $this;
    }

    /**
     * Get SubLevel
     *
     * @return \Rbins\PersoBundle\Entity\SubLevel
     */
    public function getSubLevel()
    {
        return $this->SubLevel;
    }
    /**
     * @var string
     */
    private $cost_center;

    /**
     * @var string
     */
    private $cd_function;


    /**
     * Set cost_center
     *
     * @param string $costCenter
     * @return WorkingHist
     */
    public function setCostCenter($costCenter)
    {
        $this->cost_center = $costCenter;
    
        return $this;
    }

    /**
     * Get cost_center
     *
     * @return string 
     */
    public function getCostCenter()
    {
        return $this->cost_center;
    }

    /**
     * Set cd_function
     *
     * @param string $cdFunction
     * @return WorkingHist
     */
    public function setCdFunction($cdFunction)
    {
        $this->cd_function = $cdFunction;
    
        return $this;
    }

    /**
     * Get cd_function
     *
     * @return string 
     */
    public function getCdFunction()
    {
        return $this->cd_function;
    }
    /**
     * @var string
     */
    private $lang_level;


    /**
     * Set lang_level
     *
     * @param string $langLevel
     * @return WorkingHist
     */
    public function setLangLevel($langLevel)
    {
        $this->lang_level = $langLevel;
        return $this;
    }

    /**
     * Get lang_level
     *
     * @return string 
     */
    public function getLangLevel()
    {
        return $this->lang_level;
    }

    /**
     * Set replacement_person
     *
     * @param string $replacementPerson
     * @return WorkingHist
     */
    public function setReplacementPerson($replacementPerson)
    {
        $this->replacement_person = $replacementPerson;
        return $this;
    }

    /**
     * Get replacement_person
     *
     * @return string 
     */
    public function getReplacementPerson()
    {
        return $this->replacement_person;
    }
    /**
     * @var integer
     */
    private $class;


    /**
     * Set class
     *
     * @param integer $class
     * @return WorkingHist
     */
    public function setClass($class)
    {
        $this->class = $class;
    
        return $this;
    }

    /**
     * Get class
     *
     * @return integer 
     */
    public function getClass()
    {
        return $this->class;
    }
    /**
     * @var string
     */
    private $category;


    /**
     * Set category
     *
     * @param string $category
     * @return WorkingHist
     */
    public function setCategory($category)
    {
        $this->category = $category;
    
        return $this;
    }

    /**
     * Get category
     *
     * @return string 
     */
    public function getCategory()
    {
        return $this->category;
    }
    /**
     * @var \DateTime
     */
    private $classseniority;


    /**
     * Set classseniority
     *
     * @param \DateTime $classseniority
     *
     * @return WorkingHist
     */
    public function setClassseniority($classseniority)
    {
        $this->classseniority = $classseniority;

        return $this;
    }

    /**
     * Get classseniority
     *
     * @return \DateTime
     */
    public function getClassseniority()
    {
        return $this->classseniority;
    }
    /**
     * @var \DateTime
     */
    private $scientific_seniority;


    /**
     * Set scientificSeniority
     *
     * @param \DateTime $scientificSeniority
     *
     * @return WorkingHist
     */
    public function setScientificSeniority($scientificSeniority)
    {
        $this->scientific_seniority = $scientificSeniority;

        return $this;
    }

    /**
     * Get scientificSeniority
     *
     * @return \DateTime
     */
    public function getScientificSeniority()
    {
        return $this->scientific_seniority;
    }
    /**
     * @var \DateTime
     */
    private $levelseniority;


    /**
     * Set levelSeniority
     *
     * @param \DateTime $levelSeniority
     *
     * @return WorkingHist
     */
    public function setLevelSeniority($levelSeniority)
    {
        $this->levelseniority = $levelSeniority;

        return $this;
    }

    /**
     * Get levelSeniority
     *
     * @return \DateTime
     */
    public function getLevelSeniority()
    {
        return $this->levelseniority;
    }
    /**
     * @var \DateTime
     */
    private $degree_seniority;

    /**
     * @var \DateTime
     */
    private $scale_seniority;


    /**
     * Set degreeSeniority
     *
     * @param \DateTime $degreeSeniority
     *
     * @return WorkingHist
     */
    public function setDegreeSeniority($degreeSeniority)
    {
        $this->degree_seniority = $degreeSeniority;

        return $this;
    }

    /**
     * Get degreeSeniority
     *
     * @return \DateTime
     */
    public function getDegreeSeniority()
    {
        return $this->degree_seniority;
    }

    /**
     * Set scaleSeniority
     *
     * @param \DateTime $scaleSeniority
     *
     * @return WorkingHist
     */
    public function setScaleSeniority($scaleSeniority)
    {
        $this->scale_seniority = $scaleSeniority;

        return $this;
    }

    /**
     * Get scaleSeniority
     *
     * @return \DateTime
     */
    public function getScaleSeniority()
    {
        return $this->scale_seniority;
    }
    /**
     * @var string
     */
    private $noticemonths;

    /**
     * @var string
     */
    private $noticeweeks;


    /**
     * Set noticeMonths
     *
     * @param string $noticeMonths
     *
     * @return WorkingHist
     */
    public function setNoticeMonths($noticeMonths)
    {
        $this->noticemonths = $noticeMonths;

        return $this;
    }

    /**
     * Get noticeMonths
     *
     * @return string
     */
    public function getNoticeMonths()
    {
        return $this->noticemonths;
    }

    /**
     * Set noticeWeeks
     *
     * @param string $noticeWeeks
     *
     * @return WorkingHist
     */
    public function setNoticeWeeks($noticeWeeks)
    {
        $this->noticeweeks = $noticeWeeks;

        return $this;
    }

    /**
     * Get noticeWeeks
     *
     * @return string
     */
    public function getNoticeWeeks()
    {
        return $this->noticeweeks;
    }
}
