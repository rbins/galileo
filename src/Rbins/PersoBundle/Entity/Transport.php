<?php

namespace Rbins\PersoBundle\Entity;
use Symfony\Component\Validator\ExecutionContext;

use Doctrine\ORM\Mapping as ORM;

/**
 * Rbins\PersoBundle\Entity\Transport
 */
class Transport
{
 
    /**
     * @var integer $id
     */
    private $id;

    /**
     * @var \DateTime $start_date
     */
    private $start_date;

    /**
     * @var \DateTime $end_date
     */
    private $end_date;

    /**
     * @var boolean $bike
     */
    private $bike;

    /**
     * @var boolean $car
     */
    private $car;

    /**
     * @var boolean $stibmivb
     */
    private $stibmivb;

    /**
     * @var boolean $tec
     */
    private $tec;

    /**
     * @var boolean $delijn
     */
    private $delijn;

    /**
     * @var boolean $sncb
     */
    private $sncb;

    /**
     * @var integer $km
     */
    private $km;

    /**
     * @var boolean $is_effective
     */
    private $is_effective;

    /**
     * @var \Rbins\PersoBundle\Entity\Person
     */
    private $person;

    /**
     * @var \Rbins\PersoBundle\Entity\City
     */
    private $CityFrom;

    /**
     * @var \Rbins\PersoBundle\Entity\City
     */
    private $CityTo;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set start_date
     *
     * @param \DateTime $startDate
     * @return Transport
     */
    public function setStartDate($startDate)
    {
        $this->start_date = $startDate;
    
        return $this;
    }

    /**
     * Get start_date
     *
     * @return \DateTime 
     */
    public function getStartDate()
    {
        return $this->start_date;
    }

    /**
     * Set end_date
     *
     * @param \DateTime $endDate
     * @return Transport
     */
    public function setEndDate($endDate)
    {
        $this->end_date = $endDate;
    
        return $this;
    }

    /**
     * Get end_date
     *
     * @return \DateTime 
     */
    public function getEndDate()
    {
        return $this->end_date;
    }

    /** end_date validator **/
    public function isEnd_date()
    {          
      if (!$this->end_date) return true ;
      return ($this->start_date <= $this->end_date) ;
    }   
    
    /** checkbox validator, to be sure at least one transport type is checked **/
    public function isCheckOnlyOne()
    {
      return(!$this->bike && !$this->car && !$this->stibmivb && !$this->delijn && !$this->tec && !$this->sncb);
    }

    /**
     * Set bike
     *
     * @param boolean $bike
     * @return Transport
     */
    public function setBike($bike)
    {
        $this->bike = $bike;
    
        return $this;
    }

    /**
     * Get bike
     *
     * @return boolean 
     */
    public function getBike()
    {
        return $this->bike;
    }

    /**
     * Set car
     *
     * @param boolean $car
     * @return Transport
     */
    public function setCar($car)
    {
        $this->car = $car;
    
        return $this;
    }

    /**
     * Get car
     *
     * @return boolean 
     */
    public function getCar()
    {
        return $this->car;
    }

    /**
     * Set stibmivb
     *
     * @param boolean $stibmivb
     * @return Transport
     */
    public function setStibmivb($stibmivb)
    {
        $this->stibmivb = $stibmivb;
    
        return $this;
    }

    /**
     * Get stibmivb
     *
     * @return boolean 
     */
    public function getStibmivb()
    {
        return $this->stibmivb;
    }

    /**
     * Set tec
     *
     * @param boolean $tec
     * @return Transport
     */
    public function setTec($tec)
    {
        $this->tec = $tec;
    
        return $this;
    }

    /**
     * Get tec
     *
     * @return boolean 
     */
    public function getTec()
    {
        return $this->tec;
    }

    /**
     * Set delijn
     *
     * @param boolean $delijn
     * @return Transport
     */
    public function setDelijn($delijn)
    {
        $this->delijn = $delijn;
    
        return $this;
    }

    /**
     * Get delijn
     *
     * @return boolean 
     */
    public function getDelijn()
    {
        return $this->delijn;
    }

    /**
     * Set sncb
     *
     * @param boolean $sncb
     * @return Transport
     */
    public function setSncb($sncb)
    {
        $this->sncb = $sncb;
    
        return $this;
    }

    /**
     * Get sncb
     *
     * @return boolean 
     */
    public function getSncb()
    {
        return $this->sncb;
    }

    /**
     * Set km
     *
     * @param integer $km
     * @return Transport
     */
    public function setKm($km)
    {
        $this->km = $km;
    
        return $this;
    }

    /**
     * Get km
     *
     * @return integer 
     */
    public function getKm()
    {
        return $this->km;
    }

    /**
     * Set is_effective
     *
     * @param boolean $isEffective
     * @return Transport
     */
    public function setIsEffective($isEffective)
    {
        $this->is_effective = $isEffective;
    
        return $this;
    }

    /**
     * Get is_effective
     *
     * @return boolean 
     */
    public function getIsEffective()
    {
        return $this->is_effective;
    }

    /**
     * Set person
     *
     * @param \Rbins\PersoBundle\Entity\Person $person
     * @return Transport
     */
    public function setPerson(\Rbins\PersoBundle\Entity\Person $person = null)
    {
        $this->person = $person;
    
        return $this;
    }

    /**
     * Get person
     *
     * @return \Rbins\PersoBundle\Entity\Person
     */
    public function getPerson()
    {
        return $this->person;
    }

    /**
     * Set CityFrom
     *
     * @param \Rbins\PersoBundle\Entity\City $cityFrom
     * @return Transport
     */
    public function setCityFrom(\Rbins\PersoBundle\Entity\City $cityFrom = null)
    {
        $this->CityFrom = $cityFrom;
    
        return $this;
    }

    /**
     * Get CityFrom
     *
     * @return \Rbins\PersoBundle\Entity\City
     */
    public function getCityFrom()
    {
        return $this->CityFrom;
    }

    /**
     * Set CityTo
     *
     * @param \Rbins\PersoBundle\Entity\City $cityTo
     * @return Transport
     */
    public function setCityTo(\Rbins\PersoBundle\Entity\City $cityTo = null)
    {
        $this->CityTo = $cityTo;
    
        return $this;
    }

    /**
     * Get CityTo
     *
     * @return \Rbins\PersoBundle\Entity\City
     */
    public function getCityTo()
    {
        return $this->CityTo;
    }
    /**
     * @var boolean $giveback
     */
    private $giveback;


    /**
     * Set giveback
     *
     * @param boolean $giveback
     * @return Transport
     */
    public function setGiveback($giveback)
    {
        $this->giveback = $giveback;
    
        return $this;
    }

    /**
     * Get giveback
     *
     * @return boolean 
     */
    public function getGiveback()
    {
        return $this->giveback;
    }
    /**
     * @var string
     */
    private $numberplate;

    /**
     * @var string
     */
    private $comment;


    /**
     * Set numbertplate
     *
     * @param string $numberplate
     * @return Transport
     */
    public function setNumberplate($numberplate)
    {
        $this->numberplate = $numberplate;
    
        return $this;
    }

    /**
     * Get numberplate
     *
     * @return string 
     */
    public function getNumberplate()
    {
        return $this->numberplate;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * @return Transport
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    
        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }
    /**
     * @var \DateTime
     */
    private $parking_card_date;


    /**
     * Set parking_card_date
     *
     * @param \DateTime $parkingCardDate
     * @return Transport
     */
    public function setParkingCardDate($parkingCardDate)
    {
        $this->parking_card_date = $parkingCardDate;
    
        return $this;
    }

    /**
     * Get parking_card_date
     *
     * @return \DateTime 
     */
    public function getParkingCardDate()
    {
        return $this->parking_card_date;
    }
    
    /** parking_card and numberplace validator **/
    public function isCheckParkingCard()
    {
        if (!$this->car && $this->numberplate) return true;
        return false ;
    }

    public function isCheckNumberplate()
    {
      if ($this->numberplate== "" && $this->parking_card_date) return true ;
      return false ;
    }    
}
