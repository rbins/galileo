<?php

namespace Rbins\PersoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Rbins\PersoBundle\Entity\PersonEntry
 */
class PersonEntry
{
    /**
     * @var integer $id
     */
    private $id;

    /**
     * @var \DateTime $entry_date
     */
    private $entry_date;

    /**
     * @var \DateTime $exit_date
     */
    private $exit_date;

    /**
     * @var \Rbins\PersoBundle\Entity\Person
     */
    private $Person;

    /**
     * @var \Rbins\PersoBundle\Entity\ExitReason
     */
    private $exitReason;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set entry_date
     *
     * @param \DateTime $entryDate
     * @return PersonEntry
     */
    public function setEntryDate($entryDate)
    {
        $this->entry_date = $entryDate;
    
        return $this;
    }

    /**
     * Get entry_date
     *
     * @return \DateTime 
     */
    public function getEntryDate()
    {
        return $this->entry_date;
    }

    /**
     * Set exit_date
     *
     * @param \DateTime $exitDate
     * @return PersonEntry
     */
    public function setExitDate($exitDate)
    {
        $this->exit_date = $exitDate;
    
        return $this;
    }

    /**
     * Get exit_date
     *
     * @return \DateTime 
     */
    public function getExitDate()
    {
        return $this->exit_date;
    }

    /** end_date validator **/
    public function isExit_date()
    {
      if (!$this->exit_date) return true ;
      return ($this->entry_date <= $this->exit_date) ;
    }

    /**
     * Set Person
     *
     * @param \Rbins\PersoBundle\Entity\Person $person
     * @return PersonEntry
     */
    public function setPerson(\Rbins\PersoBundle\Entity\Person $person = null)
    {
        $this->Person = $person;
    
        return $this;
    }

    /**
     * Get Person
     *
     * @return \Rbins\PersoBundle\Entity\Person
     */
    public function getPerson()
    {
        return $this->Person;
    }

    /**
     * Set exitReason
     *
     * @param \Rbins\PersoBundle\Entity\ExitReason $exitReason
     * @return PersonEntry
     */
    public function setExitReason(\Rbins\PersoBundle\Entity\ExitReason $exitReason = null)
    {
        $this->exitReason = $exitReason;
    
        return $this;
    }

    /**
     * Get exitReason
     *
     * @return \Rbins\PersoBundle\Entity\ExitReason
     */
    public function getExitReason()
    {
        return $this->exitReason;
    }
}
