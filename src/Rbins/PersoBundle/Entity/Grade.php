<?php

namespace Rbins\PersoBundle\Entity;


class Grade extends Catalogue
{

    /**
     * @var string $code
     */
    private $code;

    /**
     * @var string $name_fr
     */
    private $name_fr;

    /**
     * @var string $name_nl
     */
    private $name_nl;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    private $Grade;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->Grade = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Set code
     *
     * @param string $code
     * @return Grade
     */
    public function setCode($code)
    {
        $this->code = $code;
    
        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set name_fr
     *
     * @param string $nameFr
     * @return Grade
     */
    public function setNameFr($nameFr)
    {
        $this->name_fr = $nameFr;
    
        return $this;
    }

    /**
     * Get name_fr
     *
     * @return string 
     */
    public function getNameFr()
    {
        return $this->name_fr;
    }

    /**
     * Set name_nl
     *
     * @param string $nameNl
     * @return Grade
     */
    public function setNameNl($nameNl)
    {
        $this->name_nl = $nameNl;
    
        return $this;
    }

    /**
     * Get name_nl
     *
     * @return string 
     */
    public function getNameNl()
    {
        return $this->name_nl;
    }

    /**
     * Add Grade
     *
     * @param \Rbins\PersoBundle\Entity\WorkingHist $grade
     * @return Grade
     */
    public function addGrade(\Rbins\PersoBundle\Entity\WorkingHist $grade)
    {
        $this->Grade[] = $grade;
    
        return $this;
    }

    /**
     * Remove Grade
     *
     * @param \Rbins\PersoBundle\Entity\WorkingHist $grade
     */
    public function removeGrade(\Rbins\PersoBundle\Entity\WorkingHist $grade)
    {
        $this->Grade->removeElement($grade);
    }

    /**
     * Get Grade
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGrade()
    {
        return $this->Grade;
    }

    public function toJson()
    {
      return json_encode(array_merge(get_object_vars($this), array('id'=>$this->getId())));
    }
    /**
     * @var string
     */
    private $level;


    /**
     * Set level
     *
     * @param string $level
     * @return Grade
     */
    public function setLevel($level)
    {
        $this->level = $level;
    
        return $this;
    }

    /**
     * Get level
     *
     * @return string 
     */
    public function getLevel()
    {
        return $this->level;
    }
}
