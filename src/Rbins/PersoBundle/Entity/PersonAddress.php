<?php

namespace Rbins\PersoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Rbins\PersoBundle\Entity\PersonAddress
 */
class PersonAddress
{
    /**
     * @var integer $id
     */
    private $id;

    /**
     * @var integer $person_ref
     */
    private $person_ref;

    /**
     * @var integer $city_ref
     */
    private $city_ref;

    /**
     * @var string $street
     */
    private $street;

    /**
     * @var string $street_num
     */
    private $street_num;

    /**
     * @var integer $box
     */
    private $box;

    /**
     * @var string $type
     */
    private $type;

    /**
     * @var \Rbins\PersoBundle\Entity\Person
     */
    private $Person;

    /**
     * @var \Rbins\PersoBundle\Entity\City
     */
    private $City;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set person_ref
     *
     * @param integer $personRef
     * @return PersonAddress
     */
    public function setPersonRef($personRef)
    {
        $this->person_ref = $personRef;
    
        return $this;
    }

    /**
     * Get person_ref
     *
     * @return integer 
     */
    public function getPersonRef()
    {
        return $this->person_ref;
    }

    /**
     * Set city_ref
     *
     * @param integer $cityRef
     * @return PersonAddress
     */
    public function setCityRef($cityRef)
    {
        $this->city_ref = $cityRef;
    
        return $this;
    }

    /**
     * Get city_ref
     *
     * @return integer 
     */
    public function getCityRef()
    {
        return $this->city_ref;
    }

    /**
     * Set street
     *
     * @param string $street
     * @return PersonAddress
     */
    public function setStreet($street)
    {
        $this->street = $street;
    
        return $this;
    }

    /**
     * Get street
     *
     * @return string 
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Set street_num
     *
     * @param string $streetNum
     * @return PersonAddress
     */
    public function setStreetNum($streetNum)
    {
        $this->street_num = $streetNum;
    
        return $this;
    }

    /**
     * Get street_num
     *
     * @return string 
     */
    public function getStreetNum()
    {
        return $this->street_num;
    }

    /**
     * Set box
     *
     * @param integer $box
     * @return PersonAddress
     */
    public function setBox($box)
    {
        $this->box = $box;
    
        return $this;
    }

    /**
     * Get box
     *
     * @return integer 
     */
    public function getBox()
    {
        return $this->box;
    }

    /**
     * Set type
     *
     * @param string type
     * @return PersonAddress
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set Person
     *
     * @param \Rbins\PersoBundle\Entity\Person $person
     * @return PersonAddress
     */
    public function setPerson(\Rbins\PersoBundle\Entity\Person $person = null)
    {
        $this->Person = $person;
    
        return $this;
    }

    /**
     * Get Person
     *
     * @return \Rbins\PersoBundle\Entity\Person
     */
    public function getPerson()
    {
        return $this->Person;
    }

    /**
     * Set City
     *
     * @param \Rbins\PersoBundle\Entity\City $city
     * @return PersonAddress
     */
    public function setCity(\Rbins\PersoBundle\Entity\City $city = null)
    {
        $this->City = $city;
    
        return $this;
    }

    /**
     * Get City
     *
     * @return \Rbins\PersoBundle\Entity\City
     */
    public function getCity()
    {
        return $this->City;
    }
    /**
     * @var string
     */
    private $comment;


    /**
     * Set comment
     *
     * @param string $comment
     * @return PersonAddress
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    
        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }
    /**
     * @var string
     */
    private $building;

    /**
     * @var string
     */
    private $building_floor;


    /**
     * Set building
     *
     * @param string $building
     * @return PersonAddress
     */
    public function setBuilding($building)
    {
        $this->building = $building;
    
        return $this;
    }

    /**
     * Get building
     *
     * @return string 
     */
    public function getBuilding()
    {
        return $this->building;
    }

    /**
     * Set building_floor
     *
     * @param string $buildingFloor
     * @return PersonAddress
     */
    public function setBuildingFloor($buildingFloor)
    {
        $this->building_floor = $buildingFloor;
    
        return $this;
    }

    /**
     * Get building_floor
     *
     * @return string 
     */
    public function getBuildingFloor()
    {
        return $this->building_floor;
    }
}
