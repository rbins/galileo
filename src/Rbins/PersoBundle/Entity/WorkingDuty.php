<?php

namespace Rbins\PersoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Rbins\PersoBundle\Entity\Person;
use Symfony\Component\Validator\ExecutionContext;
/**
 * Rbins\PersoBundle\Entity\WorkingDuty
 */
class WorkingDuty
{
 
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $percentage;

    /**
     * @var \DateTime
     */
    private $start_date;

    /**
     * @var \DateTime
     */
    private $end_date;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $Calendar;

    /**
     * @var \Rbins\PersoBundle\Entity\Person
     */
    private $Person;

    /**
     * @var \Rbins\PersoBundle\Entity\Department
     */
    private $Department;

    /**
     * @var \Rbins\PersoBundle\Entity\Cause
     */
    private $Cause;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->Calendar = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set percentage
     *
     * @param integer $percentage
     * @return WorkingDuty
     */
    public function setPercentage($percentage)
    {
        $this->percentage = $percentage;
    
        return $this;
    }

    /**
     * Get percentage
     *
     * @return integer 
     */
    public function getPercentage()
    {
        return $this->percentage;
    }

    /**
     * Set start_date
     *
     * @param \DateTime $startDate
     * @return WorkingDuty
     */
    public function setStartDate($startDate)
    {
        $this->start_date = $startDate;
    
        return $this;
    }

    /**
     * Get start_date
     *
     * @return \DateTime 
     */
    public function getStartDate()
    {
        return $this->start_date;
    }

    /**
     * Set end_date
     *
     * @param \DateTime $endDate
     * @return WorkingDuty
     */
    public function setEndDate($endDate)
    {
        $this->end_date = $endDate;
    
        return $this;
    }

    /**
     * Get end_date
     *
     * @return \DateTime 
     */
    public function getEndDate()
    {
        return $this->end_date;
    }

    /** end_date validator **/
    public function isEnd_date()
    {
      if (!$this->end_date) return true ;
      return ($this->start_date <= $this->end_date) ;
    }

     /**
     * Add Calendar
     *
     * @param \Rbins\PersoBundle\Entity\DutyCalendar $calendar
     * @return WorkingDuty
     */
    public function addCalendar(\Rbins\PersoBundle\Entity\DutyCalendar $calendar)
    {
        $calendar->setWorkingDuty($this);
        $this->Calendar[] = $calendar;
        return $this;
    }

    /**
     * Remove Calendar
     *
     * @param \Rbins\PersoBundle\Entity\DutyCalendar $calendar
     */
    public function removeCalendar(\Rbins\PersoBundle\Entity\DutyCalendar $calendar)
    {
        $this->Calendar->removeElement($calendar);
    }

    /**
     * Get Calendar
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCalendar()
    {
        return $this->Calendar;
    }

    /**
     * Set Person
     *
     * @param \Rbins\PersoBundle\Entity\Person $person
     * @return WorkingDuty
     */
    public function setPerson(\Rbins\PersoBundle\Entity\Person $person = null)
    {
        $this->Person = $person;
    
        return $this;
    }

    /**
     * Get Person
     *
     * @return \Rbins\PersoBundle\Entity\Person 
     */
    public function getPerson()
    {
        return $this->Person;
    }

    /**
     * Set Department
     *
     * @param \Rbins\PersoBundle\Entity\Department $department
     * @return WorkingDuty
     */
    public function setDepartment(\Rbins\PersoBundle\Entity\Department $department = null)
    {
        $this->Department = $department;
        return $this;
    }

    /**
     * Get Department
     *
     * @return \Rbins\PersoBundle\Entity\Department
     */
    public function getDepartment()
    {
        return $this->Department;
    }

    /**
     * Set Cause
     *
     * @param \Rbins\PersoBundle\Entity\Cause $cause
     * @return WorkingDuty
     */
    public function setCause(\Rbins\PersoBundle\Entity\Cause $cause = null)
    {
        $this->Cause = $cause;
    
        return $this;
    }

    /**
     * Get Cause
     *
     * @return \Rbins\PersoBundle\Entity\Cause 
     */
    public function getCause()
    {
        return $this->Cause;
    }
    /**
     * @var string
     */
    private $comment;


    /**
     * Set comment
     *
     * @param string $comment
     * @return WorkingDuty
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    
        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }
}
