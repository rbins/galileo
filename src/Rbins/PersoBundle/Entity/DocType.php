<?php

namespace Rbins\PersoBundle\Entity;


class DocType extends Catalogue
{

    /**
     * @var string $name_fr
     */
    private $name_fr;

    /**
     * @var string $name_nl
     */
    private $name_nl;
    
    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    private $Document;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->Document = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    public function getLabel($locale) {
      $fct = 'getName'.$locale;
      return $this->$fct();
    }
    
    /**
     * Set name_fr
     *
     * @param string $nameFr
     * @return DocType
     */
    public function setNameFr($nameFr)
    {
        $this->name_fr = $nameFr;
    
        return $this;
    }

    /**
     * Get name_fr
     *
     * @return string 
     */
    public function getNameFr()
    {
        return $this->name_fr;
    }

    /**
     * Set name_nl
     *
     * @param string $nameNl
     * @return DocType
     */
    public function setNameNl($nameNl)
    {
        $this->name_nl = $nameNl;
    
        return $this;
    }

    /**
     * Get name_nl
     *
     * @return string 
     */
    public function getNameNl()
    {
        return $this->name_nl;
    }

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $DocType;


    /**
     * Add DocType
     *
     * @param \Rbins\PersoBundle\Entity\Document $docType
     * @return DocType
     */
    public function addDocType(\Rbins\PersoBundle\Entity\Document $docType)
    {
        $this->DocType[] = $docType;
    
        return $this;
    }

    /**
     * Remove DocType
     *
     * @param \Rbins\PersoBundle\Entity\Document $docType
     */
    public function removeDocType(\Rbins\PersoBundle\Entity\Document $docType)
    {
        $this->DocType->removeElement($docType);
    }

    /**
     * Get DocType
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDocType()
    {
        return $this->DocType;
    }
}
