<?php

namespace Rbins\PersoBundle\Entity;


class Status extends Catalogue
{

    /**
     * @var string $code
     */
    private $code;

    /**
     * @var string $name_fr
     */
    private $name_fr;

    /**
     * @var string $name_nl
     */
    private $name_nl;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    private $Status;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->Status = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Set code
     *
     * @param string $code
     * @return Status
     */
    public function setCode($code)
    {
        $this->code = $code;
    
        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set name_fr
     *
     * @param string $nameFr
     * @return Status
     */
    public function setNameFr($nameFr)
    {
        $this->name_fr = $nameFr;
    
        return $this;
    }

    /**
     * Get name_fr
     *
     * @return string 
     */
    public function getNameFr()
    {
        return $this->name_fr;
    }

    /**
     * Set name_nl
     *
     * @param string $nameNl
     * @return Status
     */
    public function setNameNl($nameNl)
    {
        $this->name_nl = $nameNl;
    
        return $this;
    }

    /**
     * Get name_nl
     *
     * @return string 
     */
    public function getNameNl()
    {
        return $this->name_nl;
    }

    /**
     * Add status
     *
     * @param \Rbins\PersoBundle\Entity\WorkingHist $status
     *
     * @return Status
     */
    public function addStatus(\Rbins\PersoBundle\Entity\WorkingHist $status)
    {
        $this->Status[] = $status;

        return $this;
    }

    /**
     * Remove status
     *
     * @param \Rbins\PersoBundle\Entity\WorkingHist $status
     */
    public function removeStatus(\Rbins\PersoBundle\Entity\WorkingHist $status)
    {
        $this->Status->removeElement($status);
    }

    /**
     * Get Status
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getStatus()
    {
        return $this->Status;
    }

}
