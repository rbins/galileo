<?php

namespace Rbins\PersoBundle\Entity;


class Degree extends Catalogue
{

    /**
     * @var string
     */
    private $code;

    /**
     * @var string
     */
    private $name_fr;

    /**
     * @var string
     */
    private $name_nl;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $Person;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->Person = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Set code
     *
     * @param string $code
     * @return Degree
     */
    public function setCode($code)
    {
        $this->code = $code;
    
        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set name_fr
     *
     * @param string $nameFr
     * @return Degree
     */
    public function setNameFr($nameFr)
    {
        $this->name_fr = $nameFr;
    
        return $this;
    }

    /**
     * Get name_fr
     *
     * @return string 
     */
    public function getNameFr()
    {
        return $this->name_fr;
    }

    /**
     * Set name_nl
     *
     * @param string $nameNl
     * @return Degree
     */
    public function setNameNl($nameNl)
    {
        $this->name_nl = $nameNl;
    
        return $this;
    }

    /**
     * Get name_nl
     *
     * @return string 
     */
    public function getNameNl()
    {
        return $this->name_nl;
    }

    /**
     * Add Person
     *
     * @param \Rbins\PersoBundle\Entity\Person $person
     * @return Degree
     */
    public function addPerson(\Rbins\PersoBundle\Entity\Person $person)
    {
        $this->Person[] = $person;
    
        return $this;
    }

    /**
     * Remove Person
     *
     * @param \Rbins\PersoBundle\Entity\Person $person
     */
    public function removePerson(\Rbins\PersoBundle\Entity\Person $person)
    {
        $this->Person->removeElement($person);
    }

    /**
     * Get Person
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPerson()
    {
        return $this->Person;
    }
}
