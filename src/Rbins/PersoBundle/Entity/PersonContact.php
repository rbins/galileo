<?php

namespace Rbins\PersoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Rbins\PersoBundle\Entity\PersonContact
 */
class PersonContact
{
    /**
     * @var integer $id
     */
    private $id;

    /**
     * @var string $type
     */
    private $type;

    /**
     * @var string $entry
     */
    private $entry;

    /**
     * @var boolean $prefered
     */
    private $prefered;

    /**
     * @var \Rbins\PersoBundle\Entity\Person
     */
    private $Person;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return PersonContact
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set entry
     *
     * @param string $entry
     * @return PersonContact
     */
    public function setEntry($entry)
    {
        $this->entry = $entry;
    
        return $this;
    }

    /**
     * Get entry
     *
     * @return string 
     */
    public function getEntry()
    {
        return $this->entry;
    }

    /**
     * Set prefered
     *
     * @param boolean $prefered
     * @return PersonContact
     */
    public function setPrefered($prefered)
    {
        $this->prefered = $prefered;
    
        return $this;
    }

    /**
     * Get prefered
     *
     * @return boolean 
     */
    public function getPrefered()
    {
        return $this->prefered;
    }

    /**
     * Set Person
     *
     * @param \Rbins\PersoBundle\Entity\Person $person
     * @return PersonContact
     */
    public function setPerson(\Rbins\PersoBundle\Entity\Person $person = null)
    {
        $this->Person = $person;
    
        return $this;
    }

    /**
     * Get Person
     *
     * @return \Rbins\PersoBundle\Entity\Person
     */
    public function getPerson()
    {
        return $this->Person;
    }
    /**
     * @var string
     */
    private $comment;


    /**
     * Set comment
     *
     * @param string $comment
     * @return PersonContact
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    
        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }
}
