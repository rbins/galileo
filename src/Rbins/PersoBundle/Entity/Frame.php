<?php

namespace Rbins\PersoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Frame
 */
class Frame extends Catalogue
{
    /**
     * @var string
     */
    private $code;

    /**
     * @var string
     */
    private $name_fr;

    /**
     * @var string
     */
    private $name_nl;

    /**
     * @var string
     */
    private $level;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $Frame;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->Frame = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set code
     *
     * @param string $code
     * @return Frame
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set name_fr
     *
     * @param string $nameFr
     * @return Frame
     */
    public function setNameFr($nameFr)
    {
        $this->name_fr = $nameFr;

        return $this;
    }

    /**
     * Get name_fr
     *
     * @return string
     */
    public function getNameFr()
    {
        return $this->name_fr;
    }

    /**
     * Set name_nl
     *
     * @param string $nameNl
     * @return Frame
     */
    public function setNameNl($nameNl)
    {
        $this->name_nl = $nameNl;

        return $this;
    }

    /**
     * Get name_nl
     *
     * @return string
     */
    public function getNameNl()
    {
        return $this->name_nl;
    }

    /**
     * Set level
     *
     * @param string $level
     * @return Frame
     */
    public function setLevel($level)
    {
        $this->level = $level;

        return $this;
    }

    /**
     * Get level
     *
     * @return string
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * Add Frame
     *
     * @param \Rbins\PersoBundle\Entity\Person $frame
     * @return Frame
     */
    public function addFrame(\Rbins\PersoBundle\Entity\Person $frame)
    {
        $this->Frame[] = $frame;

        return $this;
    }

    /**
     * Remove Frame
     *
     * @param \Rbins\PersoBundle\Entity\Person $frame
     */
    public function removeFrame(\Rbins\PersoBundle\Entity\Person $frame)
    {
        $this->Frame->removeElement($frame);
    }

    /**
     * Get Frame
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFrame()
    {
        return $this->Frame;
    }

    public function toJson()
    {
      return json_encode(array_merge(get_object_vars($this), array('id'=>$this->getId())));
    }
}
