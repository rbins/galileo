<?php

namespace Rbins\PersoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Rbins\PersoBundle\Entity\DutyCalendar
 */
class DutyCalendar
{
    /**
     * @var integer $id
     */
    private $id;

    /**
     * @var integer $week
     */
    private $week;

    /**
     * @var boolean $mon_am
     */
    private $mon_am = true;

    /**
     * @var boolean $mon_pm
     */
    private $mon_pm = true;

    /**
     * @var boolean $tue_am
     */
    private $tue_am = true;

    /**
     * @var boolean $tue_pm
     */
    private $tue_pm = true;

    /**
     * @var boolean $wed_am
     */
    private $wed_am = true;

    /**
     * @var boolean $wed_pm
     */
    private $wed_pm = true;

    /**
     * @var boolean $thu_am
     */
    private $thu_am = true;

    /**
     * @var boolean $thu_pm
     */
    private $thu_pm = true;

    /**
     * @var boolean $fri_am
     */
    private $fri_am = true;

    /**
     * @var boolean $fri_pm
     */
    private $fri_pm = true;

    /**
     * @var boolean $sat_am
     */
    private $sat_am;

    /**
     * @var boolean $sat_pm
     */
    private $sat_pm;

    /**
     * @var boolean $sun_am
     */
    private $sun_am;

    /**
     * @var boolean $sun_pm
     */
    private $sun_pm;

    /**
     * @var \Rbins\PersoBundle\Entity\WorkingDuty
     */
    private $WorkingDuty;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set week
     *
     * @param integer $week
     * @return DutyCalendar
     */
    public function setWeek($week)
    {
        $this->week = $week;
    
        return $this;
    }

    /**
     * Get week
     *
     * @return integer 
     */
    public function getWeek()
    {
        return $this->week;
    }

    /**
     * Set mon_am
     *
     * @param boolean $monAm
     * @return DutyCalendar
     */
    public function setMonAm($monAm)
    {
        $this->mon_am = $monAm;
    
        return $this;
    }

    /**
     * Get mon_am
     *
     * @return boolean 
     */
    public function getMonAm()
    {
        return $this->mon_am;
    }

    /**
     * Set mon_pm
     *
     * @param boolean $monPm
     * @return DutyCalendar
     */
    public function setMonPm($monPm)
    {
        $this->mon_pm = $monPm;
    
        return $this;
    }

    /**
     * Get mon_pm
     *
     * @return boolean 
     */
    public function getMonPm()
    {
        return $this->mon_pm;
    }

    /**
     * Set tue_am
     *
     * @param boolean $tueAm
     * @return DutyCalendar
     */
    public function setTueAm($tueAm)
    {
        $this->tue_am = $tueAm;
    
        return $this;
    }

    /**
     * Get tue_am
     *
     * @return boolean 
     */
    public function getTueAm()
    {
        return $this->tue_am;
    }

    /**
     * Set tue_pm
     *
     * @param boolean $tuePm
     * @return DutyCalendar
     */
    public function setTuePm($tuePm)
    {
        $this->tue_pm = $tuePm;
    
        return $this;
    }

    /**
     * Get tue_pm
     *
     * @return boolean 
     */
    public function getTuePm()
    {
        return $this->tue_pm;
    }

    /**
     * Set wed_am
     *
     * @param boolean $wedAm
     * @return DutyCalendar
     */
    public function setWedAm($wedAm)
    {
        $this->wed_am = $wedAm;
    
        return $this;
    }

    /**
     * Get wed_am
     *
     * @return boolean 
     */
    public function getWedAm()
    {
        return $this->wed_am;
    }

    /**
     * Set wed_pm
     *
     * @param boolean $wedPm
     * @return DutyCalendar
     */
    public function setWedPm($wedPm)
    {
        $this->wed_pm = $wedPm;
    
        return $this;
    }

    /**
     * Get wed_pm
     *
     * @return boolean 
     */
    public function getWedPm()
    {
        return $this->wed_pm;
    }

    /**
     * Set thu_am
     *
     * @param boolean $thuAm
     * @return DutyCalendar
     */
    public function setThuAm($thuAm)
    {
        $this->thu_am = $thuAm;
    
        return $this;
    }

    /**
     * Get thu_am
     *
     * @return boolean 
     */
    public function getThuAm()
    {
        return $this->thu_am;
    }

    /**
     * Set thu_pm
     *
     * @param boolean $thuPm
     * @return DutyCalendar
     */
    public function setThuPm($thuPm)
    {
        $this->thu_pm = $thuPm;
    
        return $this;
    }

    /**
     * Get thu_pm
     *
     * @return boolean 
     */
    public function getThuPm()
    {
        return $this->thu_pm;
    }

    /**
     * Set fri_am
     *
     * @param boolean $friAm
     * @return DutyCalendar
     */
    public function setFriAm($friAm)
    {
        $this->fri_am = $friAm;
    
        return $this;
    }

    /**
     * Get fri_am
     *
     * @return boolean 
     */
    public function getFriAm()
    {
        return $this->fri_am;
    }

    /**
     * Set fri_pm
     *
     * @param boolean $friPm
     * @return DutyCalendar
     */
    public function setFriPm($friPm)
    {
        $this->fri_pm = $friPm;
    
        return $this;
    }

    /**
     * Get fri_pm
     *
     * @return boolean 
     */
    public function getFriPm()
    {
        return $this->fri_pm;
    }

    /**
     * Set sat_am
     *
     * @param boolean $satAm
     * @return DutyCalendar
     */
    public function setSatAm($satAm)
    {
        $this->sat_am = $satAm;
    
        return $this;
    }

    /**
     * Get sat_am
     *
     * @return boolean 
     */
    public function getSatAm()
    {
        return $this->sat_am;
    }

    /**
     * Set sat_pm
     *
     * @param boolean $satPm
     * @return DutyCalendar
     */
    public function setSatPm($satPm)
    {
        $this->sat_pm = $satPm;
    
        return $this;
    }

    /**
     * Get sat_pm
     *
     * @return boolean 
     */
    public function getSatPm()
    {
        return $this->sat_pm;
    }

    /**
     * Set sun_am
     *
     * @param boolean $sunAm
     * @return DutyCalendar
     */
    public function setSunAm($sunAm)
    {
        $this->sun_am = $sunAm;
    
        return $this;
    }

    /**
     * Get sun_am
     *
     * @return boolean 
     */
    public function getSunAm()
    {
        return $this->sun_am;
    }

    /**
     * Set sun_pm
     *
     * @param boolean $sunPm
     * @return DutyCalendar
     */
    public function setSunPm($sunPm)
    {
        $this->sun_pm = $sunPm;
    
        return $this;
    }

    /**
     * Get sun_pm
     *
     * @return boolean 
     */
    public function getSunPm()
    {
        return $this->sun_pm;
    }

    /**
     * Set WorkingDuty
     *
     * @param \Rbins\PersoBundle\Entity\WorkingDuty $workingDuty
     * @return DutyCalendar
     */
    public function setWorkingDuty(\Rbins\PersoBundle\Entity\WorkingDuty $workingDuty = null)
    {
        $this->WorkingDuty = $workingDuty;
    
        return $this;
    }

    /**
     * Get WorkingDuty
     *
     * @return \Rbins\PersoBundle\Entity\WorkingDuty 
     */
    public function getWorkingDuty()
    {
        return $this->WorkingDuty;
    }
}
