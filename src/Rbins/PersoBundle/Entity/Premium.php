<?php

namespace Rbins\PersoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Rbins\PersoBundle\Entity\Premium
 */
class Premium
{
    /**
     * @var integer $id
     */
    private $id;

    /**
     * @var \DateTime $start_date
     */
    private $start_date;

    /**
     * @var \DateTime $end_date
     */
    private $end_date;

    /**
     * @var \Rbins\PersoBundle\Entity\Person
     */
    private $person;

    /**
     * @var \Rbins\PersoBundle\Entity\CataloguePremium
     */
    private $premium;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set start_date
     *
     * @param \DateTime $startDate
     * @return Premium
     */
    public function setStartDate($startDate)
    {
        $this->start_date = $startDate;
    
        return $this;
    }

    /**
     * Get start_date
     *
     * @return \DateTime 
     */
    public function getStartDate()
    {
        return $this->start_date;
    }

    /**
     * Set end_date
     *
     * @param \DateTime $endDate
     * @return Premium
     */
    public function setEndDate($endDate)
    {
        $this->end_date = $endDate;
    
        return $this;
    }

    /**
     * Get end_date
     *
     * @return \DateTime 
     */
    public function getEndDate()
    {
        return $this->end_date;
    }
    
    /** end_date validator **/
    public function isEnd_date()
    {          
      return ($this->start_date <= $this->end_date) ;
    }   
    

    /**
     * Set person
     *
     * @param \Rbins\PersoBundle\Entity\Person $person
     * @return Premium
     */
    public function setPerson(\Rbins\PersoBundle\Entity\Person $person = null)
    {
        $this->person = $person;
    
        return $this;
    }

    /**
     * Get person
     *
     * @return \Rbins\PersoBundle\Entity\Person
     */
    public function getPerson()
    {
        return $this->person;
    }

    /**
     * Set premium
     *
     * @param \Rbins\PersoBundle\Entity\CataloguePremium $premium
     * @return Premium
     */
    public function setPremium(\Rbins\PersoBundle\Entity\CataloguePremium $premium = null)
    {
        $this->premium = $premium;
    
        return $this;
    }

    /**
     * Get premium
     *
     * @return \Rbins\PersoBundle\Entity\CataloguePremium
     */
    public function getPremium()
    {
        return $this->premium;
    }
    /**
     * @var string
     */
    private $comment;


    /**
     * Set comment
     *
     * @param string $comment
     * @return Premium
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    
        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }
}
