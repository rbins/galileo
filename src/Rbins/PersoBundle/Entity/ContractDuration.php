<?php

namespace Rbins\PersoBundle\Entity;


class ContractDuration extends Catalogue
{

    /**
     * @var string $code
     */
    private $code;

    /**
     * @var string $name_fr
     */
    private $name_fr;

    /**
     * @var string $name_nl
     */
    private $name_nl;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    private $ContractDuration;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->ContractDuration = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Set code
     *
     * @param string $code
     * @return ContractDuration
     */
    public function setCode($code)
    {
        $this->code = $code;
    
        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set name_fr
     *
     * @param string $nameFr
     * @return ContractDuration
     */
    public function setNameFr($nameFr)
    {
        $this->name_fr = $nameFr;
    
        return $this;
    }

    /**
     * Get name_fr
     *
     * @return string 
     */
    public function getNameFr()
    {
        return $this->name_fr;
    }

    /**
     * Set name_nl
     *
     * @param string $nameNl
     * @return ContractDuration
     */
    public function setNameNl($nameNl)
    {
        $this->name_nl = $nameNl;
    
        return $this;
    }

    /**
     * Get name_nl
     *
     * @return string 
     */
    public function getNameNl()
    {
        return $this->name_nl;
    }

    /**
     * Add ContractDuration
     *
     * @param \Rbins\PersoBundle\Entity\WorkingHist $contractDuration
     * @return ContractDuration
     */
    public function addContractDuration(\Rbins\PersoBundle\Entity\WorkingHist $contractDuration)
    {
        $this->ContractDuration[] = $contractDuration;
    
        return $this;
    }

    /**
     * Remove ContractDuration
     *
     * @param \Rbins\PersoBundle\Entity\WorkingHist $contractDuration
     */
    public function removeContractDuration(\Rbins\PersoBundle\Entity\WorkingHist $contractDuration)
    {
        $this->ContractDuration->removeElement($contractDuration);
    }

    /**
     * Get ContractDuration
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getContractDuration()
    {
        return $this->ContractDuration;
    }
}
