<?php

namespace Rbins\PersoBundle\Entity;


class ContractComment extends Catalogue
{

    /**
     * @var string $code
     */
    private $code;

    /**
     * @var string $name_fr
     */
    private $name_fr;

    /**
     * @var string $name_nl
     */
    private $name_nl;
    
    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    private $WorkingHist;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->WorkingHist = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Set code
     *
     * @param string $code
     * @return ContractComment
     */
    public function setCode($code)
    {
        $this->code = $code;
    
        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set name_fr
     *
     * @param string $nameFr
     * @return ContractComment
     */
    public function setNameFr($nameFr)
    {
        $this->name_fr = $nameFr;
    
        return $this;
    }

    /**
     * Get name_fr
     *
     * @return string 
     */
    public function getNameFr()
    {
        return $this->name_fr;
    }

    /**
     * Set name_nl
     *
     * @param string $nameNl
     * @return ContractComment
     */
    public function setNameNl($nameNl)
    {
        $this->name_nl = $nameNl;
    
        return $this;
    }

    /**
     * Get name_nl
     *
     * @return string 
     */
    public function getNameNl()
    {
        return $this->name_nl;
    }

    /**
     * Add WorkingHist
     *
     * @param \Rbins\PersoBundle\Entity\ContractComment $workingHist
     * @return ContractComment
     */
    public function addWorkingHist(\Rbins\PersoBundle\Entity\ContractComment $workingHist)
    {
        $this->WorkingHist[] = $workingHist;
    
        return $this;
    }

    /**
     * Remove WorkingHist
     *
     * @param \Rbins\PersoBundle\Entity\ContractComment $workingHist
     */
    public function removeWorkingHist(\Rbins\PersoBundle\Entity\ContractComment $workingHist)
    {
        $this->WorkingHist->removeElement($workingHist);
    }

    /**
     * Get WorkingHist
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getWorkingHist()
    {
        return $this->WorkingHist;
    }
}
