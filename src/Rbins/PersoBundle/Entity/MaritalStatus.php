<?php

namespace Rbins\PersoBundle\Entity;


class MaritalStatus extends Catalogue
{

    /**
     * @var string $code
     */
    private $code;

    /**
     * @var string $name_fr
     */
    private $name_fr;

    /**
     * @var string $name_nl
     */
    private $name_nl;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    private $MaritalStatus;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->MaritalStatus = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Set code
     *
     * @param string $code
     * @return MaritalStatus
     */
    public function setCode($code)
    {
        $this->code = $code;
    
        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set name_fr
     *
     * @param string $nameFr
     * @return MaritalStatus
     */
    public function setNameFr($nameFr)
    {
        $this->name_fr = $nameFr;
    
        return $this;
    }

    /**
     * Get name_fr
     *
     * @return string 
     */
    public function getNameFr()
    {
        return $this->name_fr;
    }

    /**
     * Set name_nl
     *
     * @param string $nameNl
     * @return MaritalStatus
     */
    public function setNameNl($nameNl)
    {
        $this->name_nl = $nameNl;
    
        return $this;
    }

    /**
     * Get name_nl
     *
     * @return string 
     */
    public function getNameNl()
    {
        return $this->name_nl;
    }

    /**
     * Add maritalStatus
     *
     * @param \Rbins\PersoBundle\Entity\Person $maritalStatus
     *
     * @return MaritalStatus
     */
    public function addMaritalStatus(\Rbins\PersoBundle\Entity\Person $maritalStatus)
    {
        $this->MaritalStatus[] = $maritalStatus;

        return $this;
    }

    /**
     * Remove maritalStatus
     *
     * @param \Rbins\PersoBundle\Entity\Person $maritalStatus
     */
    public function removeMaritalStatus(\Rbins\PersoBundle\Entity\Person $maritalStatus)
    {
        $this->MaritalStatus->removeElement($maritalStatus);
    }

    /**
     * Get MaritalStatus
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMaritalStatus()
    {
        return $this->MaritalStatus;
    }

}
