<?php

namespace Rbins\PersoBundle\Entity;


class Country extends Catalogue
{

    /**
     * @var string $code
     */
    private $code;

    /**
     * @var string $name_fr
     */
    private $name_fr;

    /**
     * @var string $name_nl
     */
    private $name_nl;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    private $Country;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    private $ctry_persons;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->Country = new \Doctrine\Common\Collections\ArrayCollection();
        $this->ctry_persons = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Set code
     *
     * @param string $code
     * @return Country
     */
    public function setCode($code)
    {
        $this->code = $code;
    
        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set name_fr
     *
     * @param string $nameFr
     * @return Country
     */
    public function setNameFr($nameFr)
    {
        $this->name_fr = $nameFr;
    
        return $this;
    }

    /**
     * Get name_fr
     *
     * @return string 
     */
    public function getNameFr()
    {
        return $this->name_fr;
    }

    /**
     * Set name_nl
     *
     * @param string $nameNl
     * @return Country
     */
    public function setNameNl($nameNl)
    {
        $this->name_nl = $nameNl;
    
        return $this;
    }

    /**
     * Get name_nl
     *
     * @return string 
     */
    public function getNameNl()
    {
        return $this->name_nl;
    }

    /**
     * Add Country
     *
     * @param \Rbins\PersoBundle\Entity\City $country
     * @return Country
     */
    public function addCountry(\Rbins\PersoBundle\Entity\City $country)
    {
        $this->Country[] = $country;
    
        return $this;
    }

    /**
     * Remove Country
     *
     * @param \Rbins\PersoBundle\Entity\City $country
     */
    public function removeCountry(\Rbins\PersoBundle\Entity\City $country)
    {
        $this->Country->removeElement($country);
    }

    /**
     * Get Country
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCountry()
    {
        return $this->Country;
    }

    /**
     * Add ctry_persons
     *
     * @param \Rbins\PersoBundle\Entity\Person $ctryPersons
     * @return Country
     */
    public function addCtryPerson(\Rbins\PersoBundle\Entity\Person $ctryPersons)
    {
        $this->ctry_persons[] = $ctryPersons;
    
        return $this;
    }

    /**
     * Remove ctry_persons
     *
     * @param \Rbins\PersoBundle\Entity\Person $ctryPersons
     */
    public function removeCtryPerson(\Rbins\PersoBundle\Entity\Person $ctryPersons)
    {
        $this->ctry_persons->removeElement($ctryPersons);
    }

    /**
     * Get ctry_persons
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCtryPersons()
    {
        return $this->ctry_persons;
    }

    public function getCatalogueNameEn() {
        return $this->getNameEn();
    }
    public function getCatalogueNameFr() {
        return $this->getNameFr();
    }
    public function getCatalogueNameNl() {
        return $this->getNameNl();
    }
}
