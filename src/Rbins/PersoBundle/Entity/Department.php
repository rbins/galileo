<?php

namespace Rbins\PersoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Rbins\PersoBundle\Utils\Util as Util;
use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints\Choice;

/**
 * Rbins\PersoBundle\Entity\Department
 */
class Department
{
    /**
     * @var integer $id
     */
    private $id;

    /**
     * @var string $level
     */
    private $level;

    /**
     * @var string $path
     */
    private $path='/';

    /**
     * @var string $name_fr
     */
    private $name_fr;

    /**
     * @var string $name_en
     */
    private $name_en;

    /**
     * @var string $name_nl
     */
    private $name_nl;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    private $Children;

    /**
     * @var \Rbins\PersoBundle\Entity\Department
     */
    private $Parent;

    public function getFilter()
    {
      return  Util::unaccent(strtolower(
        $this->getLevel().
        $this->getNameFr().
        $this->getNameNl().
        $this->getNameEn()));
    }

    public function getCatalogueNameEn() {
      return $this->getNameEn();
    }
    public function getCatalogueNameFr() {
      return $this->getNameFr();
    }
    public function getCatalogueNameNl() {
      return $this->getNameNl();
    }

    public function getLabel($locale) {
      $fct = 'getName'.$locale;
      if($this->Parent)  return $this->$fct().' ('.$this->Parent->$fct().')';
      return $this->$fct();
    }

    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata->addGetterConstraint('ParentLegal', new IsTrue(array(
            'message' => 'entity.department.form.error.parent_self',
        )));

       $metadata->addPropertyConstraint('level', new Choice(array(
            'choices' => array('department', 'section'),
            'message' => 'entity.department.form.error.level',
        )));

        $metadata->addGetterConstraint('LevelLegal', new IsTrue(array(
            'message' => 'entity.department.form.error.level_parent',
        )));
    }

    public function isParentLegal()
    {
      if($this->getParent())
        return ($this->getParent()->getId() != $this->getId());
      return true;
    }
    public function isLevelLegal()
    {
      if($this->getParent())
        return ($this->getLevel() != 'department');
      else
       return ($this->getLevel() != 'section');
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->WorkingDuty = new \Doctrine\Common\Collections\ArrayCollection();
        $this->Children = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set level
     *
     * @param string $level
     * @return Department
     */
    public function setLevel($level)
    {
        $this->level = $level;
    
        return $this;
    }

    /**
     * Get level
     *
     * @return string 
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * Set path
     *
     * @param string $path
     * @return Department
     */
    public function setPath($path)
    {
        $this->path = $path;
    
        return $this;
    }

    /**
     * Get path
     *
     * @return string 
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set name_fr
     *
     * @param string $nameFr
     * @return Department
     */
    public function setNameFr($nameFr)
    {
        $this->name_fr = $nameFr;
    
        return $this;
    }

    /**
     * Get name_fr
     *
     * @return string 
     */
    public function getNameFr()
    {
        return $this->name_fr;
    }

    /**
     * Set name_en
     *
     * @param string $nameEn
     * @return Department
     */
    public function setNameEn($nameEn)
    {
        $this->name_en = $nameEn;
    
        return $this;
    }

    /**
     * Get name_en
     *
     * @return string 
     */
    public function getNameEn()
    {
        return $this->name_en;
    }

    /**
     * Set name_nl
     *
     * @param string $nameNl
     * @return Department
     */
    public function setNameNl($nameNl)
    {
        $this->name_nl = $nameNl;
    
        return $this;
    }

    /**
     * Get name_nl
     *
     * @return string 
     */
    public function getNameNl()
    {
        return $this->name_nl;
    }

    /**
     * Add Children
     *
     * @param \Rbins\PersoBundle\Entity\Department $children
     * @return Department
     */
    public function addChildren(\Rbins\PersoBundle\Entity\Department $children)
    {
        $this->Children[] = $children;
    
        return $this;
    }

    /**
     * Remove Children
     *
     * @param \Rbins\PersoBundle\Entity\Department $children
     */
    public function removeChildren(\Rbins\PersoBundle\Entity\Department $children)
    {
        $this->Children->removeElement($children);
    }

    /**
     * Get Children
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChildren()
    {
        return $this->Children;
    }

    /**
     * Set Parent
     *
     * @param \Rbins\PersoBundle\Entity\Department $parent
     * @return Department
     */
    public function setParent(\Rbins\PersoBundle\Entity\Department $parent = null)
    {
        $this->Parent = $parent;
    
        return $this;
    }

    /**
     * Get Parent
     *
     * @return \Rbins\PersoBundle\Entity\Department
     */
    public function getParent()
    {
        return $this->Parent;
    }
    
    public function __toString() {
      return $this->getNameFr().'-'.$this->getNameNl();
    }
    /**
     * @var boolean
     */
    private $is_active;


    /**
     * Set is_active
     *
     * @param boolean $isActive
     * @return Department
     */
    public function setIsActive($isActive)
    {
        $this->is_active = $isActive;
    
        return $this;
    }

    /**
     * Get is_active
     *
     * @return boolean 
     */
    public function getIsActive()
    {
        return $this->is_active;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $WorkingDuty;


    /**
     * Add WorkingDuty
     *
     * @param \Rbins\PersoBundle\Entity\WorkingDuty $workingDuty
     * @return Department
     */
    public function addWorkingDuty(\Rbins\PersoBundle\Entity\WorkingDuty $workingDuty)
    {
        $this->WorkingDuty[] = $workingDuty;
    
        return $this;
    }

    /**
     * Remove WorkingDuty
     *
     * @param \Rbins\PersoBundle\Entity\WorkingDuty $workingDuty
     */
    public function removeWorkingDuty(\Rbins\PersoBundle\Entity\WorkingDuty $workingDuty)
    {
        $this->WorkingDuty->removeElement($workingDuty);
    }

    /**
     * Get WorkingDuty
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getWorkingDuty()
    {
        return $this->WorkingDuty;
    }
    /**
     * @var string
     */
    private $code;


    /**
     * Set code
     *
     * @param string $code
     *
     * @return Department
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Add child
     *
     * @param \Rbins\PersoBundle\Entity\Department $child
     *
     * @return Department
     */
    public function addChild(\Rbins\PersoBundle\Entity\Department $child)
    {
        $this->Children[] = $child;

        return $this;
    }

    /**
     * Remove child
     *
     * @param \Rbins\PersoBundle\Entity\Department $child
     */
    public function removeChild(\Rbins\PersoBundle\Entity\Department $child)
    {
        $this->Children->removeElement($child);
    }
}
