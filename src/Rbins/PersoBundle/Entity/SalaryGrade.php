<?php

namespace Rbins\PersoBundle\Entity;


class SalaryGrade extends Catalogue
{

    /**
     * @var string $code
     */
    private $code;

    /**
     * @var string $ehr_code
     */
    private $ehr_code;

    /**
     * @var string $name_fr
     */
    private $name_fr;

    /**
     * @var string $name_nl
     */
    private $name_nl;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    private $SalaryGrade;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->SalaryGrade = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Set code
     *
     * @param string $code
     * @return SalaryGrade
     */
    public function setCode($code)
    {
        $this->code = $code;
    
        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set ehr_code
     *
     * @param string $ehrCode
     * @return SalaryGrade
     */
    public function setEhrCode($ehrCode)
    {
        $this->ehr_code = $ehrCode;
    
        return $this;
    }

    /**
     * Get ehr_code
     *
     * @return string 
     */
    public function getEhrCode()
    {
        return $this->ehr_code;
    }

    /**
     * Set name_fr
     *
     * @param string $nameFr
     * @return SalaryGrade
     */
    public function setNameFr($nameFr)
    {
        $this->name_fr = $nameFr;
    
        return $this;
    }

    /**
     * Get name_fr
     *
     * @return string 
     */
    public function getNameFr()
    {
        return $this->name_fr;
    }

    /**
     * Set name_nl
     *
     * @param string $nameNl
     * @return SalaryGrade
     */
    public function setNameNl($nameNl)
    {
        $this->name_nl = $nameNl;
    
        return $this;
    }

    /**
     * Get name_nl
     *
     * @return string 
     */
    public function getNameNl()
    {
        return $this->name_nl;
    }

    /**
     * Add SalaryGrade
     *
     * @param \Rbins\PersoBundle\Entity\WorkingHist $salaryGrade
     * @return SalaryGrade
     */
    public function addSalaryGrade(\Rbins\PersoBundle\Entity\WorkingHist $salaryGrade)
    {
        $this->SalaryGrade[] = $salaryGrade;
    
        return $this;
    }

    /**
     * Remove SalaryGrade
     *
     * @param \Rbins\PersoBundle\Entity\WorkingHist $salaryGrade
     */
    public function removeSalaryGrade(\Rbins\PersoBundle\Entity\WorkingHist $salaryGrade)
    {
        $this->SalaryGrade->removeElement($salaryGrade);
    }

    /**
     * Get SalaryGrade
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSalaryGrade()
    {
        return $this->SalaryGrade;
    }
    /**
     * @var string
     */
    private $level;


    /**
     * Set level
     *
     * @param string $level
     * @return SalaryGrade
     */
    public function setLevel($level)
    {
        $this->level = $level;
    
        return $this;
    }

    /**
     * Get level
     *
     * @return string 
     */
    public function getLevel()
    {
        return $this->level;
    }
}
