<?php
namespace Rbins\PersoBundle\Listener;

use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\Security\Core\SecurityContext;
use Doctrine\Bundle\DoctrineBundle\Registry as Doctrine; 
use \PDO;
use \Symfony\Component\HttpKernel\Event\GetResponseEvent;

class UserListener
{
    private $securityContext;
    private $em;

    public function onKernelRequest(GetResponseEvent $event)
    {
      if( ! $this->securityContext->getToken() || 'anon.' == $this->securityContext->getToken()->getUser()) return;

      $username = strtolower($this->securityContext->getToken()->getUser()->getUsername());
      $conn = $this->em->getConnection();
      $conn->query("select set_config('rbins.userid', ".$conn->quote($username).", false) ;");

      $version = $conn->getWrappedConnection()->getAttribute(PDO::ATTR_SERVER_VERSION);
      if(strpos($version,'8.') !== 0) {
        $conn->query("SET application_name = 'perso';") ;
      }

      if(function_exists('apache_note')) {
        apache_note('username', $username );
      }
    }

    public function __construct(SecurityContext $context, Doctrine $doctrine)
    {
      $this->securityContext = $context;
      $this->em = $doctrine->getManager();
    }
}