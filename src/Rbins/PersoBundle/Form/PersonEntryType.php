<?php

namespace Rbins\PersoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;

class PersonEntryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
    $date_opt = array(
        'widget' => 'single_text',
        'format' => 'dd/M/y',
        'attr' => array('placeholder'=>'dd/mm/yyyy'),
    );

        $builder
            ->add('entry_date', DateTimeType::class, $date_opt)
            ->add('exit_date', DateTimeType::class, $date_opt)
            ->add('exitReason')
        ;
      $builder->get('exit_date')->setRequired(false);
      $builder->get('exitReason')->setRequired(false);
    
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Rbins\PersoBundle\Entity\PersonEntry',
            'lang' => 'fr'
        ));
    }

    public function getBlockPrefix()
    {
        return 'rbins_persobundle_personentrytype';
    }
}
