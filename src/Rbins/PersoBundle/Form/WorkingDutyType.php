<?php

namespace Rbins\PersoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Rbins\PersoBundle\Form\Type\AutocompleteType;

class WorkingDutyType extends AbstractType
{
  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $date_opt = array(
        'widget' => 'single_text',
        'format' => 'dd/M/y',
        'attr' => array('placeholder'=>'dd/mm/yyyy'),
    );

    $builder
      ->add('percentage')
       ->add('start_date', DateTimeType::class, $date_opt)
      ->add('end_date', DateTimeType::class, $date_opt)
      ->add('comment',TextareaType::class, array('required'=>false))
      ->add('Department', AutocompleteType::class,array(
        'source_path' => 'complete_department',
        'class' => 'Department',
        'required' => true))
      ->add('Cause', AutocompleteType::class,array(
        'source_path' => 'complete_cause',
        'class' => 'Cause',
        'required' => false))
    ;

    $builder->get('end_date')->setRequired(false);
    $builder->get('percentage')->setData('100');
  }

  public function configureOptions(OptionsResolver $resolver) {
    $resolver->setDefaults(array(
      'data_class' => 'Rbins\PersoBundle\Entity\Workingduty',
    ));
  }

  public function getBlockPrefix() {
    return 'rbins_persobundle_workingdutytype';
  }
}
