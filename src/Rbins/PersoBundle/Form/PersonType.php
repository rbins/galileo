<?php

namespace Rbins\PersoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Rbins\PersoBundle\Form\Type\AutocompleteType;

class PersonType extends AbstractType
{
  public function buildForm(FormBuilderInterface $builder, array $options)
  {
      $this->lang = $options['lang'];

    $builder
      ->add('first_name',TextType::class,
        array('attr'=>array('class' =>'input-medium')))
      ->add('last_name',TextType::class,
        array('attr'=>array('class' =>'input-medium')))
      ->add('alias_first_name',TextType::class,
        array('required'=>false, 'attr'=> array('class'=>'input-medium')))
      ->add('alias_last_name',TextType::class,
        array('required'=>false, 'attr'=> array('class'=>'input-large')))
      ->add('mother_tongue',
          ChoiceType::class,
          array(
              'choices' => array(
                  'En'=>'EN',
                  'Fr'=>'FR',
                  'Nl'=>'NL',
                  'De'=>'DE'
              ),
              'choices_as_values'=>true
          )
      )
      ->add('dependance',CheckboxType::class,array('required'=> false))
      ->add('handicap',TextType::class,array('required'=> false))
      ->add('birth_date',DateTimeType::class, array(
          'widget' => 'single_text',
          'format' => 'dd/M/y',
          'attr' => array('placeholder'=>'dd/mm/yyyy'),
      ))
      ->add(
          'homework',ChoiceType::class,array(
              'choices' => array(
                  'form.rbins.persobundle.persontype.homework.none'=>'none',
                  'form.rbins.persobundle.persontype.homework.monday'=>'monday',
                  'form.rbins.persobundle.persontype.homework.monday_AM'=>'monday_AM',
                  'form.rbins.persobundle.persontype.homework.monday_PM'=>'monday_PM',
                  'form.rbins.persobundle.persontype.homework.tuesday'=>'tuesday',
                  'form.rbins.persobundle.persontype.homework.tuesday_AM'=>'tuesday_AM',
                  'form.rbins.persobundle.persontype.homework.tuesday_PM'=>'tuesday_PM',
                  'form.rbins.persobundle.persontype.homework.wednesday'=>'wednesday',
                  'form.rbins.persobundle.persontype.homework.wednesday_AM'=>'wednesday_AM',
                  'form.rbins.persobundle.persontype.homework.wednesday_PM'=>'wednesday_PM',
                  'form.rbins.persobundle.persontype.homework.thursday'=>'thursday',
                  'form.rbins.persobundle.persontype.homework.thursday_AM'=>'thursday_AM',
                  'form.rbins.persobundle.persontype.homework.thursday_PM'=>'thursday_PM',
                  'form.rbins.persobundle.persontype.homework.friday'=>'friday',
                  'form.rbins.persobundle.persontype.homework.friday_AM'=>'friday_AM',
                  'form.rbins.persobundle.persontype.homework.friday_PM'=>'friday_PM',
              ),
              'choices_as_values'=>true,
              'label' => 'Homework',
          )
      )
      ->add('birth_place')
      ->add(
          'gender',
          ChoiceType::class,
          array(
                'choices' => array(
                    'M'=>'M',
                    'F'=>'F'
                ),
                'choices_as_values'=>true
          )
      )
      ->add('identitycard_nr',TextType::class,
        array('attr'=>array('class' =>'input-medium'),'required'=>false))
      ->add('national_nr',TextType::class,
        array('attr'=>array('class' =>'input-medium')))
      ->add('matricule',TextType::class,array('required'=>false))
      ->add('dimona_nr',TextType::class,array('required'=>false))
      ->add('passport_nr',TextType::class,array('required'=>false))
      ->add('Degree',AutocompleteType::class,array(
        'source_path' => 'complete_degree',
        'class' => 'Degree',
        'required' => false))
      ->add('comment',TextareaType::class,array('required'=>false))
      ->add('sis_nr',TextType::class,
        array('attr'=>array('class' =>'input-medium'),'required'=>false))
      ->add('med_number',TextType::class,array('required'=>false))
      ->add('MedCenter', AutocompleteType::class,array(
        'source_path' => 'complete_medcenter',
        'class' => 'MedCenter',
        'required' => false))
      ->add('payroll_tax',CheckboxType::class,array('required'=>false))
      ->add('household_allowance',CheckboxType::class,array('required'=>false))
      ->add('residence_allowance',CheckboxType::class,array('required'=>false))
      ->add('senior_working',CheckboxType::class,array('required'=>false))
      ->add('senior_working_date',DateTimeType::class, array(
          'widget' => 'single_text',
          'format' => 'dd/M/y',
          'attr' => array('placeholder'=>'dd/mm/yyyy'),
          'required' => false
      ))
      ->add('account_iban_nr',TextType::class,array('required'=>false))
      ->add('account_bic_nr',TextType::class,array('required'=>false))
      ->add('other_account_holder',TextType::class,array('required'=>false))
      ->add('FunctionalChief', AutocompleteType::class,array(
        'source_path' => 'complete_person',
        'class' => 'Person',
        'required' => false))
        ->add('ResponsibleChief', AutocompleteType::class,array(
            'source_path' => 'complete_person',
            'class' => 'Person',
            'required' => false))
      ->add('nationality', AutocompleteType::class,array(
        'source_path' => 'complete_country',
        'class' => 'Country',
        'required' => true))
      ->add('maritalStatus', AutocompleteType::class,array(
        'source_path' => 'complete_marital',
        'class' => 'MaritalStatus',
        'required' => true))
      ->add('handicap_perc',
          ChoiceType::class,
          array(
              'choices' => array(
                  '100'=>'100',
                  '200'=>'200'
              ),
              'choices_as_values'=>true
          )
      )
      ->add('frame', AutocompleteType::class,array(
        'source_path' => 'complete_frame',
        'class' => 'Frame',
        'required' => true))
  ;
  }

  public function configureOptions(OptionsResolver $resolver)
  {
    $resolver->setDefaults(
        array(
            'data_class' => 'Rbins\PersoBundle\Entity\Person',
            'lang' => 'en',
        )
    );
  }

  public function getBlockPrefix()
  {
    return 'rbins_persobundle_persontype';
  }
}
