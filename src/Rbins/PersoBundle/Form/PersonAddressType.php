<?php

namespace Rbins\PersoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Rbins\PersoBundle\Form\Type\AutocompleteType;

class PersonAddressType extends AbstractType
{
  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $builder
    ->add('street', TextType::class)
    ->add('street_num', TextType::class, array('required'=>false))
    ->add('box', TextType::class, array('required'=>false))
    ->add('type',
        ChoiceType::class,
        array(
            'choices' => array(
                'form.rbins.persobundle.personworkingtype.PersonAddress.entry.type.0' => 'home',
                'form.rbins.persobundle.personworkingtype.PersonAddress.entry.type.1' => 'residence',
                'form.rbins.persobundle.personworkingtype.PersonAddress.entry.type.2' => 'work'
            ),
            'choices_as_values' => true
        )
    )
    ->add('comment',TextareaType::class,array('required'=>false))
    ->add('City', AutocompleteType::class,array(
      'source_path' => 'complete_city',
      'class' => 'City',
      'required' => true))
    ;
  }

  public function configureOptions(OptionsResolver $resolver)
  {
    $resolver->setDefaults(array(
        'data_class' => 'Rbins\PersoBundle\Entity\PersonAddress'
    ));
  }

  public function getBlockPrefix()
  {
    return 'rbins_persobundle_personaddresstype';
  }
}
