<?php

namespace Rbins\PersoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class MaritalStatusType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('is_active', null, array('required' => false))
            ->add('code', TextType::class, array('required' => false))
            ->add('name_fr')
            ->add('name_nl')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Rbins\PersoBundle\Entity\MaritalStatus'
        ));
    }

    public function getBlockPrefix()
    {
        return 'rbins_persobundle_maritalstatustype';
    }
}
