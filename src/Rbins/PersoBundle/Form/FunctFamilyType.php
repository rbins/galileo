<?php

namespace Rbins\PersoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FunctFamilyType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('is_active', null, array('required' => false))
            ->add('code')
            ->add('ehr_code')
            ->add('level')
            ->add('name_fr')
            ->add('name_nl')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Rbins\PersoBundle\Entity\FunctFamily'
        ));
    }

    public function getBlockPrefix()
    {
        return 'rbins_persobundle_functfamilytype';
    }
}
