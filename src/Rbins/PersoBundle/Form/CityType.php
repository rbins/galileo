<?php

namespace Rbins\PersoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;


class CityType extends AbstractType
{
  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    if($options['lang'] == 'nl') $order = 'nl';
    else $order = 'fr';

    $builder
      ->add('code')
      ->add('name')
      ->add('Country', EntityType::class,  array(
        'class' => 'Rbins\PersoBundle\Entity\Country',
        'query_builder' => function($r) use ($order) { 
          return    $r->createQueryBuilder('o') 
            ->andWhere('o.is_active=true')
            ->orderBy('o.name_'.$order, 'ASC');
        },
        'choice_label' => 'catalogue_name_'.$options['lang']
      )) 
      ;
  }

  public function configureOptions(OptionsResolver $resolver)
  {
      $resolver->setDefaults(array(
          'data_class' => 'Rbins\PersoBundle\Entity\City',
          'lang' => 'en'
      ));
  }

  public function getBlockPrefix()
  {
    return 'rbins_persobundle_citytype';
  }
}
