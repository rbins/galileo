<?php

namespace Rbins\PersoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Rbins\PersoBundle\Form\PersonBuildingType;
use Rbins\PersoBundle\Form\ChildType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Rbins\PersoBundle\Form\Type\AutocompleteType;
use Rbins\PersoBundle\Form\CombinedJobType;
use Rbins\PersoBundle\Form\DocumentType;
use Rbins\PersoBundle\Form\FamilyType;
use Rbins\PersoBundle\Form\PersonAddressType;
use Rbins\PersoBundle\Form\PersonContactType;
use Rbins\PersoBundle\Form\PersonEntryType;
use Rbins\PersoBundle\Form\PhoneBookType;
use Rbins\PersoBundle\Form\PremiumType;
use Rbins\PersoBundle\Form\TransportType;
use Rbins\PersoBundle\Form\WorkingDutyType;
use Rbins\PersoBundle\Form\WorkingHistType;

class PersonRelatedType extends AbstractType
{
  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $this->id = $options['id'];
    if($options['lang'] == 'nl') $order = 'nl';
    else $order = 'fr';
    switch ($options['entrytype'])
    {
      case 'hist' : 
        $builder->add(
            'WorkingHist',
            CollectionType::class,
            array(
                'entry_type' => WorkingHistType::class,
                'allow_add' => true,
                'by_reference' => false,
                'allow_delete' => true
            )
        )
        ->add(
            'WorkingDuty',
            CollectionType::class,
            array(
              'entry_type' => WorkingDutyType::class,
              'allow_add' => true,
              'by_reference' => false,
              'allow_delete' => true
            )
        );
        break;
      case 'contact':
        $builder->add(
            'PersonContact',
            CollectionType::class,
            array(
              'entry_type' => PersonContactType::class,
              'allow_add' => true,
              'by_reference' => false,
              'allow_delete' => true
            )
        )
        ->add(
            'PersonAddress',
            CollectionType::class,
            array(
              'entry_type' => PersonAddressType::class,
              'allow_add' => true,
              'by_reference' => false,
              'allow_delete' => true
            )
        )
        ->add(
            'building',
            ChoiceType::class,
            array(
                'required'=>false,
                'choices' => array(
                    ''=>'',
                    'Gulledelle' => 'gulledelle',
                    'Eguermin' => 'eguermin',
                    'Couvent / Klooster' => 'couvent / klooster',
                    'De Vestel' => 'de vestel',
                    'Janlet' => 'janlet',
                    'Geologie' => 'geologie',
                    'Maison / Huis Jenner' => 'maison / huis jenner',
                    ),
                'choices_as_values' => true
            )
        )
        ->add('building_floor', TextType::class, array('required'=>false))
        ->add('building_room', TextType::class, array('required'=>false))
        ;
        break;
      case 'family':
        $builder->add(
            'PersonFamily',
            CollectionType::class,
            array(
              'entry_type' => FamilyType::class,
              'allow_add' => true,
              'by_reference' => false,
              'allow_delete' => true
            )
        );
        break;
      case 'transport':
        $builder->add(
            'Transport',
            CollectionType::class,
            array(
              'entry_type' => TransportType::class,
              'allow_add' => true,
              'by_reference' => false,
              'allow_delete' => true
            )
        ) ;
        break;
      case 'premium':
        $builder->add(
            'Premium',
            CollectionType::class,
            array(
              'entry_type' => PremiumType::class,
                'entry_options' => array(
                    'lang' => $order
                ),
                'allow_add' => true,
                'by_reference' => false,
                'allow_delete' => true,
            )
        ) ;
        break;
      case 'entry':
        $builder->add(
            'PersonEntry',
            CollectionType::class,
            array(
              'entry_type' => PersonEntryType::class,
                'entry_options' => array(
                    'lang' => $order
                ),
              'allow_add' => true,
              'by_reference' => false,
              'allow_delete' => true
            )
        ) ;
        break;
      case 'combined_job':
        $builder->add(
            'CombinedJob',
            CollectionType::class,
            array(
              'entry_type' => CombinedJobType::class,
                'entry_options' => array(
                    'lang' => $order
                ),
              'allow_add' => true,
              'by_reference' => false,
              'allow_delete' => true
            )
        );
        break;
      case 'document':
        $builder->add(
            'Document',
            CollectionType::class,
            array(
              'entry_type' => DocumentType::class,
                'entry_options' => array(
                    'lang' => $order
                ),
              'allow_add' => true,
              'by_reference' => false,
              'allow_delete' => true
            )
        );
        break;
      case 'phonebook':
        $builder->add(
            'PhoneBook',
            CollectionType::class,
            array(
              'entry_type' => PhoneBookType::class,
                'entry_options' => array(
                    'person_ref' => $this->id
                ),
              'allow_add' => true,
              'by_reference' => false,
              'allow_delete' => true
            )
        )
        ->add('description', TextareaType::class, array('required'=>false,'max_length'=>700))
        ->add('file')
        ->add('building',
            ChoiceType::class,
            array(
                'required'=>false,
                'choices' => array(
                    ''=>'',
                    'Gulledelle' => 'gulledelle',
                    'Eguermin' => 'eguermin',
                    'Couvent / Klooster' => 'couvent / klooster',
                    'De Vestel' => 'de vestel',
                    'Janlet' => 'janlet',
                    'Geologie' => 'geologie',
                    'Maison / Huis Jenner' => 'maison / huis jenner',
                    ),
                'choices_as_values' => true
            )
        )
        ->add('building_floor', TextType::class, array('required'=>false))
        ->add('building_room', TextType::class, array('required'=>false));
        if ( $options['securityContext'] ) {
          $builder->add(
            'workfunction', AutocompleteType::class, array (
            'source_path' => 'complete_workfunction',
            'class' => 'WorkFunction',
            'required' => FALSE
          )
          );
        }
        $builder->add('hobbies', TextType::class, array('required'=>false,'max_length'=>100))
        ->add('removephoto', CheckboxType::class, array('mapped'  => false, 'required' => false))
        ->add('pb_visible', CheckboxType::class, array('required' => false))
        ->add('email', TextType::class, array('required' => false))
        ;
        break;
    }
  }

  public function configureOptions(OptionsResolver $resolver)
  {
    $resolver->setDefaults(array(
        'data_class' => 'Rbins\PersoBundle\Entity\Person',
        'cascade_validation' => true,
        'lang'=>'en',
        'entrytype'=>'',
        'id'=>null,
        'securityContext'=>true
    ));
  }

  public function getBlockPrefix()
  {
    return 'rbins_persobundle_personworkingtype';
  }
}
