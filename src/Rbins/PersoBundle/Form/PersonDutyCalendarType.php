<?php

namespace Rbins\PersoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Locale\Locale;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Rbins\PersoBundle\Form\WorkingHistType;

class PersonDutyCalendarType extends AbstractType
{
  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    if($options['lang'] == 'nl') $order = 'nl';
    else $order = 'fr';

    $builder
        ->add('Calendar', 'collection', array(
          'type' => new DutyCalendarType(),
          'allow_add' => true,
          'by_reference' => false,
          'allow_delete' => true,
        ))
    ;
  }

  public function configureOptions(OptionsResolver $resolver)
  {
    $resolver->setDefaults(array(
        'data_class' => 'Rbins\PersoBundle\Entity\WorkingDuty',
        'lang' => 'en'
    ));
  }

  public function getBlockPrefix()
  {
    return 'rbins_persobundle_persondutycalendartype';
  }
}
