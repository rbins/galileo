<?php

namespace Rbins\PersoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class CombinedJobType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
      $builder
	->add('start_date',DateTimeType::class,array(
	  'widget' => 'single_text',
	  'format' => 'dd/M/y',
	  'attr' => array('placeholder'=>'dd/mm/yyyy'),
	  ))
        ->add('end_date',DateTimeType::class,array(
	  'widget' => 'single_text',
	  'format' => 'dd/M/y',
	  'attr' => array('placeholder'=>'dd/mm/yyyy'),
	  'required'=> false
	  ))
        ->add('comment',TextareaType::class,array('required'=>false))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Rbins\PersoBundle\Entity\CombinedJob',
            'lang' => 'fr'
        ));
    }

    public function getBlockPrefix()
    {
        return 'rbins_persobundle_combinedjobtype';
    }
}
