<?php

namespace Rbins\PersoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Rbins\PersoBundle\Form\Type\AutocompleteType;

class TransportType extends AbstractType
{
  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $builder
      ->add('start_date',DateTimeType::class, array(
            'widget' => 'single_text',
            'format' => 'dd/M/y',
            'attr' => array('placeholder'=>'dd/mm/yyyy'),
            'required' => false,
        ))
      ->add('end_date',DateTimeType::class, array(
            'widget' => 'single_text',
            'format' => 'dd/M/y',
            'attr' => array('placeholder'=>'dd/mm/yyyy'),
            'required' => false,
        ))
      ->add('parking_card_date',DateTimeType::class, array(
            'widget' => 'single_text',
            'format' => 'dd/M/y',
            'attr' => array('placeholder'=>'dd/mm/yyyy'),
            'required' => false,
        ))        
      ->add('delijn',CheckboxType::class, array('required' => false))
      ->add('stibmivb',CheckboxType::class, array('required' => false))
      ->add('tec',CheckboxType::class, array('required' => false))
      ->add('sncb',CheckboxType::class, array('required' => false))
      ->add('bike',CheckboxType::class, array('required' => false))
      ->add('car',CheckboxType::class, array('required' => false))
      ->add('km',NumberType::class, array('required'=>false, 'scale' => 2))
      ->add('numberplate',TextType::class, array('required'=>false))
      ->add('is_effective',CheckboxType::class, array('required'=>false))
      ->add('giveback',CheckboxType::class, array('required' => false))
      ->add('comment',TextareaType::class,array('required'=>false))
    ->add('city_from', AutocompleteType::class,array(
      'source_path' => 'complete_city',
      'class' => 'City',
      'required' => false))
    ->add('city_to', AutocompleteType::class,array(
      'source_path' => 'complete_city',
      'class' => 'City',
      'required' => false))
      ;
  }

  public function configureOptions(OptionsResolver $resolver)
  {
    $resolver->setDefaults(array(
        'data_class' => 'Rbins\PersoBundle\Entity\Transport'
    ));
  }

  public function getBlockPrefix()
  {
    return 'rbins_persobundle_transporttype';
  }
}
