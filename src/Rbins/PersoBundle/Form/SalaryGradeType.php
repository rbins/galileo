<?php

namespace Rbins\PersoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SalaryGradeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('is_active', null, array('required' => false))
            ->add('code')
            ->add('ehr_code')
            ->add('name_fr')
            ->add('name_nl')
            ->add('level')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Rbins\PersoBundle\Entity\SalaryGrade'
        ));
    }

    public function getBlockPrefix()
    {
        return 'rbins_persobundle_salarygradetype';
    }
}
