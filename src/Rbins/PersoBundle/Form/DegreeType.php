<?php

namespace Rbins\PersoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DegreeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('is_active', null, array('required' => false))
            ->add('code', 'text', array('required' => false))
            ->add('name_fr')
            ->add('name_nl')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Rbins\PersoBundle\Entity\Degree'
        ));
    }

    public function getBlockPrefix()
    {
        return 'rbins_persobundle_degreetype';
    }
}
