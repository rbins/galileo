<?php

namespace Rbins\PersoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class FamilyType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
      $date_opt = array(
        'widget' => 'single_text',
        'format' => 'dd/M/y',
        'attr' => array('placeholder'=>'dd/mm/yyyy'),
        'required' => false
      );

      $builder
        ->add('type',
            ChoiceType::class,
            array(
                  'choices' => array(
                        'form.rbins.persobundle.personworkingtype.PersonFamily.entry.type.0' => 'child',
                        'form.rbins.persobundle.personworkingtype.PersonFamily.entry.type.1' => 'spouse',
                        'form.rbins.persobundle.personworkingtype.PersonFamily.entry.type.2' => 'other'
                  ),
                  'choices_as_values' => true
            )
        )
        ->add('full_name')
        ->add('gender',
            ChoiceType::class,
            array(
                'choices' => array(
                    'M'=>'M',
                    'F'=>'F'
                ),
                'choices_as_values' => true
            )
        )
        ->add('birth_date', DatetimeType::class, $date_opt)
        ->add('allowance', null, array('required' => false))
        ->add('allowance_end', DatetimeType::class, $date_opt)
        ->add('parential_leave', null, array('required' => false))
        ->add('comment', TextareaType::class, array('required' => false))
        ->add('handicap', null, array('required' => false))
        ->add('employed',
            ChoiceType::class,
            array(
                'choices' => array(
                             'form.rbins.persobundle.personworkingtype.PersonFamily.entry.unknown' => 2,
                             'form.rbins.persobundle.personworkingtype.PersonFamily.entry.no' => 0,
                             'form.rbins.persobundle.personworkingtype.PersonFamily.entry.yes' => 1
                ),
                'placeholder' => false,
                'empty_data' => 2,
                'required' => false,
                'choices_as_values' => true
            )
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Rbins\PersoBundle\Entity\Family'
        ));
    }

    public function getBlockPrefix()
    {
        return 'rbins_persobundle_familytype';
    }
}
