<?php

namespace Rbins\PersoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class DepartmentType extends AbstractType
{

  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $lang = $options['lang'];

    $builder
      ->add(
          'level',
          ChoiceType::class,
          array(
              'choices'   => array(
                  'form.entity.department.level.department' => 'department',
                  'form.entity.department.level.section' => 'section'
              ),
              'required'  => true,
              'choices_as_values'=>true
          )
      )
      ->add('is_active', null, array('required' => false))
      ->add('code', null, array('required' => false))
      ->add('name_fr')
      ->add('name_en')
      ->add('name_nl')
      ->add('Parent', EntityType::class,  array(
        'required' => false,
        'class' => 'Rbins\PersoBundle\Entity\Department',
        'query_builder' => function($r) use ($lang) { 
          return    $r->createQueryBuilder('o')
            ->where('o.level = :lvl')
            ->orderBy('o.name_'.$lang, 'ASC')
            ->setParameter('lvl', 'department');

        },
        'choice_label' => 'catalogue_name_'.$lang
      )) 
      ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Rbins\PersoBundle\Entity\Department',
            'lang' => 'en'
        ));
    }

    public function getBlockPrefix()
    {
        return 'rbins_persobundle_departmenttype';
    }
}
