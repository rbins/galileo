<?php

namespace Rbins\PersoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class MedCenterType extends AbstractType
{
  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $builder
      ->add('name')
      ->add('phone')
      ->add('fax')
      ->add('address')
      ->add('comment')
      ->add('City', EntityType::class,  array(
        'class' => 'Rbins\PersoBundle\Entity\City',
        'query_builder' => function($r) { 
          return    $r->createQueryBuilder('o')->orderBy('o.name', 'ASC');
        },
        'choice_label' => 'name'
      )) 
    ;
  }

  public function configureOptions(OptionsResolver $resolver)
  {
    $resolver->setDefaults(array(
      'data_class' => 'Rbins\PersoBundle\Entity\MedCenter'
    ));
  }

  public function getBlockPrefix()
  {
    return 'rbins_persobundle_medcentertype';
  }
}
