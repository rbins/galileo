<?php

namespace Rbins\PersoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class ReportsType extends AbstractType
{
  public function __construct($name, $report) {
    $this->name = $name;
    $this->report = $report;
  }
  
  public function buildForm(FormBuilderInterface $builder, array $options) {
    if($this->name == 'med_num') {
      $builder
        ->add('start_month', DateTimeType::class, array(
          'widget' => 'single_text','format' => 'dd/M/y','attr' => array('placeholder'=>'dd/mm/yyyy'),)
        )
        ->add('end_month', DateTimeType::class, array(
          'widget' => 'single_text','format' => 'dd/M/y','attr' => array('placeholder'=>'dd/mm/yyyy'),)
        );
    } elseif($this->name == 'work_percentage') {
      $builder
        ->add('date', DateTimeType::class, array(
          'widget' => 'single_text','format' => 'dd/M/y','attr' => array('placeholder'=>'dd/mm/yyyy'),)
        );
    } elseif($this->name == 'cumul') {
      $builder
        ->add('date', DateTimeType::class, array(
          'widget' => 'single_text','format' => 'dd/M/y','attr' => array('placeholder'=>'dd/mm/yyyy'),)
        );
    } elseif($this->name == 'replacement_contract') {
      $builder
        ->add('date', DateTimeType::class, array(
          'widget' => 'single_text','format' => 'dd/M/y','attr' => array('placeholder'=>'dd/mm/yyyy'),)
        );
    } elseif($this->name == 'com_gest') {
      $builder
        ->add('date_from', DateTimeType::class, array(
          'widget' => 'single_text','format' => 'dd/M/y','attr' => array('placeholder'=>'dd/mm/yyyy'),)
        )
        ->add('date_to', DateTimeType::class, array(
          'widget' => 'single_text','format' => 'dd/M/y','attr' => array('placeholder'=>'dd/mm/yyyy'),)
        );
    } elseif($this->name == 'pdata') {
      $builder
        ->add('date', DateTimeType::class, array(
          'widget' => 'single_text','format' => 'dd/M/y','attr' => array('placeholder'=>'dd/mm/yyyy'),)
        );
    } elseif($this->name == 'percentage' || $this->name == 'percentage_cause') {
      $builder
        ->add('start_date', DateTimeType::class, array(
          'widget' => 'single_text','format' => 'dd/M/y','attr' => array('placeholder'=>'dd/mm/yyyy'),)
        )
        ->add('end_date', DateTimeType::class, array(
          'widget' => 'single_text','format' => 'dd/M/y','attr' => array('placeholder'=>'dd/mm/yyyy'),)
        )
        ->add('ContractComment', EntityType::class,    array(
        'class' => 'Rbins\PersoBundle\Entity\ContractComment',
        'placeholder' => '')
        );
    } elseif($this->name == 'primes') {
      $builder
        ->add('start_date', DateTimeType::class, array(
          'widget' => 'single_text','format' => 'dd/M/y','attr' => array('placeholder'=>'dd/mm/yyyy'),)
        )
        ->add('end_date', DateTimeType::class, array(
          'widget' => 'single_text','format' => 'dd/M/y','attr' => array('placeholder'=>'dd/mm/yyyy'),)
        );
    } elseif($this->name == 'transports') {
      $builder
        ->add('start_date', DateTimeType::class, array(
          'widget' => 'single_text','format' => 'dd/M/y','attr' => array('placeholder'=>'dd/mm/yyyy'),)
        )
        ->add('end_date', DateTimeType::class, array(
          'widget' => 'single_text','format' => 'dd/M/y','attr' => array('placeholder'=>'dd/mm/yyyy'),)
        );
    }
    
    $builder->add(
        'format',
        ChoiceType::class,
        array(
            'choices' => array_combine($this->report['format'],$this->report['format']),
            'choices_as_values' => true
            )
    );
  }

  public function configureOptions(OptionsResolver $resolver) {
  /*
      $resolver->setDefaults(array(
          'data_class' => 'Rbins\PersoBundle\Entity\CataloguePremium'
      ));*/
  }
  
  public function getBlockPrefix() {
    return  $this->name;
  }
}
