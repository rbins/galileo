<?php

namespace Rbins\PersoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class PersonContactType extends AbstractType
{
  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $builder
      ->add(
          'type',
          ChoiceType::class,
          array(
              'choices' => array(
                'form.rbins.persobundle.personworkingtype.PersonContact.entry.type.phone' => 'phone',
                'form.rbins.persobundle.personworkingtype.PersonContact.entry.type.fax' => 'fax',
                'form.rbins.persobundle.personworkingtype.PersonContact.entry.type.email' => 'mail',
                'form.rbins.persobundle.personworkingtype.PersonContact.entry.type.emergency' => 'emergency',
              ),
              'expanded'=> false,
              'choices_as_values' => true
          )
        )
      ->add('entry',TextType::class)
      ->add('comment',TextareaType::class,array('required'=>false))
      ->add('prefered',CheckboxType::class, array('required'=>false))
    ;
  }

  public function configureOptions(OptionsResolver $resolver)
  {
    $resolver->setDefaults(array(
      'data_class' => 'Rbins\PersoBundle\Entity\PersonContact'
    ));
  }

  public function getBlockPrefix()
  {
    return 'rbins_persobundle_personcontacttype';
  }
}
