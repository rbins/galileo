<?php

namespace Rbins\PersoBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class PremiumType extends AbstractType
{
  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $lang = $options['lang'] ;
    $builder
      ->add('start_date',DateTimeType::class, array(
            'widget' => 'single_text',
            'format' => 'dd/M/y',
            'attr' => array('placeholder'=>'dd/mm/yyyy'),
        ))
      ->add('end_date',DateTimeType::class, array(
            'widget' => 'single_text',
            'format' => 'dd/M/y',
            'attr' => array('placeholder'=>'dd/mm/yyyy'),
        ))
      ->add('premium', EntityType::class,  array(
        'required' => true,
        'class' => 'Rbins\PersoBundle\Entity\CataloguePremium',
        'query_builder' => function($r) use ($lang) { 
          return    $r->createQueryBuilder('o')
            ->orderBy('o.name_'.$lang, 'ASC');
        },
        'choice_label' => 'catalogue_name_'.$lang
        ))
      ->add('comment',TextareaType::class, array('required'=> false))
    ;
  }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Rbins\PersoBundle\Entity\Premium',
            'lang' => 'fr'
        ));
    }

    public function getBlockPrefix()
    {
        return 'rbins_persobundle_premiumtype';
    }
}
