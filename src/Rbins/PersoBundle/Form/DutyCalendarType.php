<?php

namespace Rbins\PersoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class DutyCalendarType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('week',
                ChoiceType::class,
                array(
                    'choices' => array(
                        'form.rbins.persobundle.persondutycalendartype.Calendar.entry.week.0'=>0,
                        '1'=>1,
                        '2'=>2,
                        '3'=>3),
                    'expanded'=> false,
                    'choices_as_values' => true
                )
            )
            ->add('mon_am', null, array('required' => false))
            ->add('mon_pm', null, array('required' => false))
            ->add('tue_am', null, array('required' => false))
            ->add('tue_pm', null, array('required' => false))
            ->add('wed_am', null, array('required' => false))
            ->add('wed_pm', null, array('required' => false))
            ->add('thu_am', null, array('required' => false))
            ->add('thu_pm', null, array('required' => false))
            ->add('fri_am', null, array('required' => false))
            ->add('fri_pm', null, array('required' => false))
            ->add('sat_am', null, array('required' => false))
            ->add('sat_pm', null, array('required' => false))
            ->add('sun_am', null, array('required' => false))
            ->add('sun_pm', null, array('required' => false))
        ;
        $builder->get('mon_am')->setData(true);
        $builder->get('mon_pm')->setData(true);
        $builder->get('tue_am')->setData(true);
        $builder->get('tue_pm')->setData(true);
        $builder->get('wed_am')->setData(true);
        $builder->get('wed_pm')->setData(true);
        $builder->get('thu_am')->setData(true);
        $builder->get('thu_pm')->setData(true);
        $builder->get('fri_am')->setData(true);
        $builder->get('fri_pm')->setData(true);

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Rbins\PersoBundle\Entity\DutyCalendar'
        ));
    }

    public function getBlockPrefix()
    {
        return 'rbins_persobundle_dutycalendartype';
    }
}
