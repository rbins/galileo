<?php

namespace Rbins\PersoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Rbins\PersoBundle\Form\Type\AutocompleteType;

class DocumentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')
            ->add('file')
	    ->add('DocType', AutocompleteType::class,array(
	      'source_path' => 'complete_doctype',
	      'class' => 'DocType',
	      'required' => true))            
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Rbins\PersoBundle\Entity\Document',
            'lang' => 'fr'
        ));
    }

    public function getBlockPrefix()
    {
        return 'rbins_persobundle_documenttype';
    }
}
