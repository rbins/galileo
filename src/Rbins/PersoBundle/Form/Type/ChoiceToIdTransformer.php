<?php

namespace Rbins\PersoBundle\Form\Type;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\UnexpectedTypeException;
use Symfony\Component\Form\Exception\TransformationFailedException;

class ChoiceToIdTransformer implements DataTransformerInterface
{
  protected $entity;
  protected $em;

  public function __construct($em, $entity) {
    $this->entity = $entity;
    $this->em = $em;
  }
  /**
    * {@inheritdoc}
    */
  public function transform($object)
  {
    if (null === $object) {
      return array();
    }
    if(! method_exists($object, 'getId') )
      return array();
    $result = array('id'=>$object->getId());
    $result['label_fr'] = $object->getLabel('fr');
    $result['label_nl'] = $object->getLabel('nl');
    $result['label_en'] = $object->getLabel('en');
    return $result;
  }

    /**
     * {@inheritdoc}
     */
    public function reverseTransform($id)
    {
      if(! $id) return null;
      $o = $this->em
        ->getRepository('RbinsPersoBundle:'.$this->entity)
        ->find($id);
       if (null === $o) {
            throw new TransformationFailedException(sprintf(
                'An Object with number "%s" does not exist!',
                $id
            ));
        }
      return $o;
    }
}
