<?php

namespace Rbins\PersoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Rbins\PersoBundle\Repository\DepartmentRepository;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class PhoneBookType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $id = $options['person_ref'] ;
        $builder
            ->add(
                'type',
                ChoiceType::class,
                array(
                    'choices' => array(
                        'form.rbins.persobundle.personworkingtype.PersonContact.entry.type.phone' => 'phone',
                        'form.rbins.persobundle.personworkingtype.PersonContact.entry.type.gsm' => 'gsm',
                        'form.rbins.persobundle.personworkingtype.PersonContact.entry.type.fax' => 'fax',
                        'form.rbins.persobundle.personworkingtype.PersonContact.entry.type.email' => 'email',
                    ),
                    'expanded'=> false,
                    'choices_as_values' => true
                )
            )
            ->add('entry');
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Rbins\PersoBundle\Entity\PhoneBook',
            'person_ref' => null
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'rbins_persobundle_phonebook';
    }
}
