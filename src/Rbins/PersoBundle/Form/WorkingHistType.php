<?php

namespace Rbins\PersoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Rbins\PersoBundle\Form\Type\AutocompleteType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class WorkingHistType extends AbstractType
{
  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $date_opt = array(
        'widget' => 'single_text',
        'format' => 'dd/M/y',
        'attr' => array('placeholder'=>'dd/mm/yyyy'),
    );

    $builder
      ->add(
          'level',
          ChoiceType::class,
          array(
              'choices' => array(
                  'A'=>'A',
                  'B'=>'B',
                  'C'=>'C',
                  'D'=>'D',
                  'SW'=>'SW'
              ),
              'expanded'=> false,
              'choices_as_values' => true
          )
      )
      ->add(
          'linguistic_role',
          ChoiceType::class,
          array(
              'choices' => array(
                  'nl' => 'N',
                  'fr' => 'F',
                  'en' => 'E',
                  'de' => 'D'
              ),
              'expanded'=> false,
              'choices_as_values' => true
          )
      )
      ->add(
          'lang_level',
          ChoiceType::class,
          array(
              'choices' => array(
                  '1'=>'1',
                  '2'=>'2',
                  '3'=>'3',
                  '4'=>'4',
                  '5'=>'5'
              ),
              'expanded'=> false,
              'required' => false,
              'choices_as_values' => true
          )
      )
      ->add(
          'class',
          ChoiceType::class,
          array(
              'choices' => array(
                  1=>1,
                  2=>2,
                  3=>3,
                  4=>4,
                  5=>5),
              'expanded'=> false,
              'required' => false,
              'choices_as_values' => true
          )
      )
      ->add(
          'classseniority',
          DateTimeType::class,
          array(
            'widget' => 'single_text',
            'format' => 'dd/M/y',
            'attr' => array('placeholder'=>'dd/mm/yyyy'),
            'required' => false
          )
      )
      ->add(
        'scientific_seniority',
        DateTimeType::class,
        array(
          'widget' => 'single_text',
          'format' => 'dd/M/y',
          'attr' => array('placeholder'=>'dd/mm/yyyy'),
          'required' => false
        )
      )
      ->add(
        'degree_seniority',
        DateTimeType::class,
        array(
          'widget' => 'single_text',
          'format' => 'dd/M/y',
          'attr' => array('placeholder'=>'dd/mm/yyyy'),
          'required' => false
        )
      )
      ->add(
        'scale_seniority',
        DateTimeType::class,
        array(
          'widget' => 'single_text',
          'format' => 'dd/M/y',
          'attr' => array('placeholder'=>'dd/mm/yyyy'),
          'required' => false
        )
      )
      ->add(
        'levelseniority',
        DateTimeType::class,
        array(
          'widget' => 'single_text',
          'format' => 'dd/M/y',
          'attr' => array('placeholder'=>'dd/mm/yyyy'),
          'required' => false
        )
      )
      ->add(
          'category',
          ChoiceType::class,
          array(
              'choices' => array(
                'form.rbins.persobundle.personworkingtype.WorkingHist.entry.category.unknown' => 'unknown',
                'form.rbins.persobundle.personworkingtype.WorkingHist.entry.category.worker' => 'worker',
                'form.rbins.persobundle.personworkingtype.WorkingHist.entry.category.employee' => 'employee'
              )
              ,'expanded'=> false,
              'choices_as_values' => true
          )
      )
      ->add('seniority', DateTimeType::class, array(
        'widget' => 'single_text',
        'format' => 'dd/M/y',
        'attr' => array('placeholder'=>'dd/mm/yyyy'),
        'required' => false
      ))
      ->add('salary_seniority', DateTimeType::class, array(
        'widget' => 'single_text',
        'format' => 'dd/M/y',
        'attr' => array('placeholder'=>'dd/mm/yyyy'),
        'required' => false
      ))
      ->add('start_date', DateTimeType::class, $date_opt)
      ->add('end_date', DateTimeType::class, $date_opt)
      ->add('Status',null, array('required'=>true))
      ->add('cost_center',TextType::class, array('required'=>false))
      ->add('comment',TextareaType::class, array('required'=>false))
      ->add('Reason', EntityType::class,    array(
        'class' => 'Rbins\PersoBundle\Entity\Reason',
        'placeholder' => '',
        'required' => false,
      ))
      ->add('ContractDuration',null, array('required'=>true))

      ->add('ContractComment', EntityType::class,    array(
        'class' => 'Rbins\PersoBundle\Entity\ContractComment',
        'placeholder' => '',
        'required' => false,
      ))

      ->add('SubLevel', AutocompleteType::class,array(
        'source_path' => 'complete_sublevel',
        'class' => 'SubLevel',
        'required' => true))

      ->add('FunctionFamily', AutocompleteType::class,array(
        'source_path' => 'complete_functfamily',
        'class' => 'FunctFamily',
        'required' => true))

      ->add('Grade', AutocompleteType::class,array(
        'source_path' => 'complete_grade',
        'class' => 'Grade',
        'required' => true))

      ->add('SalaryGrade', AutocompleteType::class,array(
        'source_path' => 'complete_salgrade',
        'class' => 'SalaryGrade',
        'required' => true))

      ->add('replacement_person',TextareaType::class, array('required'=>false))
      ->add('noticemonths',TextareaType::class, array('required'=>false))
      ->add('noticeweeks',TextareaType::class, array('required'=>false))

    ;

    $builder->get('end_date')->setRequired(false);
    $builder->get('salary_seniority')->setRequired(false);
  }

  public function configureOptions(OptionsResolver $resolver) {
    $resolver->setDefaults(array(
      'data_class' => 'Rbins\PersoBundle\Entity\WorkingHist',
    ));
  }

  public function getBlockPrefix() {
    return 'rbins_persobundle_workinghisttype';
  }
}
