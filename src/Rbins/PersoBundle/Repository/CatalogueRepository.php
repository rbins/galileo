<?php

namespace Rbins\PersoBundle\Repository;
use Rbins\PersoBundle\Entity\MaritalStatus;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping\ClassMetadataFactory;

/**
 * CatalogueRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class CatalogueRepository extends EntityRepository
{
  public function findAllCatalogueOrdered($catalogue) {
    $order = 'g.is_active desc, g.code ASC' ;

    if ($catalogue == 'Grade' || $catalogue == 'SalaryGrade' || $catalogue == 'FunctFamily') {
      $order = 'g.is_active desc, g.level ASC, g.code ASC' ;
    }
    if ($catalogue=='DocType') $order = 'g.is_active ASC' ;
    return $this->getEntityManager()
      ->createQuery('SELECT g FROM RbinsPersoBundle:'.$catalogue.' g ORDER BY '.$order)
      ->getResult();
  }

  public function entitytoField($name) {
    switch($name) {
      case 'Grade': return 'grade';
      case 'Frame': return 'frame';
      case 'Cause': return 'cause';
      case 'Status': return 'status';
      case 'Reason': return 'reason';
      case 'FunctFamily': return 'funct_family';
      case 'CataloguePremium': return 'premium';
      case 'SalaryGrade': return 'salarygrade';
      case 'Country': return 'country';
      case 'MaritalStatus': return 'marital_status';
      case 'ContractDuration': return 'contract_duration';
      case 'ExitReason': return 'exit_reason';
      case 'ContractComment': return 'contract_comment';
      case 'SubLevel': return 'sublevel';
      case 'DocType' : return 'doctype';
      case 'Degree' : return 'degree';
      case 'WorkFunction' : return 'workfunction';
    }
  }
  public function searchComplete($catalogue, $name, $exact = false, $locale = null) {
    $conn = $this->getEntityManager()->getConnection();
    if($locale == 'en') {
      $field_name = "name_fr || ' - ' || name_nl";
    }else {
      $field_name = "name_".$locale;
    }
    $qb = $conn->createQueryBuilder();
    $qb->select("id as value, ${field_name}  as label")
      ->from('catalogue','u')
      ->andwhere('type = :type')
      ->andwhere("${field_name} ilike :search")
      ->andwhere("is_active = true")
      ->orderBy("${field_name}");

    $st = $conn->prepare($qb->getSQL());
    $st->bindValue('type', $this->entitytoField($catalogue));
    if($exact)
      $st->bindValue('search', $name);
    else
      $st->bindValue('search', '%'.$name.'%');
    $st->execute();
    return  $st->fetchAll();
  }
}
