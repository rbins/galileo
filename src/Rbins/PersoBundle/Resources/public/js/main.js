(function($)
{
  $.collection = function(el, options) 
  {
  
    var base = this;
    base.$el = $(el);
    base.el = el;


    base.$el.data("collection", base);      

    base.init = function()
    {     
      base.$el.find('li').each(function() {
        base.attachEvents($(this));
      });   
      base.options = $.extend({},$.collection.defaultOptions, options);
      base.newLinkLi = $('<li class="add_lnk"></li>').append(base.options['add_lnk']);
      base.$el.append(base.newLinkLi);    
      base.prot = base.options['prototype'];
      $(base.options['add_lnk']).click(base.onaddItem) ;
      base.checkEmptyMessage();      
    };
    
    base.onaddItem = function (e){
      e.preventDefault();
      base.addItem();
    };

    base.addItem = function () {
      rowid = base.$el.children().length;
      var newForm = base.prot.replace(/__name__/g, rowid);
      base.newLinkLi.before('<li data-rowid="' + rowid + '"></li>');
      base.newLinkLi.prev().append(newForm);
      base.attachEvents(base.newLinkLi.prev());
      
      base.checkEmptyMessage();
      return rowid;
    };

    base.attachEvents = function($tagFormLi) {
      dellnk = $tagFormLi.find('.remove_el');
      
      dellnk.on('click', function(e) {
        if(e != undefined)
          e.preventDefault();
        $tagFormLi.remove();
        base.checkEmptyMessage();
      });
      
      dup_lnk = $tagFormLi.find('.dup_el');
      dup_lnk.on('click', base.duplicate);
    };
    
    base.checkEmptyMessage = function(){
      var empty_msg = $('.empty_collection');
      if(base.$el.children().not('.heading').length > 1) {
        base.$el.find('.heading').show();
        empty_msg.hide();
      } else {
        base.$el.find('.heading').hide();
        empty_msg.show();
      }
    };
    
    base.duplicate = function(e) {
      to_id = base.addItem();
      from_id = $(e.target).closest('li[data-rowid]').data('rowid');
      base.copyDataTo(from_id,to_id);
    };

    base.copyDataTo = function(from_rowid, to_rowid) {
      e = base.$el.find('li[data-rowid="'+ from_rowid +'"]');
      e.find('input[name], select[name], textarea[name]').each(function(){
        from_fid = $(this).attr('id');
        to_id = from_fid.replace('_' + from_rowid + '_', '_' + to_rowid + '_' );
        if($('#'+from_fid).is('[type="checkbox"]')) {
          $('#'+to_id).attr('checked', $('#'+from_fid).is(':checked') );
        }else {
          $('#'+to_id).val($(this).val());
          $('#'+to_id).trigger('change');
        }
      });
    };
    
    base.init() ;
  };    
  
  $.collection.defaultOptions = {
    newLinkLi: undefined,
    prototype: undefined,
    add_lnk: undefined
  };

  $.fn.collection = function(options)
  {
    return this.each(function()
    {
      (new $.collection(this, options));
    });
  };

})(jQuery);    
    
