
function loadLetter(event) {
  var letter = $(this).text();
  $('.catalogue_list').hide();
  $.ajax({
    url: Routing.generate('catalogue_load_list', {letter: letter, catalogue:catalogue}),
    success: function(data){
      $('.catalogue_list').html(data).show();
      $('.catalogue_list > li').each(function (i) {
        el = $(this);
        CatalogueAttachEvent(el);
      });
    }
  });
}

function fitler_catalogue(event) {
  txt = stripAccents($(this).val().toLowerCase());
  search_texts = jQuery.trim(txt).split(' ');
  $('.catalogue_list').hide();
  $('.catalogue_list > li').each(function (i) {
    el = $(this);
    show = true;
    for(var needle in search_texts) {

      idx = el.data('filter').indexOf(search_texts[needle]);
      if( idx === -1 || idx === false )
        show = false;
    }
    if(show) el.show();
    else el.hide();
  });
  $('.catalogue_list').show();
}

function stripAccents(str) {
  
  var from = "áàâäãåçéèêëíìîïñóòôöõúùûüýÿ";
  var to   = "aaaaaaceeeeiiiinooooouuuuyy";
  for (var i=0, l=from.length ; i<l ; i++) {
    str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
  }
  return str;
}

function initInEdit(event) {
  $(this).hide();
  $('.alert').remove();
  var content_id = $(this).closest('li');
  var id = content_id.data('id');

  $.ajax({
    url: Routing.generate('catalogue_load', {id: content_id.data('id'), catalogue:catalogue}),
    success: function(data){
      content_id.hide();
      $('.catalogue_list li[data-id="' + id + '"] ').after($('<li></li>').append(data));
      inEdit(id);
    }
  });
}

function inToggleActive(event) {
  content_id = $(this).closest('li');
  active = $(this).closest('li').hasClass('catalogue_inactive') ? 'true' : 'false';
  id = content_id.data('id');
  $.ajax({
    url: Routing.generate('catalogue_toggle', {id: id, status: active, catalogue:catalogue}),
    dataType: 'json',
    success: function(data){
      if(data.data == 'true') {
        $('.catalogue_list li[data-id="' + id + '"] ').removeClass('catalogue_inactive');
      }
      else {
        $('.catalogue_list li[data-id="' + id + '"] ').addClass('catalogue_inactive');
      }
    }
  });
}

function inDelete(event) {
  if(! confirm($(this).data('msg')) ) {
    return ;
  }
  var content = $(this).closest('li');
  content.hide();
  $.ajax({
    url: Routing.generate('catalogue_delete', {id: content.data('id'), catalogue:catalogue}),
    dataType: 'json',
    success: function(data){
      console.log(data);
      if(data.status == 'ok'){
        content.remove();
        putAlert(delete_success_msg);
      }
      if(data.status == "error"){
        content.show();
        putAlert(delete_error_msg, 'error');
      }
    }
  });

}

function inAdd(event) {
  $('.alert').remove();
  $.ajax({
    url: Routing.generate('catalogue_load', {id: '-1', catalogue:catalogue}),
    success: function(data){
      $('.catalogue_list').append($('<li></li>').append(data));
      inEdit(-1);
    }
  });
}

function inCancel(event) {
  var form = $(this).closest('form');
  var id = form.data('id');
  form.closest('li').remove();
  $('.catalogue_list li[data-id="' + id + '"] ').show();
  $('.catalogue_list li[data-id="' + id + '"] .edit').show();
}

function inEdit(id) {
  $( "select" ).combobox();
  $('#inline_form button').trigger('click'); // cancel other edit form
  $('#inline_form').submit(inSubmit);
  $('#inline_form button').click(inCancel);
}

function putAlert(msg, level) {
  if( typeof(level) == 'undefined' ) {
    level = 'success';
  }
  $('.alert').remove();
  var alert_html ='<div class="alert alert-' + level + '"> ' +
    ' <button class="close" data-dismiss="alert" type="button">×</button> ' +
    msg + '</div>';
  $('.catalogue_list').before(alert_html);
}

function inSubmit(e) {
  e.preventDefault();
  var form = $(this);
  var id = $(this).data('id');
  $.ajax({
    type: 'POST',
    url: Routing.generate('catalogue_submit', {id: id, catalogue:catalogue}),
    data: form.serialize(),
    success: function(data) {
      if($(data).find('form').length) {
        $('#inline_form').closest('li').html(data).show();
        inEdit(id);
        return;
      }
      putAlert(save_success_msg);
      form.closest('li').remove();
      if(id != '') {
        $('.catalogue_list li[data-id="' + id + '"]').replaceWith(data).show();
      }
      else {
        $('.catalogue_list').append(data);
        id = $(data).data('id');
      }
      CatalogueAttachEvent($('.catalogue_list li[data-id="' + id + '"]'));
    }
  });
  
}

function CatalogueAttachEvent(el) {
  el.find('.edit').click(initInEdit);
  el.find('.delete ').click(inDelete);
  el.find('.toggle_active ').click(inToggleActive);
}
