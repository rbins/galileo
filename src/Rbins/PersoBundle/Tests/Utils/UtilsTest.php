<?php

namespace Rbins\PersoBundle\Tests\Utils;

use Rbins\PersoBundle\Utils\Util;

class UtilsTest extends \PHPUnit_Framework_TestCase
{
  public function testIndex()
  {
    $this->assertEquals("Hello World", Util::unaccent("Héllo World"));
    $this->assertEquals("eo", Util::unaccent("éö"));
  }
}
