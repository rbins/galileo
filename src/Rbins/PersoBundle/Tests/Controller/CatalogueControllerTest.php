<?php

namespace Rbins\PersoBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CatalogueControllerTest extends WebTestCase
{
  public function testList()
  {
    $client = static::createClient(array(), array(
      'PHP_AUTH_USER' => 'usr',
      'PHP_AUTH_PW'   => 'usr',
    ));

    $crawler = $client->request('GET', '/catalogue/Grade/list/');
    $this->assertTrue($crawler->filter('h1:contains("Grade")')->count() > 0);
  }
}
/***

  public function listAction($catalogue) {
    $this->checkCatalogue($catalogue);
    if( in_array($catalogue, array('City', 'MedCenter', 'Department', 'Building'))) {
      $repo = $this->getDoctrine()->getRepository('RbinsPersoBundle:'.$catalogue);
    }else {
      $repo = $this->getDoctrine()->getRepository('RbinsPersoBundle:Catalogue');
    }
    $entities = $repo->findAllCatallogueOrdered($catalogue);
    return $this->render('RbinsPersoBundle:Catalogue:list.html.twig',
      array('items' => $entities, 'catalogue'=> $catalogue));
  }

  public function loadItemAction($catalogue, $id){
    if($id == '-1') {
      $entity = $this->createEmptyEntity($catalogue);
    }
    else {
      $entity = $this->getItem($catalogue, $id);
    }
    $form = $this->createForm($this->createEmptyForm($catalogue), $entity);

    return $this->render('RbinsPersoBundle:Catalogue:catalogue_form.html.twig',
      array('form' => $form->createView(), 'catalogue' => $catalogue));
  }

  public function editItemAction($catalogue, Request $request){
    if($request->query->get('id') === '') { //new
      $entity = $this->createEmptyEntity($catalogue);
    }
    else {
      $entity = $this->getItem($catalogue, $request->query->get('id'));
    }
    $form = $this->createForm($this->createEmptyForm($catalogue), $entity);
    $form->bind($request);
    if ($form->isValid()) {
      $em = $this->getDoctrine()->getManager();
      $em->persist($entity);
      $em->flush();
      return $this->render('RbinsPersoBundle:Catalogue:single_catalogue.html.twig', array('item' => $entity,'catalogue' => $catalogue));
    } else {
      $this->get('logger')->
        err('An error occurred while submiting catalogue: '. var_export($form->getErrors(),true));
      //return new Response('<div class="alert alert-error">'.$this->get('translator')->trans('form.error').'</div>') ;
    return $this->render('RbinsPersoBundle:Catalogue:catalogue_form.html.twig',
      array('form' => $form->createView(), 'catalogue' => $catalogue));
    }
  }


  public function deleteItemAction($catalogue, $id) {
    $entity = $this->getItem($catalogue, $id);

    $em = $this->getDoctrine()->getManager();
    $em->remove($entity);
    $em->flush();
    return new Response(json_encode(array('status'=>'ok'))) ;
  }

  public function toggleItemAction($catalogue, $id, $status) {
    $is_active = ($status == 'false' ? false : true);

    $entity = $this->getItem($catalogue, $id);
    $entity->setIsActive($is_active);
    $em = $this->getDoctrine()->getManager();
    $em->persist($entity);
    $em->flush();
    return new Response(json_encode(array('status'=>'ok','data'=> ($is_active ? 'true':'false')))) ;
  }

  protected function getItem($catalogue, $id) {
    $this->checkCatalogue($catalogue);
    return $this->getDoctrine()
      ->getRepository('RbinsPersoBundle:'.$catalogue)
      ->find($id);
  }

  protected function checkCatalogue($catalogue) {
      if( ! in_array($catalogue, array('Grade', 'Cause', 'Status', 'Reason', 'FunctFamily', 'CataloguePremium',
      'SalaryGrade', 'MaritalStatus', 'ContractDuration', 'ExitReason','SubLevel',
      'City', 'MedCenter', 'Department'))) {
 

*/