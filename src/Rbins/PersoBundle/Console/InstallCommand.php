<?php

namespace Rbins\PersoBundle\Console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class InstallCommand extends Command
{
  protected function configure()
  {
    $this
      ->setName('perso:install')
      ->setDescription('Executes the SQL needed to generate the database schema (/data/db/db.sql)')
//       ->addOption(
//         'em',
//         null,
//         InputOption::VALUE_REQUIRED,
//         'The entity manager to use for this command',
//         'default'
//     )
     ->setHelp(<<<EOT
The <info>perso:install</info> command executes the SQL needed to
generate the database schema for the default connection:

<info>php app/console perso:install</info>
EOT
        );
// 
// You can also generate the database schema for a specific entity manager:
// 
// <info>php app/console perso:install --em=default</info>
// EOT
//         );


  }
  protected function execute(InputInterface $input, OutputInterface $output)
  {
    $file = $this->getApplication()->getKernel()->getRootDir();
    $file = $file.'/../data/db/db.sql';

    $command = $this->getApplication()->find('doctrine:query:sql');

    if( file_exists($file) ) {
      $sqlLines = file_get_contents($file);
      $sqlReqs = explode(';',$sqlLines);

      foreach($sqlReqs as $request) {
        $request = trim($request);
        // Do not exec empty and commented lines
        if( $request == '' || preg_match("/^--/", $request)) {
          continue;
        }
        if( $request == '--END AUTOINSTALL') break;

        $arguments = array(
          'command' => 'doctrine:query:sql',
          'sql'    => $request,
        );

        $input = new ArrayInput($arguments);
        $command->run($input, $output);
      }
    }
  }
}