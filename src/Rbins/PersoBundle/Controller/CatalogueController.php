<?php

namespace Rbins\PersoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;


class CatalogueController extends Controller {

  public function listAction($catalogue) {
    $this->checkCatalogue($catalogue);
    if( in_array($catalogue, array('City', 'MedCenter', 'Department', 'Building'))) {
      $repo = $this->getDoctrine()->getRepository('RbinsPersoBundle:'.$catalogue);
    }else {
      $repo = $this->getDoctrine()->getRepository('RbinsPersoBundle:Catalogue');
    }
    $entities = $repo->findAllCatalogueOrdered($catalogue);
    return $this->render('RbinsPersoBundle:Catalogue:list.html.twig',
      array('items' => $entities, 'catalogue'=> $catalogue));
  }

  public function loadItemAction($catalogue, $id, Request $request){
    if($id == '-1') {
      $entity = $this->createEmptyEntity($catalogue);
    }
    else {
      $entity = $this->getItem($catalogue, $id);
    }
    $form = $this->createForm($this->createEmptyForm($catalogue, $request), $entity);

    return $this->render('RbinsPersoBundle:Catalogue:catalogue_form.html.twig',
      array('form' => $form->createView(), 'catalogue' => $catalogue));
  }

  public function editItemAction($catalogue, Request $request){
    if($request->query->get('id') === '') {
      //new
      $entity = $this->createEmptyEntity($catalogue);
    }
    else {
      $entity = $this->getItem($catalogue, $request->query->get('id'));
    }
    $form = $this->createForm($this->createEmptyForm($catalogue, $request), $entity);
    $form->submit($request);
    if ($form->isValid()) {
      $em = $this->getDoctrine()->getManager();
      $em->persist($entity);
      $em->flush();
      return $this->render('RbinsPersoBundle:Catalogue:single_catalogue.html.twig', array('item' => $entity,'catalogue' => $catalogue));
    } else {
      $this->get('logger')->
        err('An error occurred while submiting catalogue: '. ( (string) $form->getErrors(true, false) ));
      return $this->render('RbinsPersoBundle:Catalogue:catalogue_form.html.twig',
        array('form' => $form->createView(), 'catalogue' => $catalogue));
    }
  }


  public function deleteItemAction($catalogue, $id) {
    $entity = $this->getItem($catalogue, $id);

    $em = $this->getDoctrine()->getManager();
    try {
      $em->remove($entity);
      $em->flush();
    } catch(\Exception $e) {
      $this->get('logger')->info('Remove still ref : '.$catalogue);
      return new Response(json_encode(array('status'=>'error', 'msg' => $e->getMessage()))) ;
    }
    return new Response(json_encode(array('status'=>'ok'))) ;
  }

  public function toggleItemAction($catalogue, $id, $status) {
    $is_active = ($status == 'false' ? false : true);

    $entity = $this->getItem($catalogue, $id);
    $entity->setIsActive($is_active);
    $em = $this->getDoctrine()->getManager();
    $em->persist($entity);
    $em->flush();
    return new Response(json_encode(array('status'=>'ok','data'=> ($is_active ? 'true':'false')))) ;
  }


  public function loadListAction($catalogue, $letter){
    $this->checkCatalogue($catalogue);
    $repo = $this->getDoctrine()->getRepository('RbinsPersoBundle:City');
    $entities = $repo->findAllStartingWith($catalogue, $letter);

    return $this->render('RbinsPersoBundle:Catalogue:items_list.html.twig',
      array('items' => $entities, 'catalogue' => $catalogue));
  }


  protected function getItem($catalogue, $id) {
    $this->checkCatalogue($catalogue);
    return $this->getDoctrine()
      ->getRepository('RbinsPersoBundle:'.$catalogue)
      ->find($id);
  }

  protected function checkCatalogue($catalogue) {
      if( ! in_array($catalogue, array('Grade', 'Cause', 'Status', 'Reason', 'FunctFamily', 'CataloguePremium',
      'SalaryGrade', 'MaritalStatus', 'ContractDuration', 'ExitReason','SubLevel','Country','ContractComment',
      'City', 'MedCenter', 'Department', 'DocType', 'Degree','Frame','WorkFunction'))) {

        throw $this->createNotFoundException('The Catalogue does not exist');
      }
  }

  protected function createEmptyEntity($catalogue) {
    $this->checkCatalogue($catalogue);
    $e_str= "Rbins\PersoBundle\Entity\\".$catalogue;
    return $entity = new $e_str;
  }

  protected function createEmptyForm($catalogue, Request $request) {
    $this->checkCatalogue($catalogue);
    $e_str= "Rbins\PersoBundle\Form\\".$catalogue."Type";
    return $entity = new $e_str($request->getLocale());
  }
}
