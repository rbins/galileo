<?php
namespace Rbins\PersoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Security\Csrf\CsrfTokenManager;
use Symfony\Component\HttpFoundation\Request;

class SecurityController extends Controller
{
    public function loginAction(Request $request)
    {
        $session = $request->getSession();
        $referer = $request->headers->get('referer');
        if(strpos($referer,$this->container->getParameter('phonebook_url').$this->container->getParameter('hr_domain'))) $hr = false ;
        else $hr = true ;
        // get the login error if there is one
        if ($request->attributes->has(Security::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(Security::AUTHENTICATION_ERROR);
        } else {
            $error = $session->get(Security::AUTHENTICATION_ERROR);
            $session->remove(Security::AUTHENTICATION_ERROR);
        }
        return $this->render('RbinsPersoBundle:Security:login.html.twig', array(
            // last username entered by the user
            'hr_referer' => $hr,
            'last_username' => $session->get(Security::LAST_USERNAME),
            'error'         => $error,
            'token'         => $this->generateToken(),
        ));
    }

    private function generateToken()
    {
        $token = $this->get('security.csrf.token_manager')
            ->getToken('ldap_authenticate');

        return $token;
    }

    public function loginRedirectAction(Request $request)
    {
        $currentRoute = $request->getUri();
        if(strpos($currentRoute,$this->container->getParameter('phonebook_url').$this->container->getParameter('hr_domain'))) {
            return $this->redirect($this->generateUrl('phonebook_profile'));
        }
        if( $this->container->get('security.authorization_checker')->isGranted('ROLE_HR_VIEWER')) {
            return $this->redirect($this->generateUrl('welcome'));
        }
        else {
            return $this->redirect($this->generateUrl('phonebook_profile'));
        }
    }
}