<?php

namespace Rbins\PersoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Rbins\PersoBundle\Entity\Grade;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Rbins\PersoBundle\Utils\ReportFetcher;

class DefaultController extends Controller {

  public function indexAction(Request $request) {
    $session = $request->getSession();
    return $this->render('RbinsPersoBundle:Default:index.html.twig');
  }

  public function reportListAction() {
    $list = ReportFetcher::getGlobalReports($this);
    return $this->render('RbinsPersoBundle:Default:list_reports.html.twig', array('reports' => $list));
  }

  public function getReportFormAction($name) {
    $list = ReportFetcher::getGlobalReports($this);
    return $this->render('RbinsPersoBundle:Default:list_reports.html.twig', array('reports' => $list));
  }

  public function getReportAction($report, Request $request) {
    $parameters = array();
    $format = $request->get('format') ;
    $ulist = ReportFetcher::getUsersReports();
    $glist = ReportFetcher::getGlobalReports($this);

    if($request->get('lang') != null)
      $parameters['lang'] = $request->get('lang');
    $parameters['person_id'] = $request->get('person_id');
    $parameters['locale'] = $request->getLocale() ;
    if(isset($ulist[$report])) {
      $parameters['person_id'] = $request->get('person_id');
    }
    elseif(isset($glist[$report]['form'])) {
      $form = $glist[$report]['form'];
      $form->submit($request);

      if ($form->isValid()) {
        $datas  =  $form->getData();
        foreach($datas as $k => $v) {
          if( is_object($v) && get_class($v) == "DateTime") {
            $v = $v->format('Y-m-d');
          }
          if( is_object($v))
            $v = $v->getId();
          $parameters[$k] = $v;
        }
      }
    }
    if( isset($datas['format']) ) {
      $format = $datas['format'];
    }
    elseif ($request->get('format') == '' && isset($ulist[$report]['format'])){
      $format = $ulist[$report]['format'];
    }
    $srv = $this->container->getParameter('report_server');
    ReportFetcher::get($srv, $report, $parameters, $format, $this->get('logger'));
  }

  public function searchPersonAction(Request $request) {
    if($request->query->get('id') != '') {
      $item = $this->getDoctrine()
        ->getRepository('RbinsPersoBundle:Person')
        ->find($request->query->get('id'));
      return new Response(json_encode(array('label' => (string)$item,'is_active' =>true)));
    } else {
      $results = $this->getDoctrine()
        ->getRepository('RbinsPersoBundle:Person')
        ->searchInName($request->query->get('term'), $request->query->get('exact', false) );
      return new Response(json_encode($results));
    }
  }

  public function autocompleteAction($catalogue, Request $request) {
    $entity = 'Catalogue';
    if(false !== array_search($catalogue, array('City','Department','MedCenter') ))
      $entity = $catalogue;

    $repo = $this->getDoctrine()
        ->getRepository('RbinsPersoBundle:'.$entity);
    if($request->query->get('id') != '') {
      $item = $repo->find($request->query->get('id'));
      $active = method_exists($item, 'getIsActive')? $item->getIsActive() : true;

      return new Response(json_encode(array(
        'label' => $item->getLabel($request->getLocale()),
        'is_active' =>$active
      )));
    }
    else {
      $results = $repo->searchComplete(
        $catalogue,
        $request->query->get('term'),
        $request->query->get('exact', false),
        $request->getLocale()
      );
      return new Response(json_encode($results));
    }

  }

  public function fileAction($id) {
    $em = $this->getDoctrine()->getManager();
    $entity = $em->getRepository('RbinsPersoBundle:Document')->find($id);
    if (!$entity) {
        throw $this->createNotFoundException('Unable to find document.');
    }
    $headers = array(
        'Content-Type' => 'octet/stream',
        'Content-Disposition' => 'attachment; filename="'.$entity->getName().'"'
    );
    return new Response(file_get_contents($entity->getAbsolutePath()), 200, $headers);
  }

  public function chartAction(Request $request) {
    $departments = $this->getDoctrine()
      ->getRepository('RbinsPersoBundle:Department')
      ->findAllDepts($request->getLocale());
    $persons = $this->getDoctrine()
      ->getRepository('RbinsPersoBundle:Person')
      ->getPersonByWorkingHist();
    return $this->render('RbinsPersoBundle:Default:chart.html.twig',array('departments' => $departments,'persons'=> $persons));
  }
}
