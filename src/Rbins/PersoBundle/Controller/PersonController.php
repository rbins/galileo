<?php

namespace Rbins\PersoBundle\Controller;

use Rbins\PersoBundle\Entity\Person;
use Rbins\PersoBundle\Form\PersonDutyCalendarType;
use Rbins\PersoBundle\Form\PersonRelatedType;
use Rbins\PersoBundle\Form\PersonType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Rbins\PersoBundle\Utils\ReportFetcher;

class PersonController extends Controller {

  public function PersonAction($id, $action, Request $request) {
    if($action == 'add') {
      $person = new Person();
    } else {
      $person = $this->findPerson($id, true);
    }
    if($action == 'view')
      return $this->render('RbinsPersoBundle:PersonView:person.html.twig', array('id'=> $person->getId(), 'person' => $person));

    $form = $this->createForm(
        PersonType::class,
        $person,
        array(
            'lang'=>$request->getLocale()
        )
    );
    if ($request->getMethod() == 'POST') {
      $form->submit($request);

      if ($form->isValid()) {
        $em = $this->getDoctrine()->getManager();
        $em->persist($person);
        $em->flush();
        $this->get('session')->getFlashBag()->add('success', 'perso.form.save.success');        
        return $this->redirect($this->generateUrl('person', array('id'=> $person->getId(), 'action' => 'edit')));
      }
      else {
        $this->get('logger')->
          err('An error occurred while submiting person: '. var_export($form->getErrors(),true));
      }
    }
    return $this->render('RbinsPersoBundle:Person:person.html.twig',
      array('form' => $form->createView(), 'id' => $id));
  }

  public function personTitleAction($id) {
    $person = $this->getDoctrine()
      ->getRepository('RbinsPersoBundle:Person')
      ->getPersonSummary($id);
    $now = new \DateTime ;
    if($person[0]['exit_date'] < $now)
      $info_transport = $this->getDoctrine()->getRepository('RbinsPersoBundle:Transport')->getStib($id,$person[0]['exit_date']) ;
    else $info_transport = false ;
    return $this->render('RbinsPersoBundle:Person:block_title.html.twig', array('person' => $person[0],'info' => $info_transport));
  } 


  public function HistAction($id, $action, Request $request) {  
    $person = $this->findPerson($id, true);
    $oHists = array();
    $oDuties = array() ;
    foreach ($person->getWorkingHist() as $hist) $oHists[] = $hist;
    foreach ($person->getWorkingDuty() as $duty) $oDuties[] = $duty;
    $cnt_hist = count($oHists) ;
    $cnt_duty = count($oDuties) ;
    if($action == 'view') return $this->render('RbinsPersoBundle:PersonView:hist.html.twig',array('id'=> $id,'person'=>$person,'hists'=>$oHists,'duties'=>$oDuties,'lang'=>$request->getLocale()));
    $form = $this->createForm(
        PersonRelatedType::class,
        $person,
        array(
            'lang'=>$request->getLocale(),
            'entrytype'=>'hist'
        )
    );
    if ($request->getMethod() == 'POST') {
      $form->submit($request);
      if ($form->isValid()) {
        $em = $this->getDoctrine()->getManager();
        $this->manageEmbbed($person->getWorkingHist(), $em, 'setPerson', $oHists);
        $this->manageEmbbed($person->getWorkingDuty(), $em, 'setPerson', $oDuties);
        $em->persist($person);
        $em->flush();
        if($cnt_duty == 0 && count($person->getWorkingDuty()) > 0) {
            $this->sendEmailNewPerson($person);
        }
        $this->get('session')->getFlashBag()->add('success', 'perso.form.save.success');        
        return $this->redirect($this->generateUrl('person_hist', array('id'=> $id)));
      }
      else {
        $this->get('logger')->
          err('An error occurred while submiting person: '. ( (string) $form->getErrors(true, false) ) );
      }
    }

    return $this->render('RbinsPersoBundle:Person:hist.html.twig',
      array("id"=> $id, 'form' => $form->createView()));
  }

  public function ContactAction($id, $action, Request $request) {
    $person = $this->findPerson($id, true);
    $Contacts = $this->getDoctrine()->getRepository('RbinsPersoBundle:PersonContact')->getContact($id);
    $oContacts = array();
    $oAddress = array() ;
    foreach ($Contacts as $contact) $oContacts[] = $contact;
    foreach ($person->getPersonAddress() as $adr) $oAddress[] = $adr;
    if($action == 'view') {
      return $this->render('RbinsPersoBundle:PersonView:contact.html.twig', array(
        'id'=> $person->getId(), 'contacts' => $oContacts, 'addresses' => $oAddress, 'person'=> $person));
    }

    $form = $this->createForm(
        PersonRelatedType::class,
        $person,
        array(
            'lang'=>$request->getLocale(),
            'entrytype'=>'contact'
        )
    );

    if ($request->getMethod() == 'POST')
    {
      $form->submit($request);
      if ($form->isValid())
      {
        $em = $this->getDoctrine()->getManager();
        $this->manageEmbbed($person->getPersonContact(), $em, 'setPerson', $oContacts);
        $this->manageEmbbed($person->getPersonAddress(), $em, 'setPersonRef', $oAddress);
        $em->persist($person);
        $em->flush();
        $this->get('session')->getFlashBag()->add('success', 'perso.form.save.success');
      }
      else {
        $this->get('logger')->
          err('An error occurred while submiting person: '.  ( (string) $form->getErrors(true, false) ) );
      }
    }
    return $this->render('RbinsPersoBundle:Person:contact.html.twig',array("id"=> $id, 'form' => $form->createView()));  
  }
  
  public function CalendarAction($id, $hist, $action, Request $request) {
    $person = $this->findPerson($id, true);
    $wh = $this->findHist($hist, true);
    $oCals = array();
    foreach ($wh->getCalendar() as $cal) $oCals[] = $cal;
    if($action == 'view')
      return $this->render('RbinsPersoBundle:PersonView:calendar.html.twig', array('id'=> $id, 'calendar' => $oCals));

    $form = $this->createForm(
        PersonDutyCalendarType::class,
        $wh,
        array(
            'lang'=>$request->getLocale()
        )
    );

    if ($request->getMethod() == 'POST') 
    {
      $form->submit($request);
      if ($form->isValid())
      {
        $em = $this->getDoctrine()->getManager();
        $this->manageEmbbed($wh->getCalendar(), $em, 'setWorkingDuty', $oCals);
        $em->persist($wh);
        $em->flush();
        $this->get('session')->getFlashBag()->add('success', 'perso.form.save.success');
        return $this->redirect($this->generateUrl('person_calendar', array('id'=> $id, "person"=> $person, 'hist' => $hist)));
      }
      else 
      {
        $this->get('logger')->
          err('An error occurred while submiting a calendar: '.  ( (string) $form->getErrors(true, false) ));
      }
    }
    return $this->render('RbinsPersoBundle:Person:calendar.html.twig',
      array('id'=> $id, "person"=> $person, 'hist' => $hist, 'form' => $form->createView()));
  }

  public function FamilyAction($id, $action, Request $request) {
    $person = $this->findPerson($id, true);

    $oFamily = array();
    foreach ($person->getPersonFamily() as $family) $oFamily[] = $family;
    if($action=='view') return $this->render('RbinsPersoBundle:PersonView:family.html.twig', array('id'=> $person->getId(), 'family' => $oFamily));
    $form = $this->createForm(
        PersonRelatedType::class,
        $person,
        array(
            'lang' => $request->getLocale(),
            'entrytype'=>'family'
        )
    );

    if ($request->getMethod() == 'POST') {
      $form->submit($request);
      if ($form->isValid()) {
        $em = $this->getDoctrine()->getManager();
        $this->manageEmbbed($person->getPersonFamily(), $em, 'setPerson', $oFamily);
        $em->persist($person);
        $em->flush();
        $this->get('session')->getFlashBag()->add('success', 'perso.form.save.success');
        return $this->redirect($this->generateUrl('person_family', array('id'=> $id)));
      }
      else {
        $this->get('logger')->
          err('An error occurred while submiting childrens '.  ( (string) $form->getErrors(true, false) ) );
      }
    }

    return $this->render('RbinsPersoBundle:Person:family.html.twig',
      array("id"=> $id, 'form' => $form->createView()));
  }
  
  // Public function PremiumAction
  public function PremiumAction($id, $action, Request $request) {
    $person = $this->findPerson($id, true);

    $oPremium = array();
    foreach ($person->getPremium() as $premium) $oPremium[] = $premium;
    if($action=='view') return $this->render('RbinsPersoBundle:PersonView:premium.html.twig', array('id'=> $id, 'premium' => $oPremium));
    $form = $this->createForm(
        PersonRelatedType::class,
        $person,
        array(
            'lang' => $request->getLocale(),
            'entrytype' => 'premium'
        )
    );

    if ($request->getMethod() == 'POST') {
      $form->submit($request);
      if ($form->isValid()) {
        $em = $this->getDoctrine()->getManager();
        $this->manageEmbbed($person->getPremium(), $em, 'setPerson', $oPremium);
        $em->persist($person);
        $em->flush();
        $this->get('session')->getFlashBag()->add('success', 'perso.form.save.success');
        return $this->redirect($this->generateUrl('person_premium', array('id'=> $id)));
      }
      else {
        $this->get('logger')->
          err('An error occurred while submiting childrens '.  ( (string) $form->getErrors(true, false) ) );
      }
    }

    return $this->render('RbinsPersoBundle:Person:premium.html.twig',
      array("id"=> $id, 'form' => $form->createView()));
  }

  // Public function TransportAction
  public function TransportAction($id, $action, Request $request) {
    $person = $this->findPerson($id, true);

    $oTransport = array();
    foreach ($person->getTransport() as $trans) $oTransport[] = $trans;
    if($action=='view') return $this->render('RbinsPersoBundle:PersonView:transport.html.twig', array('id'=> $id, 'transport'=>$oTransport));
    $form = $this->createForm(
        PersonRelatedType::class,
        $person,
        array(
            'lang' => $request->getLocale(),
            'entrytype' => 'transport'
        )
    );

    if ($request->getMethod() == 'POST') {
      $form->submit($request);
      if ($form->isValid()) {
        $em = $this->getDoctrine()->getManager();
        $this->manageEmbbed($person->getTransport(), $em, 'setPerson', $oTransport);
        $em->persist($person);
        $em->flush();
        $this->get('session')->getFlashBag()->add('success', 'perso.form.save.success');
        return $this->redirect($this->generateUrl('person_transport', array('id'=> $id)));
      }
      else {
        $this->get('logger')->
          err('An error occurred while submiting childrens '.  ( (string) $form->getErrors(true, false) ) );
      }
    }

    return $this->render('RbinsPersoBundle:Person:transport.html.twig',
      array("id"=> $id, 'form' => $form->createView()));
  }


  public function EntryAction($id, $action, Request $request) {
    $person = $this->findPerson($id, true);

    $oEntry = array();
    foreach ($person->getPersonEntry() as $entry) $oEntry[] = $entry;

    if($action == 'view')
      return $this->render('RbinsPersoBundle:PersonView:entry.html.twig', array('id'=> $id, 'entry' => $oEntry));
  
    $form = $this->createForm(
        PersonRelatedType::class,
        $person,
        array(
            'lang' => $request->getLocale(),
            'entrytype' => 'entry'
        )
    );

    if ($request->getMethod() == 'POST') {
      $form->submit($request);
      if ($form->isValid()) {
        $em = $this->getDoctrine()->getManager();
        $this->manageEmbbed($person->getPersonEntry(), $em, 'setPerson', $oEntry);
        $em->persist($person);
        $em->flush();
        $pers = $this->getDoctrine()->getRepository('RbinsPersoBundle:Person')->getPersonSummary($id);
        // if an exit_date is specified, put that date to end_date in status and duties where this date is undefined (null)
        if($pers[0]['exit_date'] != null) 
        {
          $this->getDoctrine()->getRepository('RbinsPersoBundle:WorkingHist')->stopHist($id,$pers[0]['exit_date']->format('Y/m/d'));
          $this->getDoctrine()->getRepository('RbinsPersoBundle:WorkingDuty')->stopDuty($id,$pers[0]['exit_date']->format('Y/m/d'));
        }
        $this->get('session')->getFlashBag()->add('success', 'perso.form.save.success');
        return $this->redirect($this->generateUrl('person_entry', array('id'=> $id)));
      }
      else {
        $this->get('logger')->
          err('An error occurred while submiting childrens '.  ( (string) $form->getErrors(true, false) ) );
      }
    }

    return $this->render('RbinsPersoBundle:Person:entry.html.twig',
      array("id"=> $id, 'form' => $form->createView()));
  }
  
  public function CombinedJobAction($id, $action, Request $request) {
    $person = $this->findPerson($id, true);

    $oJob = array();
    foreach ($person->getCombinedJob() as $job) $oJob[] = $job;

    if($action == 'view')
      return $this->render('RbinsPersoBundle:PersonView:combined_job.html.twig', array('id'=> $id, 'job' => $oJob));
  
    $form = $this->createForm(
        PersonRelatedType::class,
        $person,
        array(
            'lang' => $request->getLocale(),
            'entrytype' => 'combined_job'
        )
    );

    if ($request->getMethod() == 'POST') {
      $form->submit($request);
      if ($form->isValid()) {
        $em = $this->getDoctrine()->getManager();
        $this->manageEmbbed($person->getCombinedJob(), $em, 'setPerson', $oJob);
        $em->persist($person);
        $em->flush();
        $this->get('session')->getFlashBag()->add('success', 'perso.form.save.success');
        return $this->redirect($this->generateUrl('person_combined', array('id'=> $id)));
      }
      else {
        $this->get('logger')->
          err('An error occurred while submiting childrens '.( (string) $form->getErrors(true, false) ) );
      }
    }

    return $this->render('RbinsPersoBundle:Person:combined_job.html.twig',
      array("id"=> $id, 'form' => $form->createView()));
  }  

  public function DocumentAction($id, $action, Request $request) {
    $person = $this->findPerson($id, true);
    $oDoc = array() ;
    $Documents = $this->getDoctrine()->getRepository('RbinsPersoBundle:Document')->getDocument($id);
    foreach ($Documents as $doc) $oDoc[] = $doc;
    if($action=='view') return $this->render('RbinsPersoBundle:PersonView:document.html.twig', array('id'=> $id, 'document'=>$oDoc, 'lang' => $request->getLocale(),));
    $form = $this->createForm(
        PersonRelatedType::class,
        $person,
        array(
            'lang' => $request->getLocale(),
            'entrytype' => 'document'
        )
    );

    if ($request->getMethod() == 'POST') {
      $form->submit($request);
      if ($form->isValid()) {
        $em = $this->getDoctrine()->getManager();
        $this->manageEmbbed($person->getDocument(), $em, 'setPerson', $oDoc);
        $em->persist($person);
        $em->flush();
        $this->get('session')->getFlashBag()->add('success', 'perso.form.save.success');
        return $this->redirect($this->generateUrl('person_document', array('id'=> $id)));
      }
      else {
        $this->get('logger')->
          err('An error occurred while submiting childrens '.  ( (string) $form->getErrors(true, false) ) );
      }
    }

    return $this->render('RbinsPersoBundle:Person:document.html.twig',
      array("id"=> $id, 'lang' => $request->getLocale(), 'form' => $form->createView()));
  }

  public function ReportsAction($id, $action, Request $request) {
    $person = $this->findPerson($id, true);
    $reports = ReportFetcher::getUsersReports();
    return $this->render('RbinsPersoBundle:Person:reports.html.twig',
      array("id"=> $id,"person"=> $person, 'reports' =>$reports));
  }

  private function manageEmbbed($collection, $em, $ref_fct, $old_coll) {
    foreach ($collection as $hist) {
      foreach ($old_coll as $key => $toDel) {
        if ($toDel->getId() === $hist->getId()) {
          unset($old_coll[$key]);
        }
      }
    }
    foreach ($old_coll as $hist) {
      $hist->$ref_fct(null);
      $em->remove($hist);
    }
  }
  
  private function findPerson($id, $throw) {
    $person = $this->getDoctrine()
      ->getRepository('RbinsPersoBundle:Person')
      ->findOneById($id);
    if ($throw && ! $person) {
      throw $this->createNotFoundException('The person does not exist');
    }
    return $person;
  }

  private function findHist($id, $throw) {
    $hist = $this->getDoctrine()
      ->getRepository('RbinsPersoBundle:WorkingDuty')
      ->findOneById($id);
    if ($throw && ! $hist) {
      throw $this->createNotFoundException('The working hist does not exist');
    }
    return $hist;
  }  
  
  private function sendEmailNewPerson($person)
  {
    $email = $this->container->getParameter('support_mail_receiver');
    if(!$email) return ; // email not set, so no mail to send
    $person = $this->findPerson($person->getId(), true);
    $duty = $this->getDoctrine()->getRepository('RbinsPersoBundle:WorkingDuty')->getLastDuty($person->getId());
    $hist =  $this->getDoctrine()->getRepository('RbinsPersoBundle:WorkingHist')->getLastHist($person->getId());
    $hasHist = true;
    if ($hist !== null) {
        $hasHist = !in_array($hist->getStatus()->getCode(), array('12','20'));
    }
    if($hasHist)
    {
      $message = \Swift_Message::newInstance()
          ->setSubject('LDAP adding new person request')
          ->setFrom('HR@noreply.be')
          ->setTo($email)
          ->setBody(
              $this->renderView(
                  'RbinsPersoBundle:Person:email.txt.twig',
                  array('person' => $person, 'dept' => $duty->getDepartment())
              )
          )
      ;
      $this->get('mailer')->send($message);  
    }
  }

  public function PhoneBookAction($id, $action, Request $request) {
    $person = $this->findPerson($id, true);
    $infos = $this->getDoctrine()->getRepository('RbinsPersoBundle:PhoneBook')->getInfos($id);
    $photo = $person->getPhoto()?$person->getPhoto():'#' ;
    $phonebook = array();
    foreach ($infos as $info) $phonebook[] = $info;
    if($action == 'view') {
      return $this->render('RbinsPersoBundle:PersonView:phonebook.html.twig', array(
        'id'=> $person->getId(), 'infos' => $phonebook, 'person'=> $person));
    }

    $form = $this->createForm(
        PersonRelatedType::class,
        $person,
        array(
            'lang' => $request->getLocale(),
            'entrytype' => 'phonebook',
            'id' => $person->getId()
        )
    );
    if ($request->getMethod() == 'POST') 
    {
      $form->submit($request);
      if ($form->isValid())
      {
        $person->upload() ;
        $em = $this->getDoctrine()->getManager();
        $this->manageEmbbed($person->getPhoneBook(), $em, 'setPerson', $phonebook);
        $em->persist($person);
        $em->flush();
        $cacheManager = $this->container->get('liip_imagine.cache.manager');
        $photo = $person->getPhoto()?$person->getPhoto():'#' ;
        if($photo != '#') $cacheManager->remove('/photos/'.$photo,'pb_photo');
        $this->get('session')->getFlashBag()->add('success', 'perso.form.save.success');
      }
      else {
        $this->get('logger')->
          err('An error occurred while submiting person: '.  ( (string) $form->getErrors(true, false) ));
      }
    }
    return $this->render('RbinsPersoBundle:Person:phonebook.html.twig',array("id"=> $id,  'photo' => $photo,'form' => $form->createView()));  
  }  
  
}

