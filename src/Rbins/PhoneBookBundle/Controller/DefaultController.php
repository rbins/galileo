<?php

namespace Rbins\PhoneBookBundle\Controller;

use Rbins\PersoBundle\Entity\Person;
use Rbins\PersoBundle\Form\PersonRelatedType;
use Rbins\PersoBundle\Form\PersonType;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Yaml\Parser;
use Rbins\PersoBundle\Utils\Util as Util;

class DefaultController extends Controller
{

  public function indexAction(Request $request)
  {
    $alphas = range('A', 'Z');
    return $this->render('RbinsPhoneBookBundle:Default:index.html.twig', array('alpha' => $alphas, 'intranet' => $this->isInternalIp($request->getClientIp())));
  }

  public function profileAction(Request $request)
  {        
    $person = $this->getDoctrine()
      ->getRepository('RbinsPersoBundle:Person')
      ->findOneByUid(strtolower($this->get('security.token_storage')->getToken()->getUser()->getUserName()));
    if (! $person) {
      $this->sendProfileEmailError($this->get('security.token_storage')->getToken()->getUser()->getUserName()) ;
      return $this->render('RbinsPhoneBookBundle:Default:profile.html.twig',array("id"=> null, 'intranet' => $this->isInternalIp($request->getClientIp())));
    }
    $old_building = array('building'=> $person->getBuilding(),'floor' => $person->getBuildingFloor(), 'room' => $person->getBuildingRoom()) ;
    $anonymous = "anonymous_".($person->getGender()=="M"?"man":"woman").".png" ;
    $photo = $person->getPhoto()?$person->getPhoto():$anonymous ;
    $infos = $this->getDoctrine()->getRepository('RbinsPersoBundle:PhoneBook')->getInfos($person->getId());
    $preview = $this->getPersonPhonebook($person->getId(),'justone') ;
    $all_dpts = $this->formatedDpt($request->getLocale());
    $phonebook = array();
    foreach ($infos as $info) $phonebook[] = $info;
    $form = $this->createForm(
        PersonRelatedType::class,
        $person,
        array(
            'lang' => $request->getLocale(),
            'entrytype' => 'phonebook',
            'id' => $person->getId(),
            'securityContext' => $this->get('security.authorization_checker')->isGranted("ROLE_HR_ADMIN")
        )
    );
    if ($request->getMethod() == 'POST') 
    {
      $form->submit($request);
      if ($form->isValid())
      {
        $cacheManager = $this->container->get('liip_imagine.cache.manager');        
        if($photo != $anonymous) $cacheManager->remove('/photos/'.$photo,'pb_photo');
        if(isset($request->request->get('rbins_persobundle_personworkingtype')['removephoto']))
        { 
          $person->removeUpload() ;
          $photo = $anonymous ;
          $cacheManager->remove('/photos/'.$photo,'pb_photo');
        }
        else 
        {
          $person->upload() ;
          $photo = $person->getPhoto() ;
        }
        $this->checkLocality($person,$old_building) ;
        $em = $this->getDoctrine()->getManager();
        $this->manageEmbbed($person->getPhoneBook(), $em, 'setPerson', $phonebook);
        $em->persist($person);
        $em->flush();
        $this->get('session')->getFlashBag()->add('success', 'perso.form.save.success');
      }
      else {
        $this->get('logger')->
          err('An error occurred while submiting person: '.  ((string) $form->getErrors(true, false) ));
      }
    }
    else {
      $this->get('session')->getFlashBag()->add('info', 'perso.phonebook.warning');
    }
    return $this->render('RbinsPhoneBookBundle:Default:profile.html.twig',array('id' => $person->getId(),
                "person"=> $preview[$person->getId()],
                "photo"=>$photo,
                'dept' => $all_dpts,
                'form' => $form->createView(),
                'intranet' => true));
  }

  public function searchPersonAction(Request $request) {
    $all_dpts = $this->formatedDpt($request->getLocale());
    $person = $this->getPersonPhonebook($request->query->get('term'), $request->query->get('type'));
    return $this->render('RbinsPhoneBookBundle:Default:result.html.twig', array('persons'=>$person, 'dept'=> $all_dpts, 'intranet' => $this->isInternalIp($request->getClientIp())));
  }

  public function usefullNumAction()
  {
    $yaml = new Parser();
    $infos = $yaml->parse(file_get_contents(__DIR__.'/../Resources/doc/usefull.yml'));
    return $this->render('RbinsPhoneBookBundle:Default:usefull_num.html.twig',array('infos'=>$infos, 'intranet' => true)) ;
  }

  public function chartAction(Request $request)
  {
    $departments = $this->getDoctrine()
      ->getRepository('RbinsPersoBundle:Department')
      ->findAllDepts($request->getLocale());
    return $this->render('RbinsPhoneBookBundle:Chart:index.html.twig',array('departments' => $departments, 'intranet' => $this->isInternalIp($request->getClientIp())));
  }

  public function searchByDepartmentAction(Request $request)
  {        
    $all_dpts = $this->formatedDpt($request->getLocale());
    $person = $this->getPersonPhonebook($request->query->get('dept'),'dept');
    return $this->render('RbinsPhoneBookBundle:Default:result.html.twig', array('persons'=>$person, 'dept'=> $all_dpts, 'intranet' => $this->isInternalIp($request->getClientIp())));
  }

  private function getPersonPhonebook($term, $type)
  {
    $results = $this->getDoctrine()
      ->getRepository('RbinsPersoBundle:Person')
      ->searchPbUsers($term, $type);
    $person = array() ;    
    foreach($results as $result)
    {
      if(!isset($person[$result['value']]))
      {
        $anonymous = $result['sexe'] == 'M' ? 'anonymous_man.png' : 'anonymous_woman.png';
        $person[$result['value']] = array(
          'name' => $result['lastname']." ".$result['firstname'],
          'description' => $result['description'],
          'situation' => $result['situation'],
          'functionfr' => $result['functionfr'],
          'functionnl' => $result['functionnl'],
          'fixemail' => $result['fixemail'], 
          'photo' => $result['photo']?$result['photo']:$anonymous,
          'building' => $result['building'],
          'floor' => $result['floor'],
          'room' => $result['room'],
          'department' => explode(',',$result['dpt']),
          'other' => array(),
          'hobbies' => $result['hobbies'],
          'phone' => '',
          'fax' => '',
          'gsm' => ''
        );
      }
      if($result['info_type']!= '')
      {
        if(($result['info_type'] != 'email') && ($person[$result['value']][$result['info_type']] == ''))
          $person[$result['value']][$result['info_type']] = $this->setFormating($result['entry']) ;
        else
          $person[$result['value']]['other'][] = array('type' =>$result['info_type'], 'value' => $this->setFormating($result['entry'])) ;        
      }
    }
    return $person ;
  }

  private function manageEmbbed($collection, $em, $ref_fct, $old_coll)
  {
    foreach ($collection as $hist) {
      foreach ($old_coll as $key => $toDel) {
        if ($toDel->getId() === $hist->getId()) {
          unset($old_coll[$key]);
        }
      }
    }
    foreach ($old_coll as $hist) {
      $hist->$ref_fct(null);
      $em->remove($hist);
    }
  }

  public function autocompleteAction($catalogue, Request $request) {
    $repo = $this->getDoctrine()
        ->getRepository('RbinsPersoBundle:Department');
    $results = $repo->searchComplete(
        'Department',
        $request->query->get('term'),
        $request->query->get('exact', false),
        $request->getLocale()
      );
      return new Response(json_encode($results));
  }
  private function sendProfileEmailError($uid)
  {  
    $message = \Swift_Message::newInstance()
      ->setSubject('Galileo erreur dans un Profil')
      ->setFrom('noreply@naturalsciences.be')
      ->setTo($this->container->getParameter('admin_mail_receiver'))
      ->setBody("$uid n'a pas pu accéder à son profil")
      ;
    $this->get('mailer')->send($message);  
  }

  private function checkLocality($new_data,$old_data)
  {
    $person = $new_data->getLastName()." ".$new_data->getFirstName(); 
    if($new_data->getBuilding() != $old_data['building'] || $new_data->getBuildingFloor() != $old_data['floor'] || $new_data->getBuildingRoom() != $old_data['room']) 
    {
      $message = \Swift_Message::newInstance()
        ->setSubject('Phonebook - Profile change')
        ->setFrom('noreply@naturalsciences.be')
        ->setTo($this->container->getParameter('checker_group_mail'))
        ->setBody("$person changed is locality :\n
          Previously : ".$old_data['building'].", floor ".$old_data['floor']." room ".$old_data['room']."\n
          Now : ".$new_data->getBuilding().", floor ".$new_data->getBuildingFloor()." room ".$new_data->getBuildingRoom()."\n
----------------------------------------------------------------------------------------------------
$person a changé sa localité :\n
          Avant : ".$old_data['building'].", étage ".$old_data['floor']." pièce ".$old_data['room']."\n
          Maintenant : ".$new_data->getBuilding().", étage ".$new_data->getBuildingFloor()." pièce ".$new_data->getBuildingRoom()."\n
----------------------------------------------------------------------------------------------------
$person heeft zijn gegevens ( lokaal, gebouw, ... ) gewijzigd :\n
          Eerder : ".$old_data['building'].", vloer ".$old_data['floor']." kantor ".$old_data['room']."\n
          Nu : ".$new_data->getBuilding().", vloer ".$new_data->getBuildingFloor()." kantor ".$new_data->getBuildingRoom()."\n
          "
          )
        ;
      $this->get('mailer')->send($message);
    }    
  }

  private function setFormating($value)
  {
    $nbr = preg_replace("/(-| )+/","",$value) ;
    if(ctype_digit($nbr))
    {
      if(strlen($nbr) == 10) return preg_replace("/(\d{4})(\d{2})(\d{2})(\d{2})/","$1.$2.$3.$4",$nbr) ;
      if(strlen($nbr) == 9) 
      {
        switch(substr($nbr, 0, 2))  //let's check prefixe to format 
        {
            case '01' : return preg_replace("/(\d{3})(\d{2})(\d{2})(\d{2})/","$1.$2.$3.$4",$nbr) ; break ;
            case '02' : return preg_replace("/(\d{2})(\d{3})(\d{2})(\d{2})/","$1.$2.$3.$4",$nbr) ; break ;
            case '03' : return preg_replace("/(\d{2})(\d{3})(\d{2})(\d{2})/","$1.$2.$3.$4",$nbr) ; break ;
            case '04' : return preg_replace("/(\d{2})(\d{3})(\d{2})(\d{2})/","$1.$2.$3.$4",$nbr) ; break ;
            case '05' : return preg_replace("/(\d{3})(\d{2})(\d{2})(\d{2})/","$1.$2.$3.$4",$nbr) ; break ;
            case '06' : return preg_replace("/(\d{3})(\d{2})(\d{2})(\d{2})/","$1.$2.$3.$4",$nbr) ; break ;
            case '07' : return preg_replace("/(\d{3})(\d{2})(\d{2})(\d{2})/","$1.$2.$3.$4",$nbr) ; break ;
            case '08' : return preg_replace("/(\d{3})(\d{2})(\d{2})(\d{2})/","$1.$2.$3.$4",$nbr) ; break ;
            case '09' : return preg_replace("/(\d{2})(\d{3})(\d{2})(\d{2})/","$1.$2.$3.$4",$nbr) ; break ;
        }
      }
    }
    return $value ;
  }

  private function isInternalIp($ip)
  {
    if($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) return true ; //logged so ok
    if(substr($ip, 0, 8) == '192.168.') return true ; //local museum ( temp till no more exceptions )
    if(substr($ip, 0, 6) == '10.23.') return true ; //local museum *NEW Network*
    if(substr($ip, 0, 11) == '193.190.146') return true ; // UGMM
    if(substr($ip, 0, 5) == '10.0.') return true ; // WIFI
    if(substr($ip, 0, 6) == '10.11.') return true ; // WIFI
    if ($ip == '127.0.0.1') return true;
    return false ;
  }

  public function testIpAction(Request $request)
  {
    $info = new Response();
    $info->setContent('client ip : '.$request->getClientIp().'<br>client ip without proxy : '.
      implode($request->getClientIp(true)).
      '<br>proxies :'.implode($request->getTrustedProxies())) ;

    return($info);
  }

  private function formatedDpt($locale)
  {
    $departments = array() ;
    $all_dept = $this->getDoctrine()
      ->getRepository('RbinsPersoBundle:Department')
      ->findAll();
    foreach($all_dept as $tab)
    {
      if($tab->getParent()) $parent = $tab->getParent()->getId() ; 
      else $parent = 0 ;
      $departments[$tab->getId()] = array(
        'name' => $tab->getLabel($locale),
        'is_ugmm' => ($tab->getId()==44 OR $parent==44)?true:false, // 44 is UGMM department
      ) ;
    }
    return $departments ;
  }
}
