select p.*, c.name_fr as country,
addr.street as adr_street, addr.street_num as adr_num, addr.box as adr_box,  c1.name as adr_city, c1.code as adr_postcode,
m.name as med_name, m.phone as med_phone, m.fax as med_fax, m.address as med_address, c2.name as med_city, 
(select entry_date from person_entry e where e.person_ref = p.id) as entry, 
(select name_fr 
  FROM working_hist w
  INNER JOIN catalogue cc on w.grade_ref = cc.id
  WHERE w.person_ref = p.id
  ORDER BY start_date desc LIMIT 1
  ) as grade, 
  
(select case when code in ('14', '10', '11', '20') then 'statutaire' 
  WHEN code in ('30','31','32') then 'contractuel'
  ELSE 'other' END 
  FROM working_hist w
  INNER JOIN catalogue cc on w.status_ref = cc.id
  WHERE w.person_ref = p.id
  ORDER BY start_date desc LIMIT 1
  ) as status
  
FROM person p
INNER JOIN catalogue c ON p.nationality_ref = c.id and type='country'
INNER JOIN  (
 select * from  person_address where prefered = true) as addr ON addr.person_ref = p.id
 INNER JOIN city c1 ON c1.id =  addr.city_ref
 
 INNER JOIN medical_center m
        ON p.medical_center_ref = m.id
 INNER JOIN city c2 ON c2.id =  m.city_ref
