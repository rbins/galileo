SELECT h.linguistic_role as lang, sg.name_fr as sal_grade, sp.matricule,sp.last_name, sp.first_name,
cau.name_fr || '/' || cau.name_nl as cause, h.start_date, h.end_date, old.percentage as old, new.percentage as new
 from (select *, percentage(p.id, $P{ContractComment}, $P{start_date}, $P{end_date}) as gest from person p) as sp
INNER JOIN (SELECT * FROM working_hist h WHERE start_date < $P{end_date} AND COALESCE(end_date, '01/01/2900') >= $P{end_date} ORDER by COALESCE(end_date, '01/01/2900') DESC) as h
ON h.person_ref = (sp.gest).pid
INNER join working_duty old ON old.id= (sp.gest).old_wd
INNER join working_duty new ON new.id= (sp.gest).new_wd
INNER join catalogue cau ON cau.id=(sp.gest).cause
INNER JOIN catalogue sg ON h.salary_grade_ref=sg.id
ORDER BY lang ASC, new DESC, h.start_date ASC