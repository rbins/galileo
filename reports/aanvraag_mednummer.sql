/***
* 
***/

select x.* , (x.adr).* , city.name as city_name, city.code as city_code,  (x.entr).*,
(select name_fr from working_hist h INNER JOIN catalogue c on c.id = h.status_ref where person_ref = x.id ORDER BY start_date desc LIMIT 1) as status

FROM (
  select  distinct p.*,
   first_value(a) over (PARTITION BY a.person_ref order by type desc) as adr,
   first_value(e) over (PARTITION BY e.person_ref order by type desc) as entr
  from person p
   inner join person_address a on a.person_ref= p.id
   inner join person_entry e on e.person_ref= p.id
  ) as x
INNER JOIN city ON city.id =  (x.adr).city_ref
where
/*x.id = 2 AND */
 (x.entr).entry_date 

BETWEEN $P{start_month} and ($P{start_month}::date + '1month'::interval)
;