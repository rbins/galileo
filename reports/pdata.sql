select p.id as identification,
last_name as lastname,
CASE WHEN length(regexp_replace(national_nr,'-','','g')) = 11 THEN regexp_replace(national_nr,'-','','g') ELSE '00000000000' END as nrnumber,
ctr.code as nationality_code,
CASE WHEN gender = 'M' THEN 1 ELSE 2 END as sex,
to_char(birth_date,'YYYYMMDD') as birthdate,
c1.code as postal_code,
150 as country_code,
CASE WHEN mother_tongue = 'NL' THEN 'n' WHEN mother_tongue = 'DE' THEN 'd' ELSE 'f' END as language,
stat.code as status_code,
reas.code as cont_reason_code,
CASE WHEN stat.code = '30' THEN '1'
WHEN stat.code = '31' AND dur.code = 'CDD' THEN '7'
WHEN stat.code = '31' AND dur.code = 'CDI' THEN '0'
WHEN stat.code = '31' AND dur.code = 'CDR' THEN '7'
WHEN stat.code = '10' THEN '0'
ELSE '1' END as frame_code,
trim(to_char(grad.code::integer,'000000')) as grade_code,
10  as admin_pos_code,
(select sum(dd.percentage) from working_duty dd where person_ref = p.id and $P{date} between dd.start_date and COALESCE(dd.end_date, $P{date}) )  as activity_perc,
CASE WHEN (select sum(dd.percentage) from working_duty dd where person_ref = p.id and '31/12/2013' between dd.start_date and COALESCE(dd.end_date, '31/12/2013') )=100
AND cause.code in ('145','146','117','115','112','110') THEN '0' ELSE COALESCE(cause.code,'0') END as abs_reason_code,
--COALESCE(cause.code,'0') as abs_reason_code,
'' as disease_remain,
'' as disease_total,
first_name  as firstname,
addr.street as addr_street,
addr.street_num as addr_number,
addr.box as addr_box,
CASE WHEN p.building = 'gulledelle' THEN '2222437868' WHEN p.building = 'eguermin' THEN '2222424804' ELSE '2151933815' END as nrn_etab,
CASE WHEN substr(fam.ehr_code,0,4) = 'SW1' THEN 'W1WO'
WHEN substr(fam.ehr_code,0,4) = 'SW2' THEN 'W2WO'
WHEN substr(fam.ehr_code,0,4) = 'SW3' THEN 'W3WO'
WHEN substr(fam.ehr_code,0,4) = 'SW4' THEN 'W4WO'
ELSE fam.code END as family_code,
salg.code as salary_scale_code,
date_part('year', age(h.salary_seniority))::integer as anci_pec,
CASE WHEN EXISTS (select 1 from premium pre INNER JOIN catalogue cp ON pre.premium_ref = cp.id where name_nl ilike '%competentiepremie%' AND pre.person_ref = p.id AND $P{date} between pre.start_date and pre.end_date) THEN 1 ELSE 0 END as alloc_comp,
COALESCE((select min(code) from premium pre INNER JOIN catalogue cp ON pre.premium_ref = cp.id where code is distinct from '' AND pre.person_ref = p.id AND $P{date} between pre.start_date and pre.end_date), '0') as prime_ling_code,
CASE WHEN p.building = 'gulledelle' THEN 1200 WHEN p.building = 'eguermin' THEN 8400 ELSE 1000 END as workplace_postal_code,
2  as contractcat_code, --TODO ??
date_part('year',age(COALESCE(h.end_date,$P{date}),h.start_date))::integer  as contractduryear,
'-' as  contractdurday,
COALESCE((select 2 from catalogue cp where name_nl ilike '%onbepaalde%' AND cp.id = h.contract_duration_ref),1) as cont_dur_code,
2 as taskperm_code, --TODO ??
0 as selection_code,
(CASE WHEN reas.code = '0' THEN 1 WHEN reas.code = '35' THEN 3 ELSE 4 END) as financesrc_code,
0 as expertechel_code, --??
'' as prepension,
CASE WHEN h.replacement_person IS NOT NULL OR reas.name_fr ilike 'remplacement%' THEN 2 ELSE (CASE WHEN EXISTS (SELECT 1 from working_hist where replacement_person = p.last_name || ' ' || p.first_name AND $P{date} between start_date and COALESCE(end_date,$P{date})) THEN 1 ELSE 0 END) END as status_repl,
CASE WHEN COALESCE(p.handicap,'') != ''  THEN 9 ELSE 0 END as disabled,
COALESCE((select 1 from premium pre INNER JOIN catalogue cp ON pre.premium_ref = cp.id where name_nl ilike '%directie%' AND pre.person_ref = p.id AND $P{date} between pre.start_date and pre.end_date),'0') as manager_grant,
0 as integration_grant, --TODO??
0 as superior_grant,--TODO??
COALESCE((select 1 from premium pre INNER JOIN catalogue cp ON pre.premium_ref = cp.id where name_nl ilike '%project%' AND pre.person_ref = p.id AND $P{date} between pre.start_date and pre.end_date),'0') as project_grant,
(select replace(to_char(entry_date,'YYYY-MM-DD'),'-','/') from person_entry pe where pe.person_ref = p.id ORDER BY entry_date ASC LIMIT 1) as date_start_serve,
fam.code  as function_code,
'' as contract_id,
addr2.street as addr_street2,
addr2.street_num as addr_number2,
addr2.box as addr_box2,
c2.code as postal_code2,
150 as country_code2,
CASE WHEN COALESCE(p.handicap,'') != ''  THEN 1 ELSE 0 END as handicap_code,
0 as refund_agdet_code,
'' as local_service_code,
'' as office,
'' as floor,
'' as email,
'' as phone,
'' as fax,
'' as gsm,
COALESCE((select max(CASE WHEN sncb THEN 1 ELSE 0 END) from transport  tra WHERE $P{date} between tra.start_date and COALESCE(tra.end_date,$P{date}) AND tra.person_ref = p.id ),0) as abo_sncb_nmbs,
COALESCE((select max(CASE WHEN delijn THEN 1 ELSE 0 END) from transport  tra WHERE $P{date} between tra.start_date and COALESCE(tra.end_date,$P{date}) AND tra.person_ref = p.id ),0) as abo_delijn,
COALESCE((select max(CASE WHEN tec THEN 1 ELSE 0 END) from transport  tra WHERE $P{date} between tra.start_date and COALESCE(tra.end_date,$P{date}) AND tra.person_ref = p.id ),0) as abo_tec,
COALESCE((select max(CASE WHEN stibmivb THEN 1 ELSE 0 END) from transport  tra WHERE $P{date} between tra.start_date and COALESCE(tra.end_date,$P{date}) AND tra.person_ref = p.id ),0) as abo_stib_mivb,
CASE WHEN COALESCE(p.homework, 'none') = 'none' THEN 0 ELSE 1 END as homework,
CASE WHEN COALESCE(p.homework, 'none') IN ('monday_AM', 'monday_PM', 'thursday_AM', 'thursday_PM', 'wednesday_AM', 'wednesday_PM', 'tuesday_AM', 'tuesday_PM', 'friday_AM', 'friday_PM') THEN 1 ELSE 0 END as homework_halfday,
'' as fed20_kpi,
CASE WHEN senior_working THEN 1 ELSE 0 END as auth_extend65,
p.handicap_perc

FROM
 ( select distinct in_pers.id as pers_id , first_value(in_hist.id) OVER (PARTITION BY in_pers.id ORDER BY in_hist.end_date DESC NULLS FIRST) as hist_id,
   first_value(in_duty.id) OVER (PARTITION BY in_pers.id ORDER BY in_duty.end_date DESC NULLS FIRST) as duty_id,
   (select in_adr.id from person_address in_adr where in_adr.person_ref = in_pers.id AND type = 'home' limit 1) as adr_id,
   (select in_adr.id from person_address in_adr where in_adr.person_ref = in_pers.id AND type in('home', 'residence') order by type desc limit 1) as adr2_id
   FROM person in_pers INNER JOIN working_hist in_hist ON in_hist.person_ref = in_pers.id INNER JOIN working_duty in_duty ON in_duty.person_ref = in_pers.id
   AND $P{date} between in_duty.start_date and COALESCE(in_duty.end_date, $P{date})
 ) AS ph
INNER JOIN person p ON ph.pers_id = p.id

INNER JOIN catalogue ctr ON ctr.id = p.nationality_ref
LEFT JOIN person_address addr ON addr.id = ph.adr_id
INNER JOIN city c1 ON c1.id =  addr.city_ref
INNER JOIN working_hist h ON ph.hist_id = h.id
INNER JOIN working_duty d ON ph.duty_id = d.id
INNER JOIN catalogue stat ON h.status_ref = stat.id
INNER JOIN catalogue reas ON h.reason_ref = reas.id
INNER JOIN catalogue grad ON h.grade_ref = grad.id
LEFT JOIN catalogue cause ON d.cause_ref = cause.id
INNER JOIN catalogue fam ON h.function_family_ref = fam.id
INNER JOIN catalogue salg ON h.salary_grade_ref = salg.id

LEFT JOIN person_address addr2 ON addr2.id = ph.adr2_id
INNER JOIN city c2 ON c2.id =  addr2.city_ref
LEFT JOIN catalogue cmt ON h.contract_comment_ref = cmt.id
LEFT JOIN catalogue dur ON h.contract_duration_ref = dur.id
LEFT JOIN catalogue frame ON p.frame_ref = frame.id

WHERE

$P{date} between h.start_date and COALESCE(h.end_date, $P{date})
AND EXISTS( select 1 from person_entry pe where pe.person_ref = p.id AND $P{date} between pe.entry_date and COALESCE(pe.exit_date,$P{date}) )
AND (
   cmt.name_fr NOT ilike 'ASBL%' AND cmt.name_fr NOT ilike 'Etudiant' AND cmt.name_fr NOT ilike 'Bénévole'
   )
AND not exists(
  select 1 from working_hist wh inner join catalogue ch on wh.contract_comment_ref = ch.id
  where p.id=wh.person_ref and $P{date}::date between start_date and COALESCE(end_date, $P{date}::date) and name_fr ilike '%asbl%'
)
AND dur.name_fr NOT ilike 'bourse'
order by last_name
--limit 30