select * from (select  /*sp.id, */ (sp.gest).type_changer type_change,
          CASE WHEN stat.name_fr ~ 'tuel subv' THEN 'SPP' WHEN stat.name_fr ~ 'finitif' THEN 'statutaire' ELSE 'contractuel' END as status,
          last_name, first_name,
        stat.name_fr, salg.code, grad.name_fr || '-' || grad.name_nl as grade, d.percentage , linguistic_role,
        w.start_date, w.end_date, w.cost_center as budget, case  WHEN (sp.gest).type_changer = 'stop' THEN reas.name_fr else
        dep.name_fr end as remark

        from ( select *, com_gestion(p.id, '1/10/2012', '11/12/2012') as gest from person p) as sp
        inner join working_hist w on w.id = (sp.gest).wid
        INNER JOIN catalogue stat ON w.status_ref = stat.id
        INNER JOIN catalogue salg ON w.salary_grade_ref = salg.id
        INNER JOIN catalogue grad ON w.grade_ref = grad.id
        INNER JOIN working_duty d ON sp.id = d.person_ref and
         ( CASE WHEN (sp.gest).type_changer = 'stop' THEN w.end_date ELSE w.start_date END ) BETWEEN d.start_date and COALESCE(d.end_date, '01/01/2038')
        INNER JOIN person_entry e ON sp.id = e.person_ref and
         ( CASE WHEN (sp.gest).type_changer = 'stop' THEN w.end_date ELSE w.start_date END ) BETWEEN e.entry_date and COALESCE(e.exit_date, '01/01/2038')
        LEFT JOIN catalogue reas ON e.exit_reason_ref =reas.id
        LEFT JOIN department dep ON d.department_ref = dep.id



  UNION select 'start', 'contractuel', null,null,null, null,null,null, null,null,null,  null,null
  UNION select 'start', 'SPP', null,null,null, null,null,null, null,null,null,  null,null
  UNION select 'start', 'statutaire', null,null,null, null,null,null, null,null,null,  null,null

  UNION select 'prol', 'contractuel', null,null,null, null,null,null, null,null,null,  null,null
  UNION select 'prol', 'SPP', null,null,null, null,null,null, null,null,null,  null,null
  UNION select 'prol', 'statutaire', null,null,null, null,null,null, null,null,null,  null,null

  UNION select 'stop', 'contractuel', null,null,null, null,null,null, null,null,null,  null,null
  UNION select 'stop', 'SPP', null,null,null, null,null,null, null,null,null,  null,null
  UNION select 'stop', 'statutaire', null,null,null, null,null,null, null,null,null,  null,null
  ) as x
          ORDER BY
   CASE WHEN type_change = 'start' THEN 0 WHEN type_change ='stop' THEN 2 ELSE 1 END,
   CASE WHEN status ='SPP' THEN 1 WHEN status ='statutaire' THEN 2 ELSE 0 END,
  case WHEN type_change = 'stop' then end_date else start_date end