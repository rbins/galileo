--begin;
SET client_encoding = 'UTF8';

CREATE TABLE catalogue (
    id serial,
    type text NOT NULL,
    code text,
    ehr_code text,
    name_fr text,
    name_nl text,
    level text,
    is_active boolean not null default true,
    constraint pk_catalogue primary key (id)
);

CREATE TABLE city (
    id serial,
    country_ref integer NOT NULL,
    code text NOT NULL,
    name text NOT NULL,
    constraint pk_city primary key (id),
    constraint fk_city_country foreign key (country_ref) references catalogue(id) on delete cascade
);

CREATE TABLE department (
    id serial,
    level text NOT NULL,
    code varchar,
    name_fr text NOT NULL,
    name_en text NOT NULL,
    name_nl text NOT NULL,
    parent_ref integer,
    path text NOT NULL default '/',
    is_active boolean not null default true,
    constraint pk_department primary key (id),
    constraint unq_department_code unique (code),
    constraint fk_parent_ref_department foreign key (parent_ref) references department(id) on delete cascade
);

CREATE TABLE medical_center (
    id serial,
    city_ref integer NOT NULL,
    name text NOT NULL,
    phone text NOT NULL,
    fax text NOT NULL,
    address text NOT NULL,
    comment text,
    constraint pk_medical_center primary key (id),
    constraint fk_medical_center_city foreign key (city_ref) references city(id) on delete set null
);

CREATE TABLE person (
    id serial,
    nationality_ref integer,
    first_name text NOT NULL,
    last_name text NOT NULL,
    alias_first_name text,
    alias_last_name text default '',
    mother_tongue text,
    dependance boolean NOT NULL,
    birth_date date NOT NULL,
    birth_place text,
    gender text,
    marital_status_ref integer NOT NULL default 1,
    identitycard_nr text default '',
    national_nr text,
    matricule text default '',
    dimona_nr text default '',
    passport_nr text default '',
    payroll_tax boolean NOT NULL default false,
    account_iban_nr text default '',
    account_bic_nr text default '',
    other_account_holder text,
    highest_degree_ref integer,
    household_allowance boolean NOT NULL default false,
    residence_allowance boolean NOT NULL default false,
    handicap text,
    sis_nr text,
    med_number text,
    medical_center_ref integer,
    functional_chief_ref integer,
    senior_working boolean NOT NULL default false,
    senior_working_date date,
    homework text default 'none',
    comment text,
    old_id integer,
    building text,
    building_floor text,
    frame_ref integer,
    description text,
    photo text,
    uid text,
    constraint unq_person_uid UNIQUE (uid),
    constraint pk_person primary key (id),
    constraint fk_person_nationality foreign key (nationality_ref) references catalogue(id) on delete set null,
    constraint fk_person_functional_chief_ref foreign key (functional_chief_ref) references person(id) on delete set null,
    constraint fk_person_marital_status_ref foreign key (marital_status_ref) references catalogue(id) on delete set null,
    constraint fk_person_medical_center_ref foreign key (medical_center_ref) references medical_center(id) on delete set null,
    constraint fk_person_highest_degree_ref foreign key (highest_degree_ref) references catalogue(id) on delete set null

);

CREATE TABLE family (
    id serial,
    type text not null,
    person_ref integer not null,
    full_name text NOT NULL,
    gender text,
    birth_date date,
    allowance boolean NOT NULL default false,
    allowance_end date,
    parential_leave boolean NOT NULL default false,
    handicap text,
    employed boolean NOT NULL DEFAULT false,
    comment text,
    constraint pk_family primary key (id),
    constraint fk_family_person foreign key (person_ref) references person(id) on delete cascade
);

CREATE TABLE person_address (
    id serial,
    person_ref integer NOT NULL,
    city_ref integer NOT NULL,
    street text,
    street_num text,
    box text,
    type text NOT NULL default 'home',
    comment text,
    constraint pk_person_address primary key (id),
    constraint fk_person_address_person foreign key (person_ref) references person(id) on delete cascade
);

CREATE TABLE person_contact (
    id serial,
    person_ref integer not null,
    type text NOT NULL,
    entry text NOT NULL,
    prefered boolean NOT NULL default false,
    comment text,
    constraint pk_person_contact primary key (id),
    constraint fk_person_contact_person foreign key (person_ref) references person(id) on delete cascade
);


CREATE TABLE premium (
    id serial,
    person_ref integer not null,
    premium_ref integer not null,
    start_date date NOT NULL,
    end_date date NOT NULL,
    comment text,
    constraint pk_premium primary key (id),
    constraint fk_premium_person foreign key (person_ref) references person(id) on delete cascade,
    constraint fk_premium_premium foreign key (premium_ref) references catalogue(id) on delete set null
);


CREATE TABLE transport (
    id serial,
    person_ref integer not null,
    start_date date,
    end_date date,
    bike boolean NOT NULL default false,
    car boolean NOT NULL default false,
    stibmivb boolean NOT NULL default false,
    tec boolean NOT NULL default false,
    delijn boolean NOT NULL default false,
    sncb boolean NOT NULL default false,
    km numeric(6,2),
    numberplate text,
    parking_card_date date,
    is_effective boolean NOT NULL default true,
    city_from integer,
    city_to integer,
    giveback boolean not null default false,
    comment text,
    constraint pk_transport primary key (id),
    constraint fk_transport_person foreign key (person_ref) references person(id) on delete cascade,
    constraint fk_transport_city_from foreign key (city_from) references city(id) on delete set null,
    constraint fk_transport_city_to foreign key (city_to) references city(id) on delete set null
);


CREATE TABLE working_hist (
    id serial,
    person_ref integer not null,
    replacement_person text,
    grade_ref integer,
    reason_ref integer,
    status_ref integer,
    category text not null default 'unknown',
    salary_grade_ref integer,
    function_family_ref integer,
    --comment_ref integer,
    level character varying(2) NOT NULL,
    sublevel_ref integer NOT NULL,
    contract_duration_ref integer NOT NULL,
    linguistic_role character varying(1) NOT NULL,
    seniority date,
    salary_seniority date,
    contract_comment_ref integer,
    start_date date,
    end_date date,
    comment text,
    cd_function text,
    cost_center text,
    lang_level integer,
    class integer,
    class_seniority date,
    scientific_seniority date,
    constraint pk_working_hist primary key (id),
    constraint fk_working_hist_person foreign key (person_ref) references person(id),
    constraint fk_working_hist_grade foreign key (grade_ref) references catalogue(id),
    constraint fk_working_hist_reason foreign key (reason_ref) references catalogue(id),
    constraint fk_working_hist_status foreign key (status_ref) references catalogue(id),
    constraint fk_working_hist_sublevel foreign key (sublevel_ref) references catalogue(id),
    --constraint fk_working_hist_comment foreign key (comment_ref) references catalogue(id) on delete set null,
    constraint fk_working_hist_salary_grade foreign key (salary_grade_ref) references catalogue(id),
    constraint fk_working_hist_function_family foreign key (function_family_ref) references catalogue(id),
    constraint fk_working_hist_contract_duration_ref foreign key (contract_duration_ref) references catalogue(id),
    constraint fk_working_hist_contract_comment_ref foreign key (contract_comment_ref) references catalogue(id)
);

CREATE TABLE working_duty (
    id serial,
    person_ref integer not null,
    department_ref integer,
    cause_ref integer,
    percentage integer NOT NULL default 100,
    start_date date,
    end_date date,
    comment text,
    constraint pk_working_duty primary key (id),
    constraint fk_working_duty_person foreign key (person_ref) references person(id),
    constraint fk_working_duty_departement foreign key (department_ref) references department(id),
    constraint fk_working_duty_cause foreign key (cause_ref) references catalogue(id)
);

CREATE TABLE duty_calendar (
    id serial,
    working_duty_ref integer,
    week integer NOT NULL,
    mon_am boolean NOT NULL default true,
    mon_pm boolean NOT NULL default true,
    tue_am boolean NOT NULL default true,
    tue_pm boolean NOT NULL default true,
    wed_am boolean NOT NULL default true,
    wed_pm boolean NOT NULL default true,
    thu_am boolean NOT NULL default true,
    thu_pm boolean NOT NULL default true,
    fri_am boolean NOT NULL default true,
    fri_pm boolean NOT NULL default true,
    sat_am boolean NOT NULL default false,
    sat_pm boolean NOT NULL default false,
    sun_am boolean NOT NULL default false,
    sun_pm boolean NOT NULL default false,

    constraint pk_duty_calendar primary key (id),
    constraint fk_duty_calendar_working_duty foreign key (working_duty_ref) references working_duty(id) on delete cascade
);

CREATE TABLE person_entry (
    id serial,
    person_ref integer not null,
    entry_date date NOT NULL default now(),
    exit_date date,
    exit_reason_ref integer,
    constraint pk_person_entry primary key (id),
    constraint fk_person_entry_person foreign key (person_ref) references person(id) on delete cascade,
    constraint fk_person_entry_exit_reason_ref foreign key (exit_reason_ref) references catalogue(id) on delete set null
);

CREATE TABLE combined_job (
    id serial,
    person_ref integer not null,
    start_date date NOT NULL default now(),
    end_date date,
    comment text,
    constraint pk_combined_job primary key (id),
    constraint fk_combined_job_person foreign key (person_ref) references person(id) on delete cascade
);

CREATE TABLE document (
    id serial,
    person_ref integer not null,
    type_ref integer not null,
    title text,
    path text not null,
    name text,
    constraint pk_document primary key (id),
    constraint fk_document_person foreign key (person_ref) references person(id) on delete cascade,
    constraint fk_document_doctype foreign key (type_ref) references catalogue(id) on delete set null
);

create table users_tracking (
    id serial,
    referenced_relation varchar not null,
    record_id integer not null,
    user_ref text,
    action varchar not null default 'insert',
    old_value hstore,
    new_value hstore,
    modification_date_time timestamp default now() not null,
    constraint pk_users_tracking_pk primary key (id)
);

comment on table users_tracking is 'Tracking of users actions on tables';
comment on column users_tracking.referenced_relation is 'Reference-Name of table concerned';
comment on column users_tracking.record_id is 'ID of record concerned';
comment on column users_tracking.id is 'Unique identifier of a table track entry';
comment on column users_tracking.user_ref is 'Reference of user having made an action - id field of users table';
comment on column users_tracking.action is 'Action done on table record: insert, update, delete';
comment on column users_tracking.modification_date_time is 'Track date and time';

CREATE TABLE phonebook
(
  id serial NOT NULL,
  person_ref integer NOT NULL,
  department_ref integer,
  type text NOT NULL,
  entry text NOT NULL,
  CONSTRAINT pk_phonebook PRIMARY KEY (id),
  CONSTRAINT fk_person_ref FOREIGN KEY (person_ref)
      REFERENCES person (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE CASCADE,
   CONSTRAINT fk_department_ref FOREIGN KEY (department_ref)
      REFERENCES catalogue (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE SET NULL     
);

CREATE INDEX idx_family_person_ref ON family USING btree (person_ref);
CREATE INDEX idx_medical_center_city_ref ON medical_center USING btree (city_ref);
CREATE INDEX idx_city_country_ref ON city USING btree (country_ref);
CREATE INDEX idx_person_address_city_ref ON person_address USING btree (city_ref);
CREATE INDEX idx_person_address_person_ref ON person_address USING btree (person_ref);
CREATE INDEX idx_person_nationality_ref ON person USING btree (nationality_ref);
CREATE INDEX idx_transport_person_ref ON transport USING btree (person_ref);
CREATE INDEX idx_person_contact_person_ref ON person_contact USING btree (person_ref);
CREATE INDEX idx_premium_premium_ref ON premium USING btree (premium_ref);
CREATE INDEX idx_premium_person_ref ON premium USING btree (person_ref);

CREATE INDEX idx_working_hist_reason_ref ON working_hist USING btree (reason_ref);
CREATE INDEX idx_working_duty_department_ref ON working_duty USING btree (department_ref);
CREATE INDEX idx_working_hist_person_ref ON working_hist USING btree (person_ref);
CREATE INDEX idx_working_hist_grade_ref ON working_hist USING btree (grade_ref);
CREATE INDEX idx_working_hist_function_family_ref ON working_hist USING btree (function_family_ref);
CREATE INDEX idx_working_hist_salary_grade_ref ON working_hist USING btree (salary_grade_ref);
CREATE INDEX idx_duty_calendar_working_duty_ref ON duty_calendar USING btree (working_duty_ref);
CREATE INDEX idx_department_parent_ref ON department USING btree (parent_ref);

CREATE INDEX idx_catalogue_type ON catalogue USING btree (type);

--DUMMY fct
CREATE OR REPLACE FUNCTION get_setting(IN param text, OUT value text) as $$ select '1'::text $$ LANGUAGE sql STABLE STRICT;
CREATE OR REPLACE FUNCTION generate_person_uid()
  RETURNS TRIGGER AS
  $BODY$
BEGIN
  if EXISTS(select 1 from person where uid = left(NEW.first_name,1) || NEW.last_name) THEN
    if EXISTS(select 1 from person where uid = NEW.first_name || NEW.last_name) THEN
      NEW.uid := NEW.first_name || NEW.last_name || '1' ;
    ELSE
      NEW.uid := NEW.first_name || NEW.last_name ;
    END IF;
  ELSE
  NEW.uid := left(NEW.first_name,1) || NEW.last_name ;
  END IF;
  RETURN NEW;
END;
$BODY$
LANGUAGE plpgsql;

CREATE TRIGGER trg_set_person_uid BEFORE INSERT on person  FOR EACH ROW EXECUTE PROCEDURE generate_person_uid() ;

-- DO NOT REMOVE THIS COMMENT !!
--END AUTOINSTALL
-- This comment tell the quick console installer to stop here
-- DO NOT REMOVE THIS COMMENT !!
