﻿DROP TABLE IF EXISTS zzz_13092011_CV_BA_Template_Contactgegevens;

select row_number() over () as "CNT0", * 
into zzz_13092011_CV_BA_Template_Contactgegevens
from 
(
select 
	case when (translate(national_nr, '- /.', ''))::bigint = 0 or length(translate(national_nr, '- /.', '')) != 11 then null else translate(national_nr, '- /.', '') end::text as "CNT1",
	null::text as "CNT2", -- !!! To be filled by HR to generate a fake Number if I'm correct
	(select regexp_replace(entry, '[a-z]+|[A-Z]+|\(|\)|é|è', '', 'g') from person_contact where type = 'phone' and person_ref = p.id and substring(entry,1,3) not in ('047', '048', '049') and length(entry) < 25 and (comment is null or substr(lower(comment),1,4) not in ('werk', 'work', 'trav', 'prof') ) limit 1) as "CNT3",
	(select regexp_replace(entry, '[a-z]+|[A-Z]+|\(|\)|é|è', '', 'g') from person_contact where type = 'phone' and person_ref = p.id and substring(entry,1,3) in ('047', '048', '049') and length(entry) < 25 and (comment is null or substr(lower(comment),1,4) not in ('werk', 'work', 'trav', 'prof') ) limit 1) as "CNT4",
	(select regexp_replace(entry, '[a-z]+|[A-Z]+|\(|\)|é|è', '', 'g') from person_contact where type = 'fax' and person_ref = p.id and substring(entry,1,3) not in ('047', '048', '049') and length(entry) < 25 and (comment is null or substr(lower(comment),1,4) not in ('werk', 'work', 'trav', 'prof') ) limit 1) as "CNT5",
	(select regexp_replace(entry, '[a-z]+|[A-Z]+|\(|\)|é|è', '', 'g') from person_contact where type = 'phone' and person_ref = p.id and substring(entry,1,3) not in ('047', '048', '049') and length(entry) < 25 and comment is not null and substr(lower(comment),1,4) in ('werk', 'work', 'trav', 'prof') limit 1) as "CNT6",
	(select regexp_replace(entry, '[a-z]+|[A-Z]+|\(|\)|é|è', '', 'g') from person_contact where type = 'phone' and person_ref = p.id and substring(entry,1,3) in ('047', '048', '049') and length(entry) < 25 and comment is not null and substr(lower(comment),1,4) in ('werk', 'work', 'trav', 'prof') limit 1) as "CNT7",
	(select regexp_replace(entry, '[a-z]+|[A-Z]+|\(|\)|é|è', '', 'g') from person_contact where type = 'fax' and person_ref = p.id and substring(entry,1,3) not in ('047', '048', '049') and length(entry) < 25 and comment is not null and substr(lower(comment),1,4) in ('werk', 'work', 'trav', 'prof') limit 1) as "CNT8",
	(select entry from person_contact where type = 'mail' and person_ref = p.id and length(entry) < 71 and strpos(entry, '@') != 0 and (substr(lower(comment),1,4) in ('werk', 'work', 'trav', 'prof') or strpos(entry, 'naturalsciences.be') != 0 or strpos(entry, 'sciencesnaturelles.be') != 0 or strpos(entry, 'natuurwetenschappen.be') != 0  ) limit 1) as "CNT9",
	(select entry from person_contact where type = 'mail' and person_ref = p.id and length(entry) < 71 and strpos(entry, '@') != 0 and substr(lower(comment),1,4) not in ('werk', 'work', 'trav', 'prof') and strpos(entry, 'naturalsciences.be') = 0 and strpos(entry, 'sciencesnaturelles.be') = 0 and strpos(entry, 'natuurwetenschappen.be') = 0  limit 1) as "CNT10",
	'EOF'::text as "CNT11"
from person p
order by last_name, first_name
) as subquery;

select * from zzz_13092011_CV_BA_Template_Contactgegevens order by "CNT0";
