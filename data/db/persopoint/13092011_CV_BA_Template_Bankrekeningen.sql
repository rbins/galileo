﻿DROP TABLE IF EXISTS zzz_13092011_CV_BA_Template_Bankrekeningen;

select row_number() over () as "BAN0", * 
into zzz_13092011_CV_BA_Template_Bankrekeningen
from 
(
select 
	case when (translate(national_nr, '- /.', ''))::bigint = 0 or length(translate(national_nr, '- /.', '')) != 11 then null else translate(national_nr, '- /.', '') end::text as "BAN1",
	null::text as "BAN2", -- !!! To be filled by HR to generate a fake Number if I'm correct
	'P':: text as "INV3", -- !!! To check that the value is correct
	upper(substring(account_iban_nr,1,2)) as "INV4",
	null::text as "INV5",
	account_iban_nr as "INV6",
	trim(first_name || ' ' || last_name) as "INV7", -- !!! No mention is given about the account holder, so concatenation of person first and last name is done
	null::text as "INV8", 
	account_bic_nr as "INV9", -- !!! BIC to be filled for the ones missing
	'EOF'::text as "INV10"
from person p 
where account_iban_nr is not null and account_iban_nr != '' and upper(substring(account_iban_nr,1,2)) in (select distinct ehr_code from catalogue where type = 'country')
order by p.last_name, p.first_name
) as subquery;

select * from zzz_13092011_CV_BA_Template_Bankrekeningen order by "BAN0";
