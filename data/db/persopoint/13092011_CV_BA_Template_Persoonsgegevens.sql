﻿DROP TABLE IF EXISTS zzz_13092011_CV_BA_Template_Persoonsgegevens;

select row_number() over () as "PER0", * 
into zzz_13092011_CV_BA_Template_Persoonsgegevens
from 
(
select 
	case when (translate(national_nr, '- /.', ''))::bigint = 0 or length(translate(national_nr, '- /.', '')) != 11 then null else translate(national_nr, '- /.', '') end::text as "PER1",
	null::text as "PER2", -- !!! To be filled by HR to generate a fake Number if I'm correct
	case when matricule is null or matricule = '' or matricule = '0' then null else '19'||matricule end::text as "PER3", 
	last_name as "PER4",
	first_name as "PER5",
	alias_first_name as "PER6",
	case
		when mother_tongue = 'EN' then
			case
				when gender = 'F' then 'Mrs.'
				else 'Mr.'
			end
		when mother_tongue IN ('D', 'DE') then
			case
				when gender = 'F' then 'Frau'
				else 'Herr'
			end
		when mother_tongue = 'NL' then
			case
				when gender = 'F' then 'Mevr.'
				else 'Dhr.'
			end
		else
			case
				when gender = 'F' then 'Mme.'
				else 'Mr.'
			end
	end as "PER7",
	null::text as "PER8", -- !!! To be filled manually - info is present or can be deduced from "Geboorteplaats" - could have been filled by nationality but would lead to mistakes
	null::text as "PER9", -- !!! To be filled manually - info is present or can be deduced from "Geboorteplaats"
	case when birth_place IN ('Indéterminé/Onbepaald', 'Inconnu') then null else birth_place end as "PER10",
	to_char(birth_date, 'DDMMYYYY') as "PER11",
	gender as "PER12",
	(select ehr_code from catalogue cat where type = 'degree' and cat.id = p.highest_degree_ref) as "PER13", -- !!! ehr_code missing for this notion - to be filled in !!!
	mother_tongue as "PER14",
	null::text as "PER15",
	null::text as "PER17",
	(select cat.ehr_code from catalogue cat where cat.type = 'country' and cat.id = p.nationality_ref) as "PER18",
	passport_nr as "PER19",
	case when (translate(identitycard_nr, '- /.', '')) = '0' or length(translate(identitycard_nr, '- /.', '')) != 11 then null else translate(identitycard_nr, '- /.', '') end::text as "PER20", -- !!! A lot of entries have been skipped due to the fact that lenght was 12 chars !!!
	case when length(med_number) > 15 then null else med_number end::text as "PER21",
	(select trim(substr(name,1,2)) from medical_center where id = p.medical_center_ref) as "PER22",
	null::text as "PER23", -- ! To be possibly filled manually
	null::text as "PER24",
	null::text as "PER25",
	null::text as "PER26",
	null::text as "PER27",
	null::text as "PER28",
	'EOL'::text as "PER29"
from person p
order by last_name, first_name
) as subquery;

select * from zzz_13092011_CV_BA_Template_Persoonsgegevens order by "PER0";
