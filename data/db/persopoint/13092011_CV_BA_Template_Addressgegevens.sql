﻿DROP TABLE IF EXISTS zzz_13092011_CV_BA_Template_Addressgegevens;

select row_number() over () as "ADR0", * 
into zzz_13092011_CV_BA_Template_Addressgegevens
from 
(
select 
	case when (translate(national_nr, '- /.', ''))::bigint = 0 or length(translate(national_nr, '- /.', '')) != 11 then null else translate(national_nr, '- /.', '') end::text as "ADR1",
	null::text as "ADR2", -- !!! To be filled by HR to generate a fake Number if I'm correct
	upper(pa.type) as "ADR3",
	(select catalogue.ehr_code from catalogue inner join city on city.country_ref = catalogue.id and catalogue.type = 'country' where city.id = pa.city_ref) as "ADR4",
	(select city.name from city where city.id = pa.city_ref) as "ADR5",
	(select city.code from city where city.id = pa.city_ref) as "ADR6",
	pa.street as "ADR7",
	pa.street_num as "ADR8",
	pa.box as "ADR9",
	null::text as "ADR10",
	'EOF'::text as "ADR11"
from person p inner join person_address pa on p.id = pa.person_ref
order by p.last_name, p.first_name
) as subquery;

select * from zzz_13092011_CV_BA_Template_Addressgegevens order by "ADR0";
