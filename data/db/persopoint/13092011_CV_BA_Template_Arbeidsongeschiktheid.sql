﻿DROP TABLE IF EXISTS zzz_13092011_CV_BA_Template_Arbeidsongeschiktheid;

select row_number() over () as "INV0", * 
into zzz_13092011_CV_BA_Template_Arbeidsongeschiktheid
from 
(
select 
	case when (translate(national_nr, '- /.', ''))::bigint = 0 or length(translate(national_nr, '- /.', '')) != 11 then null else translate(national_nr, '- /.', '') end::text as "INV1",
	null::text as "INV2", -- !!! To be filled by HR to generate a fake Number if I'm correct
	'Y':: text as "INV3",
	null::text as "INV4", -- To be filled when understood
	null::text as "INV5", -- To be filled when understood
	null::text as "INV6", -- To be filled when understood
	null::text as "INV7", -- To be filled when understood
	null::text as "INV8", -- To be filled when understood
	'EOF'::text as "INV9"
from person p 
where handicap is not null and handicap != ''
order by p.last_name, p.first_name
) as subquery;

select * from zzz_13092011_CV_BA_Template_Arbeidsongeschiktheid order by "INV0";
