CREATE TYPE my_type as (pid int, wid int, type_changer text );

/*** FUNCTION FOR REPORTING **/
CREATE OR REPLACE FUNCTION com_gestion(pers_id integer, startd date, endd date) RETURNS setof my_type AS $$
        DECLARE 
          ret my_type;
        
          rec working_hist%ROWTYPE;
        begin 
          ret.pid := pers_id;
          FOR rec in select * from working_hist h where h.person_ref = pers_id::integer AND (start_date BETWEEN startd and endd OR end_date BETWEEN startd and endd)
           order by start_date asc, end_date desc loop
                ret.wid := rec.id;
                IF(rec.start_date BETWEEN startd and endd) THEN 
                        IF(EXISTS (select 1 from working_hist h where h.person_ref = pers_id AND h.status_ref = rec.status_ref
                                AND h.end_date +'2days'::interval >= rec.start_date and h.end_date <= rec.start_date) ) THEN
                                
                                ret.type_changer:='prol';
                                return next ret ;
                        ELSIF(rec.start_date BETWEEN startd and endd) THEN
                                ret.type_changer:='start';
                                return next ret ;
                        ELSE
                               --RAISE EXCEPTION 'we got a problem 1';
                        END IF;
                ELSIF( rec.end_date BETWEEN startd and endd) THEN
                        IF(EXISTS (select 1 from working_hist h where h.person_ref = pers_id AND h.status_ref = rec.status_ref
                                AND h.start_date -'2days'::interval <= rec.end_date and h.start_date >= rec.end_date) ) THEN
                                --RAISE INFO 'SKIP prol';
                        ELSE                    
                                ret.type_changer:='stop';
                                return next ret ;
                        END IF;
                ELSE
                        --RAISE INFO 'SKIP too old : % => %', rec.start_date, rec.end_date;
                END IF;
          end loop;
        
  END;$$ LANGUAGE plpgsql;

CREATE TYPE my_record as (pid int, old_wd int, new_wd int, cause int);
CREATE OR REPLACE FUNCTION percentage(pers_id integer, contract integer,startd date, endd date) RETURNS setof my_record AS $$
        DECLARE
          ret my_record;
          rec working_duty%ROWTYPE;
          prev_recod working_duty%ROWTYPE;
        begin 
          ret.pid := pers_id::int;
          FOR rec in select d.* from working_duty d 
          INNER JOIN (SELECT * FROM working_hist h WHERE h.start_date <= endd::date AND COALESCE(h.end_date, '01/01/2900') >= endd::date ORDER by COALESCE(h.end_date, '01/01/2900') DESC) as wh ON wh.person_ref=d.person_ref 
          where d.person_ref = pers_id::int AND (d.start_date BETWEEN startd::date and endd::date+'1days'::interval OR d.end_date BETWEEN startd::date and endd::date) AND comment_ref= contract
           order by start_date asc, end_date desc loop
                ret.cause  := rec.cause_ref;
                ret.new_wd := rec.id ;
                IF(prev_recod is not null AND prev_recod.percentage != rec.percentage) THEN
                  ret.old_wd = prev_recod.id ;
                  return next ret ;
                END IF;
                
                prev_recod := rec;
          end loop;
        
  END;$$ LANGUAGE plpgsql;
  
  
--RAPPORT CHEF FONCTIONNELS  
create OR REPLACE view functionnal_chiefs_tree as (
 WITH RECURSIVE c AS (
                 SELECT person.id, person.first_name,  person.last_name AS last_name, person.last_name || person.first_name AS path
                   FROM person
                  WHERE person.id = 1473 -- pisani
        UNION ALL 
                 SELECT person.id, person.first_name , person.last_name, c.path || '/'::text || person.last_name  || person.first_name AS path
                   FROM person
              JOIN c ON person.functional_chief_ref = c.id
        )
 SELECT c.id, c.last_name, c.first_name,  c.path, string_to_array(c.path, '/'::text) AS string_to_array, array_upper(string_to_array(c.path, '/'::text), 1) - 1 AS lvl
   FROM c
  ORDER BY c.path;
);


