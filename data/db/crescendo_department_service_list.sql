﻿select code, name_nl as "Naam van de dienst in het Nederlands", name_fr as "Nom du service en Français"
from department
where id != 198
  and code is not null
order by level;