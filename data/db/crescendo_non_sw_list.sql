﻿select distinct
  'INS-' || p.id as "Numéro d'identification du collaborateur",
  p.first_name as "Prénom",
  p.last_name as "Nom",
  p.email as "Adresse mail",
  to_char(p.birth_date, 'DD/MM/YYYY') as "Date de naissance",
  p.mother_tongue as "Langue de l'utilisateur",
  case 
    when p.id IN (2826, 303, 2173, 1041, 731, 2496) then 3 
    else 
      case 
        when exists(select 1 from person subp where functional_chief_ref = p.id) then 2 
        else 1 
      end 
  end as "Niveau de l'utilisateur dans l'application Crescendo",
  case when wh.function_family_ref is null then '' when wh.function_family_ref = 2093 OR wh.level = 'SW' OR p.id IN (113, 85, 545, 52, 570, 1473, 2776) then 'N' else 'Y' end as "Fonction",
  case when wh.function_family_ref = 2093 OR wh.function_family_ref IS NULL OR wh.level = 'SW' OR p.id IN (113, 85, 545, 52, 570, 1473, 2776) then '' else (select name_fr||' - '||name_nl from catalogue where id = wh.function_family_ref) end as "Code de la fonction",
  case 
    when wh.level = 'SW' OR p.id IN (113, 85, 545, 52, 570, 1473, 2776) then ''
    when wh.level != 'A' then wh.level
    else 
      case 
        when (select left(code,1) from catalogue where id = wh.salary_grade_ref) = 'S' then (select substr(code,1,3) from catalogue where id = wh.salary_grade_ref)
        when (select left(code,1) from catalogue where id = wh.salary_grade_ref) = '0' then 
          case
            when (select code from catalogue where id = wh.salary_grade_ref) = '0224' then 'A4'
            else
              (select substr(code,2,2) from catalogue where id = wh.salary_grade_ref)
          end 
        when (select left(code,1) from catalogue where id = wh.salary_grade_ref) = 'N' then  
	  (select substr(code,2,2) from catalogue where id = wh.salary_grade_ref)
	  
        else (select code from catalogue where id = wh.salary_grade_ref)
      end  
  end as "Niveau",
  case 
    when exists(select 1 from person subp where functional_chief_ref = p.id) then 'Leader'
    else 'Expert'
  end as "Rôles",
  '' as "Niveau situation exceptionnelle",
  '' as "Rôle situation exceptionnelle",
  '' as "Filière de métiers",
  '' as "Famille de fonction",
  case 
    when p.functional_chief_ref is not null then
      (select 'INS-' || sp.id from person sp where sp.id = p.functional_chief_ref) 
    else ''
  end as "Responsable",
  '' as "Profil de compétences génériques",
  '' as "Profil de compétences techniques",
  (select array_to_string(array_agg(subqry.sections), ';') 
   from (select distinct d.code as "sections"
         from working_duty wd inner join department d on wd.department_ref = d.id 
         where wd.person_ref = p.id 
           and (wd.end_date is null or wd.end_date >= now())
        ) as subqry
  ) as "Section",
  case 
    when p.responsible_chief_ref is not null then
      (select 'INS-'|| sp.id from person sp where sp.id = p.responsible_chief_ref) 
    else ''
  end as "Chef hiérarchique",
  replace(p.national_nr, '-', '') as "Numéro de registre national",
  case
    when exists(select 1 from premium where person_ref = p.id and premium_ref = 1734) then 'Y'
    else 'N'
  end as "Utilisateur peut agir en tant que bilingue légal",
  '' as "Données de fonctions sont une fonction supérieur officiel",
  CASE
    WHEN wh.level = 'SW' OR p.id IN (113, 85, 545, 52, 570, 1473, 2776) then 'N'
    ELSE 'Y'
  END as "Démarrer cycle d'évaluation",
  CASE
    WHEN wh.level = 'SW' OR p.id IN (113, 85, 545, 52, 570, 1473, 2776) then ''
    ELSE '01/01/2017'
  END  as "Date de référence du premier cycle d'évaluation",
  '' as "Cette fonction réfère à un stage",
  'EOL' as "End of Line",
  case 
    when p.mother_tongue not in ('FR', 'NL') then True 
    else False 
  end as "Langue <> FR et NL",
  CASE WHEN wh."level" = 'SW' THEN 1 ELSE 2 END as "SW order by",
  case when exists (select 1 from person ssp where p.id = ssp.functional_chief_ref or p.id = ssp.responsible_chief_ref ) then 0 else 1 end as chief_order_by
from person p
inner join working_hist wh 
   on p.id = wh.person_ref 
  and (wh.end_date is null or wh.end_date > now())
  and (contract_comment_ref in (1095, 1096, 1097, 1102, 1105, 1106) or contract_comment_ref is null)
  and (
    UPPER(wh."level") != 'SW'
    or
    (UPPER(wh."level") = 'SW' and exists (select 1 from person ssp inner join working_hist swh ON ssp.id = swh.person_ref and (swh.end_date is null or swh.end_date > now()) where (p.id = ssp.functional_chief_ref or p.id = ssp.responsible_chief_ref) and swh."level" != 'SW'))
  )  
order by "SW order by", chief_order_by, p.last_name;