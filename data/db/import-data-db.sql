begin;

CREATE OR REPLACE FUNCTION public.search_entry(id integer) RETURNS SETOF person_entry
AS $$
DECLARE
  tmp person_entry%ROWTYPE;
  rec RECORD;
  row RECORD;
  cnt integer := 2;
  cnt2 integer := 3;
BEGIN
    tmp.person_ref := id;
    tmp.exit_date := '01/01/1900';
    /****/
    WHILE(cnt != 0) LOOP
      cnt := 0;
      --SELECT THE START DATE
    FOR rec in SELECT * from working_hist h3 where h3.person_ref = tmp.person_ref AND start_date > COALESCE(tmp.exit_date,'01/01/2900') order by start_date asc, end_date desc LIMIT 1 
    LOOP
        cnt := 1;
        tmp.entry_date = rec.start_date;
        tmp.exit_date := rec.end_date;
        --RAISE INFO 'ENTRY % , EXIT %', rec.start_date, rec.end_date; --
        cnt2 := 1;
        <<end_blk>>
        WHILE( cnt2 != 0) LOOP
                cnt2 := 0;
                --SELECT THE END DATE
                FOR row in select start_date, end_date from working_hist h3 where h3.person_ref = tmp.person_ref and start_date <= (tmp.exit_date + '3day'::interval)   -- prev end date 
                        order by COALESCE(end_date,'01/01/2900') desc limit 1 
                LOOP
                        cnt2 := 1;
                        if tmp.exit_date  IS not DISTINCT FROM row.end_date THEN
                                --RAISE INFO 'EXIIII % = % ', COALESCE(tmp.exit_date::text,'null'),COALESCE(row.end_date::text,'null');
                                EXIT end_blk;
                        END IF;
                        --RAISE INFO 'FIND NEXT YEY S:% E:% ', row.start_date,row.end_date;
                        tmp.exit_date:= row.end_date;

                END LOOP;
        END LOOP;
        return next tmp;
     END LOOP;
     
   END LOOP;

END;
$$ LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION public.add_entries() RETURNS boolean
AS $$
DECLARE
  p RECORD;
BEGIN
    FOR p IN SELECT id from person 
    LOOP
  INSERT INTO person_entry (person_ref, entry_date, exit_date)
  (
    select p.id, entry_date, exit_date  from search_entry(p.id)
  );
  END LOOP;
  RETURN TRUE;
  END;
$$ LANGUAGE plpgsql;


--Insert catalogue
INSERT INTO public.catalogue(type, ehr_code, name_fr, name_nl)
VALUES ('marital_status', '1', 'Célibataire', 'Ongehuwd'),
('marital_status', '2','Marié(e)' , 'Gehuwd'),
('marital_status', '3','Divorcé(e)' , 'Gescheiden'),
('marital_status', '4','Veuf/Veuve' , 'Weduwe(naar)'),
('marital_status', '5','Cohabitant(e)' , 'Samenwonend'),
('marital_status', '6','Inconnu' , 'Onbekende'),
('sublevel', '1', 'Personnel scientifique', 'Wetenschappelijk personeel'),
('sublevel', '2', 'Non applicable','niet van toepassing'),
('sublevel', '3', 'Personnel technique','Technisch personeel'),
('sublevel', '4', 'Personnel dirigeant','Statutair leidinggevend personeel'),
('sublevel', '5', 'Personnel non scientifique','Niet wetenschappelijk personeel'),
('sublevel', '6', 'Personnel considéré comme scientifique','Zogenaamd Wetenschappelijk personeel'),
('sublevel', '7', 'Personnel scientifique dirigeant','Leiding gevend wetenschappelijk personeel'),
('exit_reason', '1','fin de contrat', 'Einde contract'),
('exit_reason', '2','à la retraite','Op pensioen'),
('exit_reason', '3','à la retraite anticipé','Op vervroegd pensioen'),
('exit_reason', '4','à la retraite par cause de maladie','Pensioen wegens ziekte'),
('exit_reason', '5','licenciement volontaire','Vrijwillig ontslag'),
('exit_reason', '6','licenciement forcé','Gedwongen ontslag'),
('exit_reason', '7','décédé','Overleden'),
('exit_reason', '8','autre raison','Andere reden'),
('contract_duration','1','Contrat à durée déterminée','Arbeidsovereenkomst voor een bepaalde duur'),
('contract_duration','2','Contrat à durée indéterminée','Arbeidsovereenkomst voor een onbepaalde duur'),
('contract_duration','3','Contrat à objet déterminé','Arbeidsovereenkomst voor een bepaald project'),
('contract_duration','4','Contrat de remplacement','Arbeidsovereenkomst voor vervanging'),
('contract_comment','1','Statutaire','Statutair'),
('contract_comment','2','Contractuel','Contractueel'),
('contract_comment','3','Personnel détaché','Gedetacheerd personeel'),
('contract_comment','4','Collaborateur','Medewerker'),
('contract_comment','5','Bénévole','Vrijwilliger'),
('contract_comment','6','ASBL Amis','VZW vrienden'),
('contract_comment','7','Collaborateur sc.','weten. medewerker'),
('contract_comment','8','Contractuel SSTC','Contractueel DWTC'),
('contract_comment','9','Contractuel étudiant','Studentcontractueel'),
('contract_comment','10','Chercheur','Vorser'),
('contract_comment','11','Temporaire','Tijdelijk'),
('contract_comment','12','Plan Rosetta','Plan Rosetta'),
('contract_comment','13','Etudient','Student'),
('contract_comment','14','Visiteur','Bezoeker'),
('contract_comment','15','Stagiaire','Stagiaire'),
('contract_comment','16','ASBL Anthropology','VZW Anthropologie'),
('contract_comment','17','ASBL Entomology','VZW Entomologie'),
('contract_comment','18',E'Personnel détaché de l\'IRSNB','Gedetacheerd personeel vanuit KBIN'),
('doctype','','Général','Algemeen');

INSERT INTO public.catalogue(id, type, code, ehr_code, name_fr, name_nl)
SELECT cou_id_ctn, 'country', cou_code, cou_code2, initcap(cou_label_fr), initcap(cou_label_nl) 
FROM tbl_countries;

SELECT setval('catalogue_id_seq', (select max(id) from catalogue)) ; 

INSERT INTO public.catalogue(type, ehr_code, code, name_fr, name_nl, level, is_active)
SELECT 'grade', gra_id_ctn, gra_code, gra_label_fr, gra_label_nl, gra_level,
(CASE WHEN gra_active='0' THEN False ELSE true END) 
FROM tbl_grades ;

INSERT INTO public.catalogue(type, ehr_code, code, name_fr, name_nl)
SELECT 'cause', cau_id_ctn, cau_code, cau_description_fr, cau_description_nl
FROM tbl_causes ; 

INSERT INTO public.catalogue(type, ehr_code, code, name_fr, name_nl, is_active)
SELECT 'status', sta_id_ctn, sta_code, sta_label_fr, sta_label_nl,
(CASE WHEN sta_active='0' THEN False ELSE true END) 
FROM tbl_status ;

INSERT INTO public.catalogue(type, ehr_code, code, name_fr, name_nl)
SELECT 'reason', emp_id_ctn, emp_code, emp_label_fr, emp_label_nl
FROM tbl_reasons ;

INSERT INTO public.catalogue(type, ehr_code, code, name_fr, name_nl)
SELECT 'premium',  pre_id_ctn, pre_code, pre_label_fr, pre_label_nl
FROM tbl_premiums;

INSERT INTO public.catalogue(type, ehr_code, code, name_fr, name_nl, level)
SELECT 'salarygrade', sal_id_ctn, sal_code, sal_label_fr, sal_label_nl, sal_level
FROM tbl_salarygrades;

INSERT INTO public.catalogue(id, type, ehr_code, code, name_fr, name_nl, level,is_active)
SELECT 2000+fun_id_ctn, 'funct_family',  fun_ehr_code, fun_code, fun_label_fr, fun_label_nl, fun_level,
(CASE WHEN fun_active='0' THEN False ELSE true END) 
FROM tbl_functionfam ORDER BY fun_id_ctn ;

--Insert Cities and Medical centers
INSERT INTO public.city(id, country_ref, code, name)
SELECT cty_id_ctn, cty_country_id_nr, cty_code, cty_name
FROM tbl_cities ;

INSERT INTO public.medical_center(id,city_ref, name, phone, fax, address)
SELECT mec_id_ctn,mec_city_id_nr, mec_name, 
COALESCE(mec_phone,''), 
COALESCE(mec_fax,''), 
COALESCE(mec_address,'')
FROM tbl_medcentres;

--Insert department
INSERT INTO public.department(id, level, name_fr, name_en, name_nl, parent_ref, path)
SELECT dep_id_ctn, 'department', dep_name_fr, dep_name_en, dep_name_nl, null, '/' FROM tbl_departments ;
   
SELECT setval('department_id_seq', (select max(id) from department)) ; 

INSERT INTO public.department(id, level, name_fr, name_en, name_nl, parent_ref, path)
SELECT 100+sec_id_ctn, 'section', sec_name_fr, sec_name_en, sec_name_nl, sec_department_id_nr, ('/' || sec_department_id_nr || '/') as path
FROM tbl_sections ;

--Insert persons
INSERT INTO public.person(old_id,nationality_ref, first_name, last_name, alias_first_name, alias_last_name, 
mother_tongue, dependance, birth_date, birth_place, gender, marital_status_ref, identitycard_nr, national_nr, 
matricule, id, dimona_nr, payroll_tax, account_iban_nr, account_bic_nr, med_number, sis_nr, medical_center_ref, passport_nr)
SELECT DISTINCT ON (p.prs_id_ctn) p.prs_id_ctn, i.inf_nationality_nr, p.prs_first, p.prs_last, 
COALESCE(i.inf_alias,''),
COALESCE(i.inf_alias1,''),
p.prs_mothertongue, false,
COALESCE(i.inf_dateofbirth,timestamp '2001-01-01 00:00:00'),
COALESCE(i.inf_placeofbirth,''),
(CASE WHEN i.inf_gender='1' THEN 'M' ELSE 'F' END),
COALESCE((SELECT id FROM catalogue WHERE type='marital_status' AND ehr_code=COALESCE(i.inf_maritalstatus,'6')),6), i.inf_identitycardnr, 
i.inf_natnr, COALESCE(i.inf_matricule,''), p.prs_badgeid, i.inf_dimonanr, 
(CASE WHEN i.inf_payrolltax='0' THEN False ELSE true END) as payroll_tax, 
(CASE WHEN (i.inf_landcode || i.inf_controle || i.inf_accountnr) ISNULL THEN '' ELSE i.inf_landcode || i.inf_controle || i.inf_accountnr END), 
(CASE WHEN (i.inf_swift_plaats || i.inf_swift_bank || i.inf_swift_land) ISNULL THEN '' ELSE i.inf_swift_bank || i.inf_swift_land || i.inf_swift_plaats END),
(SELECT (CASE WHEN med_id_nr ~ '^0*$' THEN NULL else med_id_nr END) FROM tbl_mednumbers WHERE med_person_id_nr=p.prs_id_ctn),
(SELECT med_sisnr FROM tbl_mednumbers WHERE med_person_id_nr=p.prs_id_ctn) as sys_nr,
(SELECT med_medical_center_id_nr FROM tbl_mednumbers WHERE med_person_id_nr=p.prs_id_ctn),
(CASE WHEN i.inf_passnr='0' THEN '' ELSE i.inf_passnr END) 
--(SELECT (CASE WHEN med_id_nr ~ '^0*$' THEN NULL else med_id_nr END) FROM public.tbl_mednumbers WHERE med_person_id_nr=p.prs_id_ctn)
 
FROM tbl_persons p, tbl_persinfo i
WHERE p.prs_id_ctn = i.inf_person_id_nr AND prs_badgeid IS NOT NULL;

-- set badge_id sequence correctly
SELECT setval('person_id_seq', (select max(id) from person)) ;

--Insert person_addresses
INSERT INTO public.person_address(person_ref, city_ref, street, street_num, box, type)
SELECT 
(SELECT id from person where old_id=adr_person_id_nr) as person_ref,
adr_city_id_nr, COALESCE(adr_address,''), COALESCE(adr_address_nr,''), COALESCE(adr_address_box,''), 
(CASE WHEN adr_category_id_nr='9' THEN 'work' ELSE 'home' END)
FROM tbl_persaddresses WHERE adr_person_id_nr NOT IN (1985,660,2116,658) ;

--Insert into person_contact
INSERT INTO public.person_contact(person_ref, type, entry, prefered)
SELECT 
(SELECT id from person where old_id=mai_person_id_nr) as person_ref,
'mail', mai_email, false FROM tbl_emails WHERE mai_person_id_nr NOT IN (1985,660,2116,658) ;

INSERT INTO public.person_contact(person_ref, type, entry, prefered)
SELECT (SELECT id from person where old_id=rop_person_id_nr), 'phone', pho_phonenr, false FROM tbl_phones, tbl_persrooms 
WHERE pho_category_id_nr = '8' AND pho_person_room_id_nr=rop_id_ctn AND rop_person_id_nr NOT IN (1985,660,2116,658) ;


INSERT INTO public.person_contact(person_ref, type, entry, prefered)
SELECT (SELECT id from person where old_id=rop_person_id_nr), 'fax', fax_faxnr, false FROM tbl_faxes, tbl_persrooms 
WHERE fax_category_nr = '8' AND fax_person_room_id_nr=rop_id_ctn AND rop_person_id_nr NOT IN (1985,660,2116,658) ;

--Insert children in Family
INSERT INTO public.family(type, person_ref, full_name, gender, birth_date, allowance, 
allowance_end, parential_leave, comment)
SELECT 'child', 
(SELECT id from person where old_id=chi_person_id_nr), chi_first || ' ' || chi_last, (CASE WHEN chi_gender='1' THEN 'M' ELSE 'F' END), chi_dateofbirth, 
chi_allowance::boolean, chi_endallowance, chi_parential_leave::boolean, chi_comment FROM tbl_children 
 WHERE chi_person_id_nr NOT IN (1985,660,2116,658) ;
--Insert Partners in Family
INSERT INTO public.family(type, person_ref, full_name, gender, employed)
SELECT 'spouse', 
(SELECT id from person where old_id=par_person_id_nr), par_last || ' ' || par_first, (CASE WHEN par_gender='1' THEN 'M' ELSE 'F' END), par_employed::boolean FROM tbl_partners 
WHERE par_person_id_nr NOT IN (1985,660,2116,658) ;

--Insert Transport
INSERT INTO public.transport(person_ref, start_date, end_date, stibmivb, tec, delijn, sncb, is_effective,city_from, city_to)
SELECT 
(SELECT id from person where old_id=trs_person_id_nr), trs_datefrom, trs_dateto,
(CASE WHEN trs_stibmivb = '1' THEN true ELSE false END),
(CASE WHEN trs_tec = '1' THEN true ELSE false END),
(CASE WHEN trs_delijn = '1' THEN true ELSE false END),
(CASE WHEN trs_sncbnmbs = '1' THEN true ELSE false END),
trs_effective::boolean, trs_from, trs_to FROM tbl_transportation
WHERE trs_person_id_nr NOT IN (1985,660,2116,658) ;

INSERT INTO public.transport(person_ref, start_date, bike, km, is_effective,comment)
SELECT (SELECT id from person where old_id=bic_person_id_nr), bic_datefrom, true, bic_km, true, bic_comment FROM tbl_bicycles 
WHERE bic_person_id_nr NOT IN (1985,660,2116,658) ;

INSERT INTO public.transport(person_ref, car, start_date, numberplate)
SELECT (SELECT id from person where old_id=car_person_id_nr), true, car_datefrom, car_numberplate FROM tbl_cars
WHERE car_person_id_nr NOT IN (1985,660,2116,658) ;

--Insert person premiums
INSERT INTO public.premium(person_ref, premium_ref, start_date, end_date)
SELECT (SELECT id from person where old_id=hpr_person_id_nr), 
(SELECT id FROM catalogue WHERE type='premium' and ehr_code=hpr_premium_id_nr::text), hpr_datefrom, hpr_dateto
FROM tbl_perspremiums
WHERE hpr_person_id_nr NOT IN (1985,660,2116,658) ;

--Insert working_hist
UPDATE tbl_histstatus SET hst_sublevel = 'Non applicable/niet van toepassing'
WHERE hst_sublevel = 'Non applicable / niet van toepassing';

INSERT INTO working_hist(id, person_ref, replacement_person, contract_comment_ref, grade_ref, 
reason_ref, salary_grade_ref, function_family_ref, level, sublevel_ref, contract_duration_ref, linguistic_role, seniority, salary_seniority, 
start_date, end_date, status_ref) SELECT
s.hst_id_ctn, (SELECT id from person where old_id=s.hst_person_id_nr) as person_ref, (select last_name || ' ' || first_name from person where old_id=s.hst_replacement_id_nr) as remplacement,
(SELECT id FROM catalogue WHERE type='contract_comment' AND ehr_code=s.hst_comment),
(SELECT id FROM catalogue WHERE type='grade' AND ehr_code=s.hst_grade_id_nr::text),
(SELECT id FROM catalogue WHERE type='reason' AND ehr_code=s.hst_employment_id_nr::text),
(SELECT id FROM catalogue WHERE type='salarygrade' AND ehr_code=s.hst_salarygrade_id_nr::text),
(SELECT id FROM catalogue WHERE id = 2000+COALESCE(s.hst_function_family_id_nr,93)), s.hst_level,
(SELECT id FROM catalogue WHERE type='sublevel' AND name_fr = substring(s.hst_sublevel from 1 for position('/' in s.hst_sublevel)-1)),
(SELECT id FROM catalogue WHERE type='contract_duration' AND ehr_code=s.hst_duration),
s.hst_linguistic, s.hst_seniority, s.hst_gradeseniority,
s.hst_datefrom, s.hst_dateto,
(SELECT id FROM catalogue WHERE type='status' AND ehr_code=s.hst_status_id_nr::text) 
FROM tbl_histstatus s WHERE hst_person_id_nr NOT in (1985,660,2116,658);

--Insert working_duty
INSERT INTO working_duty(
            id, person_ref, department_ref, cause_ref, percentage, start_date, 
            end_date)
SELECT dut_id_ctn, (select id from person where dut_person_id_nr=old_id),
COALESCE(100+dut_section_id_nr, dut_department_id_nr, 166),
(SELECT id FROM catalogue WHERE type='cause' AND ehr_code=dut_cause_id_nr::text), 
COALESCE(dut_pct,100), dut_datefrom, dut_dateto
FROM tbl_histduties
WHERE dut_person_id_nr NOT IN (1985,660,2116,658) ;

-- Insert entries
SELECT public.add_entries() ;

--Insert duty_calendar
INSERT INTO public.duty_calendar(
working_duty_ref, week, mon_am, mon_pm, tue_am, tue_pm, wed_am, 
wed_pm, thu_am, thu_pm, fri_am, fri_pm, sat_am, sat_pm, sun_am, 
sun_pm)
SELECT id,
c1.cal_week::integer-1, c2.cal_mon::boolean, c1.cal_mon::boolean, c2.cal_tue::boolean, 
c1.cal_tue::boolean, c2.cal_wed::boolean,c1.cal_wed::boolean, c2.cal_thu::boolean,
c1.cal_thu::boolean, c2.cal_fri::boolean, c1.cal_fri::boolean,c2.cal_sat::boolean,
c1.cal_sat::boolean,c2.cal_sun::boolean, c1.cal_sun::boolean
FROM tbl_calendar c1, tbl_calendar c2, working_duty
WHERE c1.cal_duty_id_nr=id
AND c1.cal_duty_id_nr = c2.cal_duty_id_nr
AND c1.cal_week = c2.cal_week
AND c1.cal_am = 0 AND c2.cal_am = 1;

--Remove useless ehr_code in catalogue
Update public.catalogue set ehr_code = NULL
WHERE type NOT IN ('country','funct_family');


--drop table public.dubious_status_duties;
drop function "add_entries"();
drop function "search_entry"(integer);


SELECT setval('person_id_seq', (select max(id) from person)) ;
SELECT setval('city_id_seq', (select max(id) from city)) ;
SELECT setval('department_id_seq', (select max(id) from department)) ;
SELECT setval('medical_center_id_seq', (select max(id) from medical_center)) ;
SELECT setval('family_id_seq', (select max(id) from family)) ;
SELECT setval('person_contact_id_seq', (select max(id) from person_contact)) ;
SELECT setval('person_address_id_seq', (select max(id) from person_address)) ;
SELECT setval('premium_id_seq', (select max(id) from premium)) ;
SELECT setval('transport_id_seq', (select max(id) from transport)) ;
SELECT setval('working_hist_id_seq', (select max(id) from working_hist)) ;
SELECT setval('working_duty_id_seq', (select max(id) from working_duty)) ;
SELECT setval('duty_calendar_id_seq', (select max(id) from duty_calendar)) ;



commit;
