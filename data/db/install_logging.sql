
CREATE OR REPLACE FUNCTION get_setting(IN param text, OUT value text)
LANGUAGE plpgsql STABLE STRICT AS
$$BEGIN
  SELECT current_setting(param) INTO value;
  EXCEPTION
  WHEN UNDEFINED_OBJECT THEN
    value := NULL;
END;$$;


CREATE OR REPLACE FUNCTION fct_trk_log_table() RETURNS TRIGGER
AS $$
DECLARE
  user_id text;
  track_level integer;
  track_fields integer;
  trk_id bigint;
BEGIN

  SELECT COALESCE(get_setting('rbins.userid'),'') INTO user_id;
  IF user_id = '' THEN
    RETURN NEW;
  END IF;

  IF TG_OP = 'INSERT' THEN
    INSERT INTO users_tracking (referenced_relation, record_id, user_ref, action, modification_date_time, new_value)
        VALUES (TG_TABLE_NAME::text, NEW.id, user_id, 'insert', now(), hstore(NEW)) RETURNING id into trk_id;
  ELSEIF TG_OP = 'UPDATE' THEN

    IF ROW(NEW.*) IS DISTINCT FROM ROW(OLD.*) THEN
    INSERT INTO users_tracking (referenced_relation, record_id, user_ref, action, modification_date_time, new_value, old_value)
        VALUES (TG_TABLE_NAME::text, NEW.id, user_id, 'update', now(), hstore(NEW), hstore(OLD)) RETURNING id into trk_id;
    ELSE
      RAISE info 'unnecessary update on table "%" and id "%"', TG_TABLE_NAME::text, NEW.id;
    END IF;

  ELSEIF TG_OP = 'DELETE' THEN
    INSERT INTO users_tracking (referenced_relation, record_id, user_ref, action, modification_date_time, old_value)
      VALUES (TG_TABLE_NAME::text, OLD.id, user_id, 'delete', now(), hstore(OLD));
  END IF;

  RETURN NULL;
END;
$$
LANGUAGE plpgsql;




CREATE TRIGGER catalogue_trk AFTER INSERT OR UPDATE OR DELETE
        ON catalogue FOR EACH ROW
        EXECUTE PROCEDURE fct_trk_log_table();

CREATE TRIGGER city_trk AFTER INSERT OR UPDATE OR DELETE
        ON city FOR EACH ROW
        EXECUTE PROCEDURE fct_trk_log_table();

CREATE TRIGGER department_trk AFTER INSERT OR UPDATE OR DELETE
        ON department FOR EACH ROW
        EXECUTE PROCEDURE fct_trk_log_table();

CREATE TRIGGER medical_center_trk AFTER INSERT OR UPDATE OR DELETE
        ON medical_center FOR EACH ROW
        EXECUTE PROCEDURE fct_trk_log_table();

CREATE TRIGGER person_trk AFTER INSERT OR UPDATE OR DELETE
        ON person FOR EACH ROW
        EXECUTE PROCEDURE fct_trk_log_table();

CREATE TRIGGER family_trk AFTER INSERT OR UPDATE OR DELETE
        ON family FOR EACH ROW
        EXECUTE PROCEDURE fct_trk_log_table();

CREATE TRIGGER person_address_trk AFTER INSERT OR UPDATE OR DELETE
        ON person_address FOR EACH ROW
        EXECUTE PROCEDURE fct_trk_log_table();

CREATE TRIGGER cperson_contact_trk AFTER INSERT OR UPDATE OR DELETE
        ON person_contact FOR EACH ROW
        EXECUTE PROCEDURE fct_trk_log_table();

CREATE TRIGGER transport_trk AFTER INSERT OR UPDATE OR DELETE
        ON transport FOR EACH ROW
        EXECUTE PROCEDURE fct_trk_log_table();

CREATE TRIGGER working_hist_trk AFTER INSERT OR UPDATE OR DELETE
        ON working_hist FOR EACH ROW
        EXECUTE PROCEDURE fct_trk_log_table();

CREATE TRIGGER duty_calendar_trk AFTER INSERT OR UPDATE OR DELETE
        ON duty_calendar FOR EACH ROW
        EXECUTE PROCEDURE fct_trk_log_table();

CREATE TRIGGER person_entry_trk AFTER INSERT OR UPDATE OR DELETE
        ON person_entry FOR EACH ROW
        EXECUTE PROCEDURE fct_trk_log_table();

CREATE TRIGGER document_trk AFTER INSERT OR UPDATE OR DELETE
        ON document FOR EACH ROW
        EXECUTE PROCEDURE fct_trk_log_table();
